
"Reaction-diffusion"-type example
===================================

Four-component chemical network (A/B/C/X), the first two of which are
diffusible through gap junctions.  The kinetic parameters of the
network are evolved by the GA -- 20 parameters in all.  (Note that the
diffusion constants are fixed and identical, so this is not actually a
Turing mechanism; it is auto-electrophoretic.)

(Adapted from a sample by Alexis Pietak.)


## Example 1 -- Selecting for pattern amplitude:

### Fitness function:
    
    reduce_stdev() / (1 + reduce_rms(deltaval))

Standard deviation of Vmem is used as a metric of pattern amplitude,
to which the usual softened penalty on RMS dVmem/dt is applied.


### Example outputs:

##### Trajectory:
![Example trajectory](pics/stdev-trajectory-rd.png)

(Population 89)

##### Early result:
![Intermediate output](pics/stdev-ex1-rd.png)

##### Late result:
![Late output](pics/stdev-ex2-rd.png)


## Example 2 -- Selecting for non-directional patterns:

### Fitness function:
    
    reduce_stdev() / (1 + global_directionality()) / (1 + reduce_rms(deltaval))
   

### Example outputs:

##### Trajectory:
![Example trajectory](pics/stdevoopgd-trajectory-rd.png)

(Population 89)

##### Early result:
![Intermediate output](pics/stdevoopgd-ex1-rd.png)

##### Late result:
![Late output](pics/stdevoopgd-ex2-rd.png)

##### Best ever:
![Best output](pics/stdevoopgd-exbest-rd.png)


