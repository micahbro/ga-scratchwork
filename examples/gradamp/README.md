Polarization by gradient amplification
========================================

Four-component chemical network (A/B/C/X), the first two of which are
diffusible through gap junctions.  The kinetic parameters of the
network are evolved by the GA.  A weak gradient is applied to the
production rate of X.  (Network is otherwise the same as the
"rdpattern" examples, and patch file is identical.)

(Based on a sample by Alexis Pietak.)


## Example -- Selecting for a polarization dipole:

### Fitness function:

Dipole moment, with usual penalty on dVmem/dt:

    dipole() / (1 + reduce_rms(deltaval))


### Example outputs:

![Example trajectory](pics/dipole-trajectory-gradamp.png)

(Population 89)


![Example output](pics/dipole-ex-gradamp.png)
