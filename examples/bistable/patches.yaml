%YAML 1.1
---

tissue profile definition:
    profiles:
        # Example default tissue profile applying to all cells.
        - type: tissue
          name: all            # unique profile name
          diffusion constants: # constants applied to all targetted cells
              Dm_Na:
                $match:
                  Name: Dm_Na
                  Max: 1.0e-15              
                  Min: 1.0e-20
                  Logscale: True
                  # 1.0e-18
              Dm_K:
                $match:
                  Name: Dm_K
                  Max: 1.0e-15
                  Min: 1.0e-20
                  Logscale: True
                  # 15.0e-18
        - type: tissue
          name: spot           # unique profile name
          diffusion constants: # constants applied to all targetted cells
              Dm_Na:
                $match:
                  Name: Dm_Na_spot
                  Derived: trait('Dm_Na')
              Dm_K:
                $match:
                  Name: Dm_K_spot
                  Derived: trait('Dm_K')


persistent vg Na+:
    turn on:                 # voltage gated sodium channel with persistant current
      $match:
        Name: vg_Nap_en
    apply to: ['all']              # name(s) of the tissue profile(s) to apply it to
    channel type: ['Nav1p6']         # available types: 'Nav1p6' (persistent brain)
    max value:
      $match:
        Name: vg_Nap_max
        Max: 1.0e-5 
        #1.0e-7             # maximum channel conductivity when channel fully open [S/m2]

voltage gated K+:             # voltage gated potassium channel, delayed rectifyer
    turn on: True 
    apply to: ['all']          # name(s) of the tissue profile(s) to apply it to
    channel type:
      $match:
        Name: vg_K_types
        Containing:
          Type: set
          Alternatives: ['Kv1p1', 'Kv1p2', 'Kv1p3', 'Kv1p5', 'K_Slow', 'K_Fast', 'Kv2p1']
          # ['K_Slow','K_Fast']      # available types: 'Kv1p1', 'Kv1p2', 'Kv1p3', 'Kv1p5',
                                           # 'K_Slow', 'K_Fast', 'Kv2p1'
    max value:
      $match:
        Name: vg_K_max
        Max: 1.0e-5
        # 1.0e-7         # maximum channel conductivity when channel fully open [S/m2]

additional voltage gated K+:    # Other types of K+ currents including inward-rectifying
    turn on: True 
    apply to: ['all']          # name(s) of the tissue profile(s) to apply it to
    channel type:
      $match:
        Name: avg_K_types
        Containing:
          Type: set
          Alternatives: ['Kir2p1', 'Kv3p4', 'Kv3p3']
    # ['Kir2p1']      # available types: 'Kir2p1', 'Kv3p4' and 'Kv3p3' (A-currents)
    max value:
      $match:
        Name: avg_K_max
        Max: 1.0e-5 
        # 1.0e-7         # maximum channel conductivity when channel fully open [S/m2]


variable settings:

  env boundary concentrations:   # these concentrations are placed at the global boundary durring init and sim
                                 # altering these is an easy way to change extracellular ion concentrations

    Na:
      $match:
        Name: Na_bound
        Max: 1000 
        #145.0    # Na+ concentration at bound [mM]
    K:
      $match:
        Name: K_bound
        Max: 100 
        #5.0      # K+ concentration at bound [mM]

#  temperature: 310            # system temperature [K]

  gap junctions:

    gap junction surface area:
      $match:
        Name: gj_sa
        Min: 1.0e-9
        Max: 1.0e-6
        Logscale: True
        #1.0e-6     # surface area of gj as fraction of cell area (1.0e-5 extremely open, 1e-9 closed)

    voltage sensitive gj: True   # gj close with voltage difference between cells?

    gj voltage threshold:
      $match:
        Name: gj_vthresh
        Max: 100        # cell-cell voltage at which gj are half closed [mV]
                                 # suggested range from 10 mV to 80 mV

    gj minimum:
      $match:
        Name: gj_min
        Max: 1.0      # for voltage sensitive gj, the fraction of maxium permeability when channel 'closed'


  tight junction scaling: 
      $match:
        Name: tj_scal
        Min: 1.0e-8
        Max: 1.0           # factor by which tight junctions scale
        Logscale: True
                                   # free-diffusion (1.0 = free diffusion, 1.0e-9 = similar to membrane permeability)

#  adherens junction scaling: 1.0   # factor by which cell-cell junctions scale
                                   # free-diffusion

internal parameters:
  tissue resistivity:
      $match:
        Name: R_tissue
        Min: 1.0
        Max: 75.0        #  electrical resistivity of bulk tissue (Ohms.m)




