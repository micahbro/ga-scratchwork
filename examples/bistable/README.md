
"Bistability" example
============================

Cell cluster with evolvable channel types, channel parameters,
membrane conductivites, medium conductivity, gap junctions, solution
concentrations, and other non-regulatory features -- 14 parameters in
total.  A programmed intervention occurs between 0.5s and 0.8s,
temporarily increasing the membrane sodium permeability in a central
spot region, causing localized depolarization.

Illustrates the use of advanced patch file features: derived traits,
Boolean traits, and set-valued traits.  Derived traits are utilized to
ensure that the multiple regions defined in the BETSE configuration
file all evolve with the same parameters.  Boolean and set-valued
traits are used to evolve the set of ion channels present.

Also illustrates the use of time-dependent "before and after"
fitness functions for scoring the response to an intervention.


## Example -- Selecting for bistable memory:

### Fitness function:

    within(imagemask('testruns/bistable/base/geo/circle/spot.png', 200),        # Inside the spot...
           reduce_avg(1 + tanh(defval / 100 + .3))) *                           # Avg score above threshold
      within(1 - imagemask('testruns/bistable/base/geo/circle/spot.png', 200),  # Outside the spot...
             reduce_avg(1 - tanh(defval / 100 + .3))) *                         # Avg score below threshold
      atframe(3, reduce_avg(1 - tanh(defval / 100 + .3)))                       # Prior to intervention, avg score below threshold
      / (10 + reduce_rms(deltaval))                                             # d/dt penalty


Selects for Vmem uniformly below -30mV just prior to intervention (at
frame 3), but at the end of the simulation, long after intervention,
below -30mV only outside the region of intervention, and otherwise
above -30mV. In other words, selects for strong and sustained
depolarization in the area of the intervention, after the intervention
has been removed, and nowhere else. `tanh` is used as a soft threshold
function (favoring uniformity over scattered crazy cells), and the
second argument to `imagemask` ('200') is the world size (microns).  A
softened penalty on final RMS dVmem/dt is applied, as usual, but 10x
softer than has been typical. (This was chosen empirically after
observing that the usual penalty seemed excessively harsh here and
sometimes favored local minima.)

 
### Example outputs:

![Example trajectory](pics/bistable-trajectory-pureelec.png)

(Population 89, 30 rounds of initial mutation)

##### Vmem just prior to intervention:

![Example starting output](pics/bistable-ex-initial-pureelec.png)


##### Vmem long after intervention removed:

![Example ending output](pics/bistable-ex-final-pureelec.png)

Independent experimentation shows that the effect is indeed memory
(and not programmed overfitting).

