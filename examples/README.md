
GABEE example runs
--------------------

* [pureelectric](/examples/pureelectric) -- Introductory, "pure
  bioelectric" examples

* [bistable](/examples/bistable) -- Bistable memory in a pure
  bioelectric system

* [rdpattern](/examples/rdpattern) -- "Reaction-diffusion"-like
  patterning examples using chemically gated channels

* [gradamp](/examples/gradamp) -- Polarization via amplification of a
  seed gradient in a chemically gated system

* [overfit-surgery](/examples/overfit-surgery) -- Overfitting-prone
  attempt at polarization that can regenerate when sliced

* [spontanepol](/examples/spontanepol) -- Spontaneous polarization in
  a chemically gated system

* [stripespot](/examples/stripespot) -- Stripes and spots in an
  extended "Reaction-diffusion"-like chemically gated system

