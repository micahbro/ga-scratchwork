Overfitting-prone post-surgical gradient regeneration
=======================================================

Four-component chemical network (A/B/C/X), the first two of which are
diffusible through gap junctions.  The kinetic parameters of the
network are evolved by the GA.  A weak gradient is applied to the
production rate of X.  (Network is the same as the "gradamp" example,
and patch file is identical.)

The init stage is configured to run for a full 20s (same as sim stage)
in order to develop an initial pattern, and then a surgery operation
takes place at the beginning of sim, cutting the cell cluster into
three chunks.  (Unfortunately, BETSE does not support surgery
mid-sim.)

(Based on a sample by Alexis Pietak.)


## Example -- Selecting for a polarization dipole both before and after surgery:

### Fitness function:

Geometric mean of dipoles before and after:
    
    (within(posvec[0] < 75, smax(0, dipole(axis=(1,0)))) *
      within((posvec[0] > 75) * (posvec[0] < 125), smax(0, dipole(axis=(1,0)))) *
      within(posvec[0] > 125, smax(0, dipole(axis=(1,0)))) *
      atframe(0, smax(0, dipole(axis=(1,0))))) ** (1/4)
     / (1 + reduce_rms(deltaval))

The geometric mean of four x-axis dipole measurements is computed --
three at the end of the simulation, one each per cut chunk, and one
over the whole domain at the very beginning (right _after_ surgery,
but hopefully before any changes in pattern have taken place).  This
favors the presence of a single large dipole at the beginning and
three small, localized dipoles at the end, which tends to encourage at
least partial regeneration after surgery.  The dipole measurements are
truncated positive using `smax` (a smoothed version of `max`, which
also works acceptably), to favor only +x dipoles (otherwise they may
be oriented in different directions).  The usual penalty on dVmem/dt
at the end of the simulation is applied.

This approach gives mixed results.  Sometimes cut-dependent partial
regeneration is observed.  Often, however, the system trains to
anticipate the cuts, producing a "regenerated", multi-peaked pattern
even if the cuts are omitted.


### Example semi-successful run:

##### Trajectory:
![Example trajectory](pics/regen-succ-trajectory-gradamp.png)

(Population 89, 50 rounds initial mutation)

##### Result at beginning of sim stage:
![Example initial output](pics/regen-succ-ex-gradamp-init.png)

##### Result at end of sim stage:
![Example final output](pics/regen-succ-ex-gradamp-final.png)

##### Result at end of sim stage w/cuts manually disabled:
![Example no cuts output](pics/regen-succ-ex-gradamp-final-no-cuts.png)

(not evaluated by fitness function)

##### Comments:

This run was somewhat successful -- if the cuts are disabled, a
unimodal polarization develops (although it is much stronger at steady
state than the result seen here at the beginning of the sim stage).
Other runs may look similar but fail this test (see below).


### Example overfit run:

##### Trajectory:
![Example trajectory](pics/regen-overfit-trajectory-gradamp.png)

(Population 89, 50 rounds initial mutation)

##### Result at beginning of sim stage:
![Example initial output](pics/regen-overfit-ex-gradamp-init.png)

##### Result at end of sim stage:
![Example final output](pics/regen-overfit-ex-gradamp-final.png)

##### Result at end of sim stage w/cuts manually disabled:
![Example no cuts output](pics/regen-overfit-ex-gradamp-final-no-cuts.png)

##### Comments:

This run looks great, until you examine what happens with surgery
disabled: it's clearly overfitting, anticipating the cuts whether or
not they happen (even though it also shows some genuine
regeneration-like behavior).  Further evolution of this run can even
develop a visible multimodal pattern by the beginning of sim stage.
