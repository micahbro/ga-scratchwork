
Selecting for stripes and spots in extended "Reaction-diffusion"-like example
==============================================================================

Four-component chemical network (A/B/C/X), the first two of which are
diffusible through gap junctions.  The kinetic parameters of the
network, the diffusion constants, the initial chemical concentrations,
and the global gap junction parameters are all evolved by the GA (same
as spontanepol example) -- 31 parameters in all.  This is an expanded
version of the rdpattern example, with additional parameters allowed
to evolve.  The substrate domain is also replaced with a large circle,
to reduce boundary effects and to provide a larger area so that
spectral properties are exhibited more distinctly -- at the cost of
long running times.  Patterns of a consistent scale are selected by
computing total variation with a smoothing filter.  Stripes and spots
are distinguished from each other by the skewness of the empirical
distribution of Vmem values.

(Adapted from a sample by Alexis Pietak.)


## Example 1 -- Selecting for stripes:

### Fitness function:

Smoothed total variation with a penalty against skewness --
    
    reduce_avg(absvariation(movingavg(15)))     # Smoothed total variation
        / (.1 + reduce_stat_moment(3) ** 2)     # Skewness penalty
        / (1 + reduce_rms(deltaval))            # d/dt penalty

Total variation as a metric of pattern amplitude favors high
frequency, fine-grained patterns (it effectively measures the boundary
perimeter).  It is applied to a moving average filter with a radius of
15 microns (about 2 cells), which damps out the highest frequencies.
The combination results in a frequency-selective measure of pattern
amplitude, which is helpful in avoiding degenerate patterns.
(`reduce_stdev()` is an acceptable non-frequency-selective substitute,
but results are not as good.)

Skewness, the third statistical moment, is characteristically low for
stripe patterns (which are symmetrical in their high and low areas)
and so is penalized.  Skewness is squared to ensure a positive value
and then applied as a penalty with slight softening to avoid
pathological divide-by-zero ruggedness.  The usual penalty against
dVmem/dt is also applied to favor stability.

### Example run (a):

##### Trajectory:
![Example trajectory](pics/stripes-trajectory-rdextcirc-1.png)

(Population 89, 50 rounds initial mutation)

##### Result:
![Example output](pics/stripes-ex-rdextcirc-1.png)

### Example run (b):

##### Trajectory:
![Example trajectory](pics/stripes-trajectory-rdextcirc-2.png)

(Population 89, 50 rounds initial mutation)

##### Result:
![Example output](pics/stripes-ex-rdextcirc-2.png)


## Example 2 -- Selecting for spots:

### Fitness function:

Smoothed total variation with a saturated bonus for skewness --

     within(neighborcount >= 6,                      # Restrict to non-boundary
            reduce_avg(absvariation(movingavg(25)))  # Smoothed total variation
            * abs(tanh(reduce_stat_moment(3))))      # Skewness bonus
       / (1 + reduce_rms(deltaval))                  # d/dt penalty
    
For the same reasons as above, total variation is used as a
frequency-selective metric of pattern amplitude.  Skewness,
significantly elevated for spots, is favored, encouraging
concentration of magnitude into small areas, but with `tanh`
saturation to limit how high it grows. (Otherwise, patterns may
collapse into single dots.)  The `abs` is nonessential but ensures
positivity.  Both total variation and skewness penalty are tabulated
only over non-boundary cells, to encourage interior cells participate
in spots without excessive reliance on boundary cues -- nonessential,
but helpful.  The usual penalty against dVmem/dt is applied to favor
stability.

### Example run:

##### Trajectory:
![Example trajectory](pics/spots-trajectory-rdextcirc.png)

(Population 89, 50 rounds initial mutation)

##### Result:
![Example output](pics/spots-ex-rdextcirc.png)
