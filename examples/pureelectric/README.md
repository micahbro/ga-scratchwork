
"Pure bioelectric" example
============================

Simple introductory examples.  Cell cluster with evolvable channel
parameters, membrane conductivites, medium conductivity, gap
junctions, solution concentrations, and other non-regulatory features
-- 12 parameters in total.


## Example 1 -- Selecting for a target Vmem (-35mV):

### Fitness function:

A Gaussian centered at the target voltage:
    
    reduce_avg(exp(-(defval + 35) ** 2 / 100)) / (1 + reduce_rms(deltaval))
    
The global average is taken of a per-cell Gaussian centered on the
target Vmem, with variance wide enough to be smooth across the typical
range of Vmem values but narrow enough to be strongly biased toward
the target. A softened penalty on RMS dVmem/dt is also applied (as
usual), in order to favor steady state patterns.

### Example outputs:

![Example trajectory](pics/gaussvmem-trajectory-pureelec.png)

(Population 89)


![Example output](pics/gaussvmem-ex-pureelec.png)



## Example 2 -- Selecting for pattern amplitude:

### Fitness function:

Standard deviation used as a measure of the amplitude of spatial
patterning:

    reduce_stdev() / (1 + reduce_rms(deltaval))    


### Example outputs:

![Example trajectory](pics/stdev-trajectory-pureelec.png)

(Population 89)


![Example output](pics/stdev-ex-pureelec.png)

