
Spontaneous polarization in extended "Reaction-diffusion"-like example
========================================================================

Four-component chemical network (A/B/C/X), the first two of which are
diffusible through gap junctions.  The kinetic parameters of the
network, the diffusion constants, the initial chemical concentrations,
and the global gap junction parameters are all evolved by the GA -- 31
parameters in all.  This is an expanded version of the rdpattern
example, with additional parameters allowed to evolve.  The individual
simulations are also run twice as long, allowing time for larger-scale
interactions that would not otherwise have time to stabilize --
critical for spontaneous polarization.

(Adapted from a sample by Alexis Pietak.)


## Example -- Selecting for spontaneous polarization:

### Fitness function:

A saturated version of the dipole moment (with the usual d/dt penalty) --
    
    dipole(tanh((defval - reduce_avg()) / 5) * 5) / (1 + reduce_rms(deltaval))
    
Vmem is saturated about its mean using `tanh` before computing the
dipole moment, in order to improve the attractiveness of having a
large overall number of polarized cells over having smaller numbers of
cells with extremely strong Vmem in a more disorganized spatial
arrangement (such as Turing spots).


### Example outputs:

#### Example run 1:

##### Trajectory:
![Example trajectory](pics/dipole-trajectory-rdextlong.png)

(Population 89)

##### Result:
![Example output](pics/dipole-ex-rdextlong.png)

#### Example run 2:


##### Trajectory:
![Example trajectory](pics/dipole-trajectory-rdextlong-2.png)

(Population 89)

##### Result:
![Example output](pics/dipole-ex-rdextlong-2.png)



