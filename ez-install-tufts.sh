#!/bin/bash

set -e  # Stop if an error occurs

echo "Quick and dirty GABEE / BETSE setup for Tufts cluster..."
echo


##
## Prepare Python
##

echo "Preparing Python 3..."

PYTHON_MODULE=python/3.5.0
# NOTE: if the cluster is upgraded and 3.5.0 is discontinued, a newer
# 3.x version number may need to be substituted!

python3 --version > /dev/null 2>&1 || module load $PYTHON_MODULE

python3 --version
python3 -c "print('Python 3 working.')"
echo

echo "Creating Python venv..."
VENV_NAME=venv-betse
python3 -m venv $VENV_NAME

source $VENV_NAME/bin/activate

python --version
echo


##
## Fetch Python dependencies
##

# (Note that this is not normally the recommended way to get some of
# these packages, e.g. numpy / scipy -- you'll only get
# single-threaded versions this way. For now that's okay, since GABEE
# expects BETSE to be single-threaded anyway.)

echo "Fetching and installing Python modules..."
echo "(This may take some time...)"
echo

pip install dill~=0.2
pip install pillow~=3.4
pip install pyyaml~=3.12
pip install setuptools~=18.2
pip install six~=1.10
pip install numpy~=1.11.2
pip install scipy~=0.18
pip install matplotlib~=1.5
# (TODO: add networkx, pydot?, dot hack?? if newer BETSEs continue to require)
echo


##
## Fetch code for BETSE and GABEE
##

echo "Fetching code..."

# Latest BETSE commit that has been tested and confirmed to work
# under GABEE:
LAST_TESTED_BETSE_COMMIT=df15bd52902f55be653324372702b872477c9d63

if [ -d betse/betse ]
then
    echo "betse directory already exists; not fetching BETSE code."
else
    git clone https://gitlab.com/betse/betse.git
    (cd betse ; git checkout --quiet $LAST_TESTED_BETSE_COMMIT)
fi

echo

if [ -d ga-scratchwork/gabee ]
then
    echo "ga-scratchwork directory already exists; updating GABEE..."
    (cd ga-scratchwork ; git pull)
else
    git clone https://gitlab.com/micahbro/ga-scratchwork.git
fi

echo


# Prepare updated PYTHONPATH to include BETSE and GABEE
PYTHONPATH_ADDENDUM=`pwd`/betse:`pwd`/ga-scratchwork
export PYTHONPATH=$PYTHONPATH_ADDENDUM:$PYTHONPATH


##
## Quick smoke-tests
##

echo "Smoke test BETSE:"
(cd betse ; git log -n 1 | grep -e '^\S' )
python -m betse.cli --version

echo

echo "Smoke test GABEE..."
(cd ga-scratchwork ; git log -n 1 | grep -e '^\S' )
python -m gabee.cli --help > /dev/null

echo


##
## Make a default working path
##

if [ -w /cluster/shared/$USER ]
then
    # Set up a working directory in user's shared scratch space (Tufts-specific)
    RUNSDIR=/cluster/shared/${USER}/~gabee-default-workpath~
    test -d $RUNSDIR || mkdir $RUNSDIR
    ln -sfT $RUNSDIR default-workpath
else
    test -d default-workpath || mkdir default-workpath
fi

echo "Made default GABEE working path:"
readlink -f default-workpath

echo


##
## Make quick and dirty start-up scripts
##

make_script () {
    cat > $1 <<EOF
#!/bin/bash
# Automatically generated start-up script for $2
source "`pwd`/$VENV_NAME/bin/activate"
export "PYTHONPATH=$PYTHONPATH_ADDENDUM:\$PYTHONPATH"
export "GABEE_DEFAULT_WORKPATH=`pwd`/default-workpath"
test -d "\$GABEE_DEFAULT_WORKPATH" || mkdir \`readlink -f "\$GABEE_DEFAULT_WORKPATH"\`
python -m $3 "\$@"
EOF
    chmod +x $1
}

make_script gabee.sh GABEE gabee.cli
make_script betse.sh BETSE betse.cli
make_script patchtool.sh "GABEE Patchtool" gabee.patchtool
make_script trajtool.sh "GABEE Trajectory Tool" gabee.trajtool
make_script fittool.sh "GABEE Fitness Calculation Tool" gabee.fittool


##
## Make a ready-to-run sample configuration
##

if [ ! -d testruns/examplerun ]
then
    echo "Preparing a sample configuration..."

    ./betse.sh config testruns/examplerun/template/config.yaml > /dev/null
    cp ga-scratchwork/examples/rdpattern/*.yaml testruns/examplerun/template/
    ./betse.sh seed testruns/examplerun/template/config.yaml > /dev/null

    mkdir testruns/examplerun/runs

#    if [ -w /cluster/shared/$USER ]
#    then
#	# Set up a working directory in user's shared scratch space (Tufts-specific)
#        RUNSDIR=/cluster/shared/${USER}/~gabee-example-runs~.tmp
#	test -d $RUNSDIR || mkdir $RUNSDIR
#	ln -s $RUNSDIR testruns/examplerun/runs
#    else
#	mkdir testruns/examplerun/runs
#    fi

    echo
fi


echo "Done!"
echo "-------------------------------"
echo
echo "Run ./betse.sh to invoke BETSE."
echo "Run ./gabee.sh to invoke GABEE."
echo
echo "Try this to launch the sample configuration:"
echo 'salloc -n 20 -t 2:00:00 srun --x11=first --pty -N1 -n1 ./gabee.sh --templatepath testruns/examplerun/template/ --fitness "reduce_stdev() / (1 + reduce_rms(deltaval))" --popsize 19 --init-mix-rounds 50 --gen-count 40'
echo

