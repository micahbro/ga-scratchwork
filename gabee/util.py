## GABEE common utility routines
##
## (C) Micah Z. Brodsky 2016-
##


import random
import sys
import yaml
import os


symcount = 0
def gensym(prefix = ""):
    global symcount
    symcount += 1
    return prefix + "".join(['abcdefghijklmnopqrstuvwxyz'[random.randrange(26)] for _ in range(1,10)]) + str(symcount)
    # a counter plus a random component in case of mixed data across reloads or other mischief...


def load_config(pathname : str):
    with open(pathname, mode='rt') as f:
        return yaml.load(f)

def save_config(config, pathname : str):
    with open(pathname, mode='wt') as f:
        f.write(yaml.dump(config))

def save_config_safe(config, pathname : str):
    # Interruption-safe routine for overwriting a data file
    temp_save_file = pathname + gensym("~~~")
    save_config(config, temp_save_file)
    os.replace(temp_save_file, pathname) # Atomic rename for crash / CTRL+C safety


def argmax(d : dict):
    return max({(d[k], k) for k in d})[1]


def eprint(*args, **kwargs):
    print(*args, file=sys.stderr, **kwargs)
