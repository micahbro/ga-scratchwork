## .......................................
##
## (C) Micah Z. Brodsky 2017-
##

import math
import os
import sys
import matplotlib.image
import numpy
import scipy
import scipy.sparse as sparse
import scipy.sparse.linalg
import time
import yaml
import argparse
#import functools
#import copy


def hexplotmesh(h, w, in_mask):
    #patches = []
    polys = []
    for y in range(0, h):
        for x in range(0, w):
            oddrow = y % 2
            x_rect = x + oddrow / 2
            y_rect = y * math.sqrt(3) / 2
            sq3o3 = math.sqrt(3) / 3 * 1.1

            #patches.append(plt.Polygon([[x_rect - .55, y_rect + sq3o3 / 2],
            #                            [x_rect - .55, y_rect - sq3o3 / 2],
            #                            [x_rect, y_rect - sq3o3],
            #                            [x_rect + .55, y_rect - sq3o3 / 2],
            #                            [x_rect + .55,  y_rect + sq3o3 / 2],
            #                            [x_rect,  y_rect + sq3o3]], True))

            if not in_mask or in_mask(x,y):
                polys.append([[x_rect - .55, y_rect + sq3o3 / 2],
                              [x_rect - .55, y_rect - sq3o3 / 2],
                              [x_rect, y_rect - sq3o3],
                              [x_rect + .55, y_rect - sq3o3 / 2],
                              [x_rect + .55,  y_rect + sq3o3 / 2],
                              [x_rect,  y_rect + sq3o3]])

    #pc = matplotlib.collections.PatchCollection(patches)

    return matplotlib.collections.PolyCollection(polys)

def heximshow(axes, U, in_mask = None):

    tp0 = time.perf_counter()

    w = U.shape[1]
    h = U.shape[0]

    data = []
    for y in range(0, h):
        for x in range(0, w):
            if not in_mask or in_mask(x,y):
                data.append(U[y, x])


    # Astonishingly, putting together the PolyCollection is the vast majority of CPU that goes into plotting.
    # matplotlib neither allows them to be reused in multiple plots nor cloned. So, we make one per subfigure,
    # tack it to the axes, and keep updating it. Ugh. :P

    if '__mm_hexmesh' not in dir(axes):  # hacky
        #print("fleh")
        axes.__mm_hexmesh = hexplotmesh(h, w, in_mask)

    pc = axes.__mm_hexmesh


    tp1 = time.perf_counter()

    #pc.set_array(numpy.reshape(U, (w * h)))
    pc.set_array(numpy.array(data))
    pc.set_cmap('RdBu_r')
    pc.set_edgecolor('none')
    pc.set_clim(numpy.min(data), numpy.max(data))


    axes.add_collection(pc)
    axes.set_xlim((-1, w + .5))
    axes.set_ylim((-1, (h + .5) * math.sqrt(3) / 2))
    axes.set_aspect("equal")
    
    tp2 = time.perf_counter()
    #print("plot times " + str(tp1 - tp0) + " " + str(tp2 - tp1))    
    
    return pc



    #hceilodd = math.floor(h / 2) * 2 + 1
    #heven = 1 - (h - math.floor(h / 2) * 2)

    #neutralvalue = (numpy.max(U) + numpy.min(U)) / 2

    #return axes.hexbin(list(range(w)) * hceilodd, sum(([y] * w for y in range(hceilodd)), []), cmap = 'RdBu_r',
    #                   C = numpy.concatenate((numpy.reshape(U, (w * h)), [neutralvalue] * (w * heven))), gridsize = (w - 1, math.floor(h / 2)))


class Monitor:
    def __init__(self, saveimgname = None, savedataname = None):
        self.saveimgname = saveimgname
        self.savedataname = savedataname

        self.fig = plt.figure()
        self.fig.show()
        self.framecount = 0

    def update(self, U_matrix, in_mask):
        self.fig = plt.figure(self.fig.number)
        ###self.fig.clear()

        layers = U_matrix.shape[2]
        dim1 = math.ceil(math.sqrt(layers))
        dim2 = math.ceil(layers / dim1)

        for k in range(layers):
            ax = self.fig.add_subplot(dim1, dim2, k + 1)
            ax.clear()
            #im = ax.imshow(U_matrix[:, :, k], cmap = 'RdBu_r')
            im = heximshow(ax, U_matrix[:, :, k], in_mask)
            if '__mm_colorbar' not in dir(ax):  # hack :P
                if '_Monitor__mm_colorbar' in dir(ax):
                    print("wtfwtf") ### WTF??
                    #print(dir(ax))
                else:
                    #####
                    ax.__mm_colorbar = plt.colorbar(im)
        self.fig.canvas.draw()
        plt.pause(0.001)

        if self.saveimgname:
            self.fig.savefig(self.saveimgname + str(self.framecount) + ".png")

        if self.savedataname:
            scale = 10 # To be roughly consistent with BETSE default cell spacing... (ought to be a parameter)
            with open(self.savedataname + str(self.framecount) + ".csv", "wt") as datafile:
                datafile.write("# x, y, U...\n")

                w = U_matrix.shape[1]
                h = U_matrix.shape[0]
                for y in range(0, h):
                    for x in range(0, w):
                        if in_mask(x, y):
                            oddrow = y % 2
                            x_rect = x + oddrow / 2
                            y_rect = y * math.sqrt(3) / 2
                            datafile.write(str(x_rect * scale) + ", " + str(y_rect * scale) + ", " + ", ".join([str(u) for u in U_matrix[y, x, :]]) + "\n")

        self.framecount += 1


class SimState:
    def __init__(self, w : int = 50, h : int = 50, dx = 0.3, species : int = 1):
        self.w = w
        self.h = h
        self.dx = dx
        self.species = species
        self.t = 0
        self.sigma = None

        self.init_random()

    def init_random(self):
        n = self.w * self.h
        self.rho = numpy.random.uniform(0, 2.0, (n, self.species))
        self.V = numpy.random.uniform(0, 0.01, (n))

    def allocate_statevars(self, num_vars):
        self.sigma = numpy.zeros((self.w * self.h, num_vars))

    #def add_statevar(self, initvalues = None):
    #    # Add a new local state variable, and return its index in sigma

    #    if initvalues is None:
    #        newcol = numpy.zeros((self.w * self.h, 1))
    #    else:
    #        newcol = numpy.reshape(initvalues, (self.w * self.h, 1))

    #    if self.sigma is None:
    #        self.sigma = numpy.copy(newcol)
    #    else:
    #        self.sigma = numpy.concatenate((self.sigma, newcol), 1)

    #    return numpy.shape(self.sigma)[1] - 1


class Parameterizer:
    component_constructors = {}

    def __init__(self, species = 1, alpha = [-1.5], mu = [0], nu = [0], gamma = [0.1], epsilon = 0.2, beta = [5],
                 components = [], default_linear_channel = True, default_kinetics = True, vector_defaults = {}, default_values = {}):
        self.species = species
        #self.alpha = alpha
        #self.mu = mu
        #self.nu = nu
        #self.gamma = gamma
        #self.epsilon = epsilon
        #self.beta = beta

        self.default_values = default_values
        self.default_values["epsilon"] = epsilon

        self.set_vector_default("alpha", alpha)
        self.set_vector_default("mu", mu)
        self.set_vector_default("nu", nu)
        self.set_vector_default("gamma", gamma)
        self.set_vector_default("beta", beta)
        for k in vector_defaults:
            self.set_vector_default(k, vector_defaults[k])

        self.components = []
        self.providers = {}
        self.num_statevars = 0
        for c in components:
            self.add_component(c)

        if default_linear_channel:
            self.add_component(LinearChannelModel())

        if default_kinetics:
            self.add_component(AutonomousKineticsModel())

    # Must be stateless except cached parameter values, to allow for higher-order integrators that
    # may hop around in time!

    def set_vector_default(self, paramname, value):
        for i in range(len(value)):  #range(self.species):
            if value[i] != None:
                self.default_values[paramname + "[" + str(i) + "]"] = value[i]

    def update(self, sim : SimState):
        assert self.species == sim.species

        for c in self.components:
            c.refresh(sim, self)

    def add_component(self, component):
        self.components.append(component)

        # Register component as a source for everything it generates
        for parameter in component.provides:
            if parameter not in self.providers:
                self.providers[parameter] = set()
            self.providers[parameter].add(component)

        # Register component's default values for any previously unknown variables
        for paraminfo in component.parameters:
            if paraminfo.defvalue != None:
                if paraminfo.vector:
                    for i in range(self.species):
                        indexedname = paraminfo.fullname + "[" + str(i) + "]"
                        if indexedname not in self.default_values:
                            self.default_values[indexedname] = paraminfo.defvalue
                else:
                    if paraminfo.fullname not in self.default_values:
                        self.default_values[paraminfo.fullname] = paraminfo.defvalue

        # Assign component's state variable slots
        # (maybe this should be done like parameters, with global names, to allow direct sharing??)
        for k in component.statevars:
            component.statevars[k] = self.num_statevars
            self.num_statevars += 1

    def generate_component(self, properties : dict):
        name = properties["Name"]
        if any(component.name == name for component in self.components):
            raise ValueError("Duplicate component name " + name)

        type = properties["Type"]
        constructor = self.component_constructors[type]
        newcomponent = constructor(name = name)

        for paraminfo in newcomponent.parameters:
            if paraminfo.paramname in properties:
                paraminfo.defvalue = properties[paraminfo.paramname]
        
        self.add_component(newcomponent)

    def init_statevars(self, sim : SimState):
        if sim.sigma is None:
            sim.allocate_statevars(self.num_statevars)

            for c in self.components:
                pass  # TODO add a handler for components to provide initial state variable values

    def query(self, sim : SimState, parameter : str):
        if parameter in self.providers:
            return sum(c.query(sim, self, parameter) for c in self.providers[parameter])
        else:
            return self.default_values[parameter]

    def query_vector(self, sim : SimState, parameter : str):
        # slow :P
        # would it be better just to de-vectorize things than use this baloney?

        results = [self.query(sim, parameter + "[" + str(i) + "]") for i in range(self.species)]
        if all(isinstance(result, numpy.ndarray) for result in results):
            # bleh
            n = sim.w * sim.h
            return numpy.concatenate([numpy.reshape(result, (n, 1)) for result in results], 1)
        elif any(isinstance(result, numpy.ndarray) for result in results):
            # blehhhh :P
            n = sim.w * sim.h
            ident = numpy.ones(n)
            return numpy.concatenate([numpy.reshape(result * ident, (n, 1)) for result in results], 1)
            # seriously, there's got to be a better way... 
        else:
            return results

        # does not help :P
        #if parameter not in self.qvcache:
        #    paramnames = [parameter + "[" + str(i) + "]" for i in range(self.species)]
        #    #results = [self.query(sim, parameter + "[" + str(i) + "]") for i in range(self.species)]
        #    results = [self.query(sim, paramname) for paramname in paramnames]
        #    if all(isinstance(result, numpy.ndarray) for result in results):
        #        # bleh
        #        n = sim.w * sim.h
        #        #return numpy.concatenate([numpy.reshape(result, (n, 1)) for result in results], 1)
        #        self.qvcache[parameter] = lambda sim: numpy.concatenate([numpy.reshape(self.query(sim, paramname), (n, 1)) for paramname in paramnames], 1)
        #    elif any(isinstance(result, numpy.ndarray) for result in results):
        #        # blehhhh :P
        #        n = sim.w * sim.h
        #        ident = numpy.ones(n)
        #        #return numpy.concatenate([numpy.reshape(result * ident, (n, 1)) for result in results], 1)
        #        ## seriously, there's got to be a better way... 
        #        self.qvcache[parameter] = lambda sim: numpy.concatenate([numpy.reshape(self.query(sim, paramname) * ident, (n, 1)) for paramname in paramnames], 1)
        #    else:
        #        #return results
        #        self.qvcache[parameter] = lambda sim: [self.query(sim, paramname) for paramname in paramnames]
        #return self.qvcache[parameter](sim) # crock!


    def chem_kinetics(self, sim : SimState):
        # return net production rate of chemical species
        return self.query(sim, "rhodot")


    def transmembrane_norton(self, sim : SimState):
        # return tuple of G, I (both dedimensionalized) for transmembrane current equivalent circuit model
        return self.query(sim, "Gm"), self.query(sim, "Im")



class CellComponent: # abstract
    def __init__(self, provides, name = None):
        self.provides = provides
        self.name = name
        self.parameters = []
        self.statevars = {}

    class ParameterInfo:
        def __init__(self, fullname, paramname, defvalue, description, vector, constant):
            self.fullname = fullname
            self.paramname = paramname
            self.defvalue = defvalue
            self.description = description
            self.vector = vector
            self.constant = constant

    def register_parameter(self, scope : str, paramname : str, defvalue = None, description : str = None,
                           vector = False, constant = False):
        fullname = (scope + "." if scope else "") + paramname

        self.parameters.append(CellComponent.ParameterInfo(fullname, paramname, defvalue, description, vector, constant))

        return fullname

    def refresh(self, sim: SimState, param : Parameterizer):
        pass

    def query(self, sim: SimState, param : Parameterizer, paramname : str):
        raise NotImplementedError()

def constructible_component(name):
    def constructible_component_inner(cls):
        Parameterizer.component_constructors[name] = cls
        return cls
    return constructible_component_inner

@constructible_component("AutonomousKinetics")
class AutonomousKineticsModel(CellComponent):
    def __init__(self, decay_order : int = 3, name = None):
        super().__init__(provides = ["rhodot"], name = name)

        #self.orderparam = self.register_parameter(name, "decay_order", decay_order, constant = True) #???
        self.muparam = self.register_parameter(name, "mu", 0, vector = True)
        self.nuparam = self.register_parameter(name, "nu", 0, vector = True)
        self.decay_order = decay_order
        # (more general channels are going to really need private parameter names because thye may appear multiple times... is this good enough?)

    def refresh(self, sim: SimState, param : Parameterizer):
        self.cached = None

    def query(self, sim: SimState, param : Parameterizer, paramname : str):
        assert paramname == "rhodot"

        mu = param.query_vector(sim, self.muparam)
        nu = param.query_vector(sim, self.nuparam)

        drho_dt = -(nu * sim.rho)
        for i in range(self.decay_order - 1):
            drho_dt *= sim.rho
            # actually faster, i think!

        #drho_dt = mu - nu * sim.rho * sim.rho * sim.rho

        # (Already hitting issues with large step sizes + large kinetic params + initial random values -> instability...
        #  Maybe it's time to try stiff / Rosenbrock?)

        drho_dt += mu

        assert drho_dt.shape == sim.rho.shape
        return drho_dt

class ChannelModel(CellComponent): # abstract
    def __init__(self, name = None):
        super().__init__(provides = ["Gm", "Im"], name = name)

        self.cached = None

    def refresh(self, sim: SimState, param : Parameterizer):
        self.cached = None

    def query(self, sim: SimState, param : Parameterizer, paramname : str):
        if self.cached is None:
            self.cached = self.compute_norton(sim, param)

        if paramname == "Gm":
            return self.cached[0]
        elif paramname == "Im":
            return self.cached[1]
        else:
            raise ValueError(paramname)

    def compute_norton(self, sim: SimState, param : Parameterizer):
        # return tuple of G, I (both dedimensionalized) for transmembrane current equivalent circuit model
        raise NotImplementedError()

@constructible_component("LinearChannelModel")
class LinearChannelModel(ChannelModel):
    def __init__(self, name = None):
        super().__init__(name = name)

        self.epsilonparam = self.register_parameter(name, "epsilon", 0.2)
        self.betaparam = self.register_parameter(name, "beta", 5, vector = True)

    def compute_norton(self, sim: SimState, param : Parameterizer):
        epsilon = param.query(sim, self.epsilonparam)
        beta = param.query_vector(sim, self.betaparam)
        gpos = epsilon * numpy.maximum(1 + numpy.sum(beta * sim.rho, 1), 0)   # / (1 + numpy.exp(-4 * (sim.V - .98)))
        gneg = epsilon * 1   # * 1 / (1 + numpy.exp(4 * (sim.V + .5)))
        # tuple of G, I (both dedimensionalized) for transmembrane current equivalent circuit model
        return (gpos + gneg, gpos - gneg)

        #gpos = epsilon * numpy.maximum(1 + numpy.sum(beta * sim.rho, 1), 0) #  / (1 + numpy.exp(-4 * (V - .507)))
        #gneg = epsilon * 1  #* 1 / (1 + numpy.exp(4 * (V + .5)))

        # apparently bistable with default rho config!
        #gpos = epsilon * numpy.maximum(1 + numpy.sum(beta * rho, 1), 0)   / (1 + numpy.exp(-4 * (V - .98)))
        #gneg = epsilon * 1  * 1 / (1 + numpy.exp(4 * (V + .5)))

        # bistable with default seed & beta = 0
        #gpos = epsilon * numpy.maximum(1 + numpy.sum(beta * rho, 1), 0)   / (1 + numpy.exp(-4 * (V - .507)))
        #gneg = epsilon * 1  * 1 / (1 + numpy.exp(4 * (V + .5)))
        # (collapsing regions boundaries gather up a _lot_ of tracer ligand!)

@constructible_component("SimpleVg")
class SimpleVg(ChannelModel):
    def __init__(self, name = None):
        super().__init__(name = name)

        self.epsilonparam = self.register_parameter(name, "vgeps", 1)
        self.threshparam = self.register_parameter(name, "vgthresh", 0)
        self.zparam = self.register_parameter(name, "vgz", 4)
        self.vrelparam = self.register_parameter(name, "vgvrel", 1)
        # (more general channels are going to really need private parameter names because thye may appear multiple times... is this good enough?)
        # (also... more general channels probably want to provide default param values!)
        # (is providing a default through param.query enough, or does it really need to be blessed as an explicit top-level parameter?)

    def compute_norton(self, sim: SimState, param : Parameterizer):
        vgeps = param.query(sim, self.epsilonparam)
        vgthresh = param.query(sim, self.threshparam)
        vgz = param.query(sim, self.zparam)
        vgvrel = param.query(sim, self.vrelparam)
        gpos = vgeps / (1 + numpy.exp(vgz * (sim.V - vgthresh)))
        return (gpos, vgvrel * gpos)


#@constructible_component("HodgHuxVg")
#class HodgHuxVg(ChannelModel):
#    def __init__(self, name = None):
#        super().__init__(name = name)

#        self.epsilonparam = self.register_parameter(name, "hheps", 1)
#        #self.threshparam = self.register_parameter(name, "hhthresh", 0.25)
#        #self.zparam = self.register_parameter(name, "hhz", 10)
#        self.vrelparam = self.register_parameter(name, "hhvrel", 1)

#        self.malphafn = lambda Vmv: (0.1 * (25 - Vmv)) / (numpy.exp((25 - Vmv) / 10) - 1.0)
#        self.mbetafn = lambda Vmv: 4.0 * (numpy.exp(-Vmv / 18))
#        self.halphafn = lambda Vmv: 0.07 * numpy.exp(-Vmv / 20) 
#        self.hbetafn = lambda Vmv: 1 / (numpy.exp((30 - Vmv) / 10) + 1.0) 

#        self.statevars = {"m" : None, "h" : None}
 
#    def compute_norton(self, sim: SimState, param : Parameterizer):
#        hheps = param.query(sim, self.epsilonparam)
#        #vgthresh = param.query(sim, self.threshparam)
#        #vgz = param.query(sim, self.zparam)
#        hhvrel = param.query(sim, self.vrelparam)
#        gpos = hheps * numpy.power(sim.sigma[:, self.statevars["m"]], 3) * sim.sigma[:, self.statevars["h"]]
#        return (gpos, vgvrel * gpos)

#    # state update shit...!
#    # should it be in the form of a query sigmadot thingy? automatically collated by parameterizer?
#    # or more primitive since (for now?) each sigma has only one contributor?
#    # do they even need to be fused? they need to at least be in blocks, though, so co-owned DoFs can co-evolve...
#    # although, hh gates dont even interact except thru v which i prolly can't handle (and which is a slow variable).
#    # just dont support interaction outside of chemical kinetics?

class ParameterGradient(CellComponent):
    def __init__(self, paramname: str, minvalue, maxvalue, axis : int):
        super().__init__(provides = [paramname])

        #self.minvalueparam = self.register_parameter(str(id(self)), "minvalue", minvalue, constant = True) #???
        self.minvalue = minvalue
        self.maxvalue = maxvalue
        self.axis = axis
        self.lastconfig = None


    def refresh(self, sim: SimState, param : Parameterizer):
        if self.lastconfig != (sim, param):
            # overall shape not allowed to change, so if that's all we depend on, then we can cache across refreshes
            self.lastconfig = sim, param

            w = sim.w
            h = sim.h
            n = w * h
            self.gradient = numpy.zeros(n)

            for y in range(0, h):
                for x in range(0, w):
                    i = x + w * y
                    oddrow = y % 2
                    xeff = x + oddrow / 2
                    if self.axis == 0:
                        self.gradient[i] = self.minvalue + xeff / (w + .5) * (self.maxvalue - self.minvalue)
                    elif self.axis == 1:
                        self.gradient[i] = self.minvalue + y / h * (self.maxvalue - self.minvalue)


    def query(self, sim: SimState, param : Parameterizer, paramname : str):
        assert paramname == self.provides[0]
        return self.gradient



def diasum(A : sparse.dia_matrix, B : sparse.dia_matrix):
    # Sum two diagonal sparse matrices, preserving diagonal format (which scipy doesn't do)

    if len(A.offsets) == len(B.offsets) and all(A.offsets == B.offsets):
        # Fast path
        return sparse.dia_matrix((A.data + B.data, A.offsets), A.shape)
    elif len(A.offsets) < len(B.offsets):
        return diasum(B, A)
    else:
        newdata = numpy.array(A.data) # (copies by default)
        newoffsets = numpy.array(A.offsets)
        for i in range(len(B.offsets)):
            for j in range(len(A.offsets) + 1):
                if j >= len(A.offsets):
                    newdata = numpy.concatenate((newdata, [B.data[i,:]]))
                    newoffsets = numpy.concatenate((newoffsets, [B.offsets[i]]))
                elif B.offsets[i] == A.offsets[j]:
                    newdata[j,:] += B.data[i,:]
                    break

        return sparse.dia_matrix((newdata, newoffsets), A.shape)


def multi_minimodel_run(sim : SimState = None, param : Parameterizer = None, itercount = 10000, dt = 0.005, plotinterval = 500,
                        precondition = True, variable_diffusion = False, bcclosure = 0, mask = None, monitorgen = Monitor):

    if sim is None:
        sim = SimState()
    w = sim.w
    h = sim.h
    n = w * h
    species = sim.species
    dx = sim.dx

    if param is None:
        param = Parameterizer(species)
    param.init_statevars(sim)

    #if isinstance(mixmat, list):
    #    mixmat = numpy.matrix(mixmat)
    bcclosure = numpy.ones(species + 1) * bcclosure


    onevec = numpy.ones(n)

    def in_mask(x, y):
        if x < 0 or x >= w or y < 0 or y >= h:
            return False

        oddrow = y % 2
        x_rect = x + oddrow / 2
        y_rect = y * math.sqrt(3) / 2
        w_rect = w + 1/2
        h_rect = h * math.sqrt(3) / 2

        if isinstance(mask, str) and mask == "circle":
            return math.hypot(x_rect - w_rect / 2, y_rect - h_rect / 2)  < min(w_rect / 2, h_rect / 2)
        elif mask is None:
            return True
        else:           
            # Keep mask aspect ratio, truncate on shorter dimension
            xc_rect = x_rect - w_rect / 2
            yc_rect = y_rect - h_rect / 2
            maxdim = max(w_rect, h_rect)
            maskx = round((xc_rect / maxdim + .5) * mask.shape[0])
            masky = round((yc_rect / maxdim + .5) * mask.shape[1]) 

            # Stretch:
            #maskx = round(x_rect / w_rect * mask.shape[0])
            #masky = round(y_rect / h_rect * mask.shape[1]) 

            if maskx < 0 or maskx >= mask.shape[0] or masky < 0 or masky >= mask.shape[1]:
                return False # (Just to be safe)
            return mask[maskx, masky] == 0
            # (if mask comes from scipy.misc.imread, don't forget to transpose it. imread is funny?)


    leftdiag = numpy.zeros(n)
    rightdiag = numpy.zeros(n)
    upleftdiags = [numpy.zeros(n), numpy.zeros(n), None] # -w - 1, -w, -w + 1
    uprightdiags = [None, numpy.zeros(n), numpy.zeros(n)] # -w - 1, -w, -w + 1
    downleftdiags = [numpy.zeros(n), numpy.zeros(n), None] # +w - 1, +w, +w + 1
    downrightdiags = [None, numpy.zeros(n), numpy.zeros(n)] # +w - 1, +w, +w + 1
    outofmask = numpy.zeros(n)
    maindiag_open = numpy.zeros(n)
    maindiag_closed = numpy.zeros(n)
    for y in range(0, h):
        for x in range(0, w):
            i = x + w * y
            oddrow = y % 2
            evenrow = (y + 1) % 2
            neighborcount = 0

            if in_mask(x, y):

                if in_mask(x - 1, y):
                    neighborcount += 1
                    leftdiag[i] = 1
                if in_mask(x + 1, y):
                    neighborcount += 1
                    rightdiag[i] = 1
                if in_mask(x - evenrow, y - 1): # up-left
                    neighborcount += 1
                    upleftdiags[oddrow][i] = 1
                if in_mask(x - evenrow + 1, y - 1): # up-right
                    neighborcount += 1
                    uprightdiags[oddrow + 1][i] = 1
                if in_mask(x - evenrow, y + 1): # down-left
                    neighborcount += 1
                    downleftdiags[oddrow][i] = 1
                if in_mask(x - evenrow + 1, y + 1): # down-right
                    neighborcount += 1
                    downrightdiags[oddrow + 1][i] = 1

                maindiag_open[i] = -6
                maindiag_closed[i] = -neighborcount
                # possibly replace this with a Robin-like boundary condition coefficient

            else:
                #if not in_mask(x, y):
                sim.rho[i, :] = 0
                sim.V[i] = 0

                maindiag_open[i] = -6
                maindiag_closed[i] = -6
                outofmask[i] = 1


            ## masked + CG has horrible issues if you make dx significantly larger than normal

            ## also has issues also on certain small masks, perhaps when wavelength is barely fitting??
            ## reducing dt seems to help un-wedge it

            ## also i think i'm not imagining progressive slowdown issues here with such bad cases!! restarting python helps!?!

            ## preconditioning fixes it??
            ## (almost all the time... but not _all_ the time?)

            ## possibly the extremal values inside compressed tight loops cause trouble? i've once seen one actually go unstable and abort!
            ## this would seem to make sense in conjunction with the dt observation -- excessive dt could be driving the nonlinearities near-unstable
            ## (indeed, just cutting the timestep in half to dt = 0.1 seems to stop such loops from going unstable)
    

    updiags = [upleftdiags[0], upleftdiags[1] + uprightdiags[1], uprightdiags[2]]
    downdiags = [downleftdiags[0], downleftdiags[1] + downrightdiags[1], downrightdiags[2]]

    inmask = 1 - outofmask
    ident_outofmask = sparse.spdiags([outofmask], [0], n, n)
                   
    lap = [None] * (species + 1)
    lapdiags = [None] * (species + 1)
    getleft = [None] * (species + 1)
    getright = [None] * (species + 1)
    getupleft = [None] * (species + 1)
    getupright = [None] * (species + 1)
    getdownleft = [None] * (species + 1)
    getdownright = [None] * (species + 1)
    hasleft = [None] * (species + 1)
    hasright = [None] * (species + 1)
    hasupleft = [None] * (species + 1)
    hasupright = [None] * (species + 1)
    hasdownleft = [None] * (species + 1)
    hasdownright = [None] * (species + 1)
    dleft = [None] * (species + 1)
    dright = [None] * (species + 1)
    dupleft = [None] * (species + 1)
    dupright = [None] * (species + 1)
    ddownleft = [None] * (species + 1)
    ddownright = [None] * (species + 1)

    for k in range(species + 1):
        lapdiags[k] = numpy.array([[*updiags[0][w + 1:], *numpy.zeros(w + 1)],
                                  [*updiags[1][w:], *numpy.zeros(w)],
                                  [*updiags[2][w - 1:], *numpy.zeros(w - 1)],
                                  [*leftdiag[1:], 0],
                                  maindiag_open * (1 - bcclosure[k]) + maindiag_closed * bcclosure[k],
                                  [0, *rightdiag[:n - 1]],
                                  [*numpy.zeros(w - 1), *downdiags[0][:n - (w - 1)]],
                                  [*numpy.zeros(w), *downdiags[1][:n - w]],
                                  [*numpy.zeros(w + 1), *downdiags[2][:n - (w + 1)]]]) / (6 * dx * dx)
        # (positioning the diagonals is confusing as hell because of how spdiags truncates the diagonal data arrays)
    
        lap[k] = sparse.spdiags(lapdiags[k], [-w - 1, -w, -w + 1, -1, 0, 1, w - 1, w, w + 1], n, n)
        # Of note: i don't really see a big perf difference switching to CSC or CSR format


        getleft[k] = sparse.spdiags([[*leftdiag[1:], 0]], [-1], n, n)
        getright[k] = sparse.spdiags([[0, *rightdiag[:n - 1]]], [1], n, n)

        getupleft[k] = sparse.spdiags([[*upleftdiags[0][w + 1:], *numpy.zeros(w + 1)],
                                       [*upleftdiags[1][w:], *numpy.zeros(w)]], [-w - 1, -w], n, n)
        getupright[k] = sparse.spdiags([[*uprightdiags[1][w:], *numpy.zeros(w)],
                                        [*uprightdiags[2][w - 1:], *numpy.zeros(w - 1)]], [-w, -w + 1], n, n)

        getdownleft[k] = sparse.spdiags([[*numpy.zeros(w - 1), *downleftdiags[0][:n - (w - 1)]],
                                         [*numpy.zeros(w), *downleftdiags[1][:n - w]]], [w - 1, w], n, n)
        getdownright[k] = sparse.spdiags([[*numpy.zeros(w), *downrightdiags[1][:n - w]],
                                          [*numpy.zeros(w + 1), *downrightdiags[2][:n - (w + 1)]]], [w, w + 1], n, n)

        hasleft[k] = sparse.spdiags([inmask * (1 - bcclosure[k]) + leftdiag * bcclosure[k]], [0], n, n)
        hasright[k] = sparse.spdiags([inmask * (1 - bcclosure[k]) + rightdiag * bcclosure[k]], [0], n, n)

        hasupleft[k] = sparse.spdiags([inmask * (1 - bcclosure[k]) + (upleftdiags[0] + upleftdiags[1]) * bcclosure[k]], [0], n, n)
        hasupright[k] = sparse.spdiags([inmask * (1 - bcclosure[k]) + (uprightdiags[1] + uprightdiags[2]) * bcclosure[k]], [0], n, n)

        hasdownleft[k] = sparse.spdiags([inmask * (1 - bcclosure[k]) + (downleftdiags[0] + downleftdiags[1]) * bcclosure[k]], [0], n, n)
        hasdownright[k] = sparse.spdiags([inmask * (1 - bcclosure[k]) + (downrightdiags[1] + downrightdiags[2]) * bcclosure[k]], [0], n, n)

        #print(sum(leftdiag))
        #print(sum(rightdiag))
        #print(sum(upleftdiags[0]))
        #print(sum(upleftdiags[1]))
        #print(sum(uprightdiags[1]))
        #print(sum(uprightdiags[2]))
        #print(sum(downleftdiags[0]))
        #print(sum(downleftdiags[1]))
        #print(sum(downrightdiags[1]))
        #print(sum(downrightdiags[2]))
        #print(sum(maindiag_closed))
        #print(sum(maindiag_open))
        #altmd = -(hasleft[k] + hasright[k] + hasupleft[k] + hasupright[k] + hasdownleft[k] + hasdownright[k])
        #print(numpy.sum(abs(sparse.spdiags([maindiag_open * (1 - bcclosure[k]) + maindiag_closed * bcclosure[k]], [0], n, n) - altmd)))


        dleft[k] = diasum(getleft[k], -hasleft[k]) / dx
        dright[k] = diasum(getright[k], -hasright[k]) / dx

        dupleft[k] = diasum(getupleft[k], -hasupleft[k]) / dx
        dupright[k] = diasum(getupright[k], -hasupright[k]) / dx

        ddownleft[k] = diasum(getdownleft[k], -hasdownleft[k]) / dx
        ddownright[k] = diasum(getdownright[k], -hasdownright[k]) / dx


        #dleft[k] = sparse.spdiags([[*leftdiag[1:], 0], -(numpy.ones(n) * (1 - bcclosure[k]) + leftdiag * bcclosure[k])], [-1, 0], n, n) / dx 
        #dright[k] = sparse.spdiags([[0, *rightdiag[:n - 1]], -(numpy.ones(n) * (1 - bcclosure[k]) + rightdiag * bcclosure[k])], [1, 0], n, n) / dx 

        #dupleft[k] = sparse.spdiags([[*upleftdiags[0][w + 1:], *numpy.zeros(w + 1)],
        #                          [*upleftdiags[1][w:], *numpy.zeros(w)],
        #                          -(numpy.ones(n) * (1 - bcclosure[k]) + (upleftdiags[0] + upleftdiags[1]) * bcclosure[k])], [-w - 1, -w, 0], n, n) / dx 
        #dupright[k] = sparse.spdiags([[*uprightdiags[1][w:], *numpy.zeros(w)],
        #                           [*uprightdiags[2][w - 1:], *numpy.zeros(w - 1)],
        #                           -(numpy.ones(n) * (1 - bcclosure[k]) + (uprightdiags[1] + uprightdiags[2]) * bcclosure[k])], [-w, -w + 1, 0], n, n) / dx 

        #ddownleft[k] = sparse.spdiags([[*numpy.zeros(w - 1), *downleftdiags[0][:n - (w - 1)]],
        #                            [*numpy.zeros(w), *downleftdiags[1][:n - w]],
        #                           -(numpy.ones(n) * (1 - bcclosure[k]) + (downleftdiags[0] + downleftdiags[1]) * bcclosure[k])], [w - 1, w, 0], n, n) / dx 
        #ddownright[k] = sparse.spdiags([[*numpy.zeros(w), *downrightdiags[1][:n - w]],
        #                             [*numpy.zeros(w + 1), *downrightdiags[2][:n - (w + 1)]],
        #                             -(numpy.ones(n) * (1 - bcclosure[k]) + (downrightdiags[1] + downrightdiags[2]) * bcclosure[k])], [w, w + 1, 0], n, n) / dx 

        altlap = (dleft[k] + dright[k] + dupleft[k] + dupright[k] + ddownleft[k] + ddownright[k] - 6 / dx * ident_outofmask) / (6 * dx)

        #print(lap[k] - altlap)
        #print(numpy.sum(abs(lap[k] - altlap)))
        #print(numpy.sum(abs(lap[k] * inmask))) # should be zero for closed BCs
        assert numpy.sum(abs(lap[k] * inmask)) < 1e-8 or bcclosure[k] < 1 and numpy.sum(lap[k] * inmask) < 0
        assert numpy.sum(abs(lap[k] - altlap)) < 1e-8


        #global blarg
        #blarg = altlap



    ident = sparse.identity(n)

    monitor = monitorgen()


    #imp_opV = [None] * species
    #imp_opV_inv = [None] * species
    #imp_opV_pc = [None] * species

    #imp_opV = ident + dt * imp_coeff * (lambd[k] ** 2 * lap[k] * lap[k] + 2 * lambd[k] * lap[k] + ((numpy.ones(species) * vx)[k] * ddx))
    ## (I wonder if it affects things that (epsilon - 1) * U is not included in imp_op?)
    #if invmethod == "direct":
    #    #imp_opV_inv = sparse.linalg.inv(imp_opV)
    #    if n * n > 1.0e8:
    #        raise ValueError("Don't be stupid!")
    #    imp_opV_inv = numpy.linalg.inv(imp_opV.todense())

    #if precondition:
    #    imp_opV_pc = scipy.sparse.linalg.LinearOperator((n,n), sparse.linalg.spilu(imp_opV.tocsc()).solve)
    #    # TODO understand this line ;-D


    #imp_opV_const = ident - dt * lap[-1]
    imp_opV_const = diasum(ident, -dt * lap[-1])

    ident_diags = numpy.array([numpy.zeros(n),
                               numpy.zeros(n),
                               numpy.zeros(n),
                               numpy.zeros(n),
                               numpy.ones(n),
                               numpy.zeros(n),
                               numpy.zeros(n),
                               numpy.zeros(n),
                               numpy.zeros(n)])

    imp_opV_const_diags = ident_diags - dt * lapdiags[-1]



    zeros_wide = sparse.spdiags([numpy.zeros(n)] * 9, [-w - 1, -w, -w + 1, -1, 0, 1, w - 1, w, w + 1], n, n)
    zeros_widet = zeros_wide.transpose()

    ident_outofmask_wt = diasum(zeros_wide, ident_outofmask).transpose()
    dleft_wt = [None] * (species + 1)
    dright_wt = [None] * (species + 1)
    dupleft_wt = [None] * (species + 1)
    dupright_wt = [None] * (species + 1)
    ddownleft_wt = [None] * (species + 1)
    ddownright_wt = [None] * (species + 1)
    for k in range(species + 1):
        dleft_wt[k] = diasum(zeros_wide, dleft[k]).transpose()
        dright_wt[k] = diasum(zeros_wide, dright[k]).transpose()
        dupleft_wt[k] = diasum(zeros_wide, dupleft[k]).transpose()
        dupright_wt[k] = diasum(zeros_wide, dupright[k]).transpose()
        ddownleft_wt[k] = diasum(zeros_wide, ddownleft[k]).transpose()
        ddownright_wt[k] = diasum(zeros_wide, ddownright[k]).transpose()
        assert(all(dleft_wt[k].offsets == ident_outofmask_wt.offsets))
    # not clear this helps any!
    # (at least when used with iterated diasum. definitely helps when broken out into a single operation...)


    param.update(sim)
    # TODO if gamma is ever time dependent, then the imp op step needs to be repeated every change!!
    # (if variable_diffusion = True, it redoes it and just gives up on ilu...)

    imp_op_rho = [None] * species
    imp_op_rho_pc = [None] * species
    gamma = param.query_vector(sim, "gamma")
    for k in range(species):
        imp_op_rho[k] = ident - dt * gamma[k] * lap[k]
        # (not obviously faster to compute with if in DIA format)
        if precondition:
            imp_op_rho_pc[k] = scipy.sparse.linalg.LinearOperator((n,n), sparse.linalg.spilu(imp_op_rho[k].tocsc()).solve)
            # TODO understand this line ;-D

    #print(lap[0].__class__) ####
    #print((2*lap[0]).__class__) ####
    #print((lap[0] + lap[0]).__class__) #### BORRK CSR not DIA (maybe that's why it's so slow to add?)
    #print(imp_op_rho[0].__class__) ####
    #print((lap[0] * lap[0]).__class__) #### also CSR :P
    #print((sparse.diags(numpy.ones(n), 0) * lap[0]).__class__) #### also CSR :P


    #homo_opV_approx = dt * (lap[-1]) # - 2 * epsilon * ident)
    #imp_opV_approx = ident - imp_theta * homo_opV_approx

    ##if precondition:
    #imp_opV_pc = scipy.sparse.linalg.LinearOperator((n,n), sparse.linalg.spilu(imp_opV_approx.tocsc()).solve)
    ## TODO understand this line ;-D

    diag_pc_V = sparse.diags(numpy.ones(n), 0)

    warnedneg = False

    #lap_rho = numpy.zeros((n, species))
    divrhogradV = numpy.zeros((n, species))

    divxgradV_op = [None] * species

    tp0 = time.perf_counter()
    for i in range(itercount):

        # expt: < 10% perf impact on 50x50 100sampint 1 layer
        #lap[k] = sparse.spdiags(lapdiags/2 + lapdiags/2, [-w - 1, -w, -w + 1, -1, 0, 1, w - 1, w, w + 1], n, n)
        #lap[k] = sparse.spdiags(lapdiags/2 + lapdiags/2, [-w - 1, -w, -w + 1, -1, 0, 1, w - 1, w, w + 1], n, n)
        #lap[k] = sparse.spdiags(lapdiags/2 + lapdiags/2, [-w - 1, -w, -w + 1, -1, 0, 1, w - 1, w, w + 1], n, n)
        #lap[k] = sparse.spdiags(lapdiags/2 + lapdiags/2, [-w - 1, -w, -w + 1, -1, 0, 1, w - 1, w, w + 1], n, n)

        # (of note: half the time is already plotting overhead)

        # expt: ~ 30% perf impact on same
        #lap[k] = lap[k] / 2 + lap[k] / 2
        #lap[k] = lap[k] / 2 + lap[k] / 2
        #lap[k] = lap[k] / 2 + lap[k] / 2
        #lap[k] = lap[k] / 2 + lap[k] / 2

        # expt: ~ 100% perf impact on same ?!?!
        #mop = lap[k] / 2 + lap[k] / 2
        #mop = lap[k] / 2 + lap[k] / 2
        #mop = lap[k] / 2 + lap[k] / 2
        #mop = lap[k] / 2 + lap[k] / 2

        # expt: ~ 40% perf impact on same
        #mop = lap[k] / 2 + lap[k] / 2
        #mop = mop / 2 + mop / 2
        #mop = mop / 2 + mop / 2
        #mop = mop / 2 + mop / 2

        # expt: ~ 30% perf impact on same. grr.
        #lap[k] = diasum(lap[k] / 2, lap[k] / 2)
        #lap[k] = diasum(lap[k] / 2, lap[k] / 2)
        #lap[k] = diasum(lap[k] / 2, lap[k] / 2)
        #lap[k] = diasum(lap[k] / 2, lap[k] / 2)

        # expt: ~ 30% perf impact on same. grr.
        #mop = diasum(lap[k] / 2, lap[k] / 2)
        #mop = diasum(lap[k] / 2, lap[k] / 2)
        #mop = diasum(lap[k] / 2, lap[k] / 2)
        #mop = diasum(lap[k] / 2, lap[k] / 2)

        # expt: < 10% perf impact on same :P
        #mop = diasum(lap[k], lap[k])
        #mop = diasum(lap[k], lap[k])
        #mop = diasum(lap[k], lap[k])
        #mop = diasum(lap[k], lap[k])

        # expt: ~ 15% perf impact on same
        #npon = numpy.ones(n) # does not make a huge difference
        #mop = diasum(sparse.diags(npon, 0), imp_opV_const)
        #mop = diasum(sparse.diags(npon, 0), imp_opV_const)
        #mop = diasum(sparse.diags(npon, 0), imp_opV_const)
        #mop = diasum(sparse.diags(npon, 0), imp_opV_const)

        # expt: ~ 50% perf impact on same
        #altlap = (diasum(dleft[k], diasum(dright[k], diasum(dupleft[k], diasum(dupright[k], diasum(ddownleft[k], diasum(ddownright[k], -6 / dx * ident_outofmask))))))) / (6 * dx)
        #altlap = (diasum(dleft[k], diasum(dright[k], diasum(dupleft[k], diasum(dupright[k], diasum(ddownleft[k], diasum(ddownright[k], -6 / dx * ident_outofmask))))))) / (6 * dx)
        #altlap = (diasum(dleft[k], diasum(dright[k], diasum(dupleft[k], diasum(dupright[k], diasum(ddownleft[k], diasum(ddownright[k], -6 / dx * ident_outofmask))))))) / (6 * dx)
        #altlap = (diasum(dleft[k], diasum(dright[k], diasum(dupleft[k], diasum(dupright[k], diasum(ddownleft[k], diasum(ddownright[k], -6 / dx * ident_outofmask))))))) / (6 * dx)


        param.update(sim)
        #alpha = param.alpha ## spatial dependence need tweaks to formula??
        #gamma = param.gamma ## note gamma cannot be time-dependent at present, see above (without variable_diffusion = True)
                            ## FIXME this is going to be an issue for gated GJs!
                            ## also cell-to-cell spatial differences and appearance in diffusion term need 
                            ## to be handled carefully. likely need to construct new lap[k] and imp_op_rho
                            ## every iteration.
                            ## need weight vectors for all six cardinal directions. can probably use one shared
                            ## lap for all rho and V, constructed same as originally but with weights broadcast,
                            ## (does the out_of_mask trickery need to be redone to support this? don't think so?)
                            ## as long as different GJs don't have differential chemical selectivity.
                            ## (and also broadcast same weights on advection fluxes.)
                            ## still have to sparse-sum up an imp_op_rho for each k, though. pissy. :P
                            ## (is there some way to use LinearOperator to avoid this, taking a linear combination
                            ##  of the results of existing sparse operators? ..and will it really be faster?)
                            ## (or maybe reconstruct every time with spdiags??)

        alpha = param.query_vector(sim, "alpha")
        gamma = param.query_vector(sim, "gamma")

        netprodrho = param.chem_kinetics(sim)

        Gnorton, Inorton = param.transmembrane_norton(sim)
        # (i wonder if it might improve accuracy to do this after updating rho rather than synchronously?)

        
        lap_V = lap[-1] * sim.V
        dleftV = dleft[-1] * sim.V
        drightV = dright[-1] * sim.V
        dupleftV = dupleft[-1] * sim.V
        duprightV = dupright[-1] * sim.V
        ddownleftV = ddownleft[-1] * sim.V
        ddownrightV = ddownright[-1] * sim.V

        inleft0 = (dleftV < 0) + 0
        inright0 = (drightV < 0) + 0
        inupleft0 = (dupleftV < 0) + 0
        inupright0 = (duprightV < 0) + 0
        indownleft0 = (ddownleftV < 0) + 0
        indownright0 = (ddownrightV < 0) + 0

        if variable_diffusion:
            def gjgate(dvoltage):
                gj_s = 5 #2 #8#3
                gj_thresh = 0.05
                gj_min = 0 #.1
                return gj_min + 1 / (1 + numpy.exp(gj_s * (numpy.abs(dvoltage) - gj_thresh)))

            leftgate = gjgate(dleftV)
            rightgate = gjgate(drightV)
            upleftgate = gjgate(dupleftV)
            uprightgate = gjgate(duprightV)
            downleftgate = gjgate(ddownleftV)
            downrightgate = gjgate(ddownrightV)

            #k_gate = 10
            #leftgate = 1 / (1 + k_gate * dleftV * dleftV)
            #rightgate = 1 / (1 + k_gate * drightV * drightV)
            #upleftgate = 1 / (1 + k_gate * dupleftV * dupleftV)
            #uprightgate = 1 / (1 + k_gate * duprightV * duprightV)
            #downleftgate = 1 / (1 + k_gate * ddownleftV * ddownleftV)
            #downrightgate = 1 / (1 + k_gate * ddownrightV * ddownrightV)
        else:
            leftgate = 1
            rightgate = 1
            upleftgate = 1
            uprightgate = 1
            downleftgate = 1
            downrightgate = 1

        # observation: sub-stepping makes things erroneously blurrier when pushing the advection CFL limit..!!
        # the effect diminishes if you reduce the base step size (even while substepping by the same factor). 
        # ?!?!!
        # is the over-CFL error causing anti-diffusion that counteracts errors in large-step implicit diffusion??
        # how would over-eager diffusive errors even arise??

        adv_substeps = 1
        redo_advection = True
        while redo_advection:
            rhoexp = sim.rho
            for s in range(adv_substeps):

                for k in range(species):
                    #lap_rho[:,k] = lap[k] * rho[:,k]

                    # Make sure sign of gradient is measured correctly for upwind differencing
                    # (i don't know that this here is any more efficient than just recalculating each layer / iter :P )
                    if alpha[k] < 0:
                        inleft = inleft0
                        inright = inright0
                        inupleft = inupleft0
                        inupright = inupright0
                        indownleft = indownleft0
                        indownright = indownright0
                    else:
                        inleft = 1 - inleft0
                        inright = 1 - inright0
                        inupleft = 1 - inupleft0
                        inupright = 1 - inupright0
                        indownleft = 1 - indownleft0
                        indownright = 1 - indownright0


                    leftflux = leftgate * dleftV * (getleft[k] * rhoexp[:,k] * inleft + hasleft[k] * rhoexp[:,k] * (1 - inleft))
                    rightflux = rightgate * drightV * (getright[k] * rhoexp[:,k] * inright + hasright[k] * rhoexp[:,k] * (1 - inright))
                    upleftflux = upleftgate * dupleftV * (getupleft[k] * rhoexp[:,k] * inupleft + hasupleft[k] * rhoexp[:,k] * (1 - inupleft))
                    uprightflux = uprightgate * duprightV * (getupright[k] * rhoexp[:,k] * inupright + hasupright[k] * rhoexp[:,k] * (1 - inupright))
                    downleftflux = downleftgate * ddownleftV * (getdownleft[k] * rhoexp[:,k] * indownleft + hasdownleft[k] * rhoexp[:,k] * (1 - indownleft))
                    downrightflux = downrightgate * ddownrightV * (getdownright[k] * rhoexp[:,k] * indownright + hasdownright[k] * rhoexp[:,k] * (1 - indownright))

                    # todo if i ever revive the alternatives below -- replace rho with rhoexp

                    #maxgradk = 6 * dx / dt / abs(alpha[k]) # This seems to be too aggressive a limit (leading to artifacts) and i don't know why
                    #dleftVm = numpy.maximum(numpy.minimum(dleftV, maxgradk), -maxgradk)
                    #drightVm = numpy.maximum(numpy.minimum(drightV, maxgradk), -maxgradk)
                    #dupleftVm = numpy.maximum(numpy.minimum(dupleftV, maxgradk), -maxgradk)
                    #duprightVm = numpy.maximum(numpy.minimum(duprightV, maxgradk), -maxgradk)
                    #ddownleftVm = numpy.maximum(numpy.minimum(ddownleftV, maxgradk), -maxgradk)
                    #ddownrightVm = numpy.maximum(numpy.minimum(ddownrightV, maxgradk), -maxgradk)
                    #leftflux = dleftVm * (getleft[k] * rho[:,k] * inleft + hasleft[k] * rho[:,k] * (1 - inleft))
                    #rightflux = drightVm * (getright[k] * rho[:,k] * inright + hasright[k] * rho[:,k] * (1 - inright))
                    #upleftflux = dupleftVm * (getupleft[k] * rho[:,k] * inupleft + hasupleft[k] * rho[:,k] * (1 - inupleft))
                    #uprightflux = duprightVm * (getupright[k] * rho[:,k] * inupright + hasupright[k] * rho[:,k] * (1 - inupright))
                    #downleftflux = ddownleftVm * (getdownleft[k] * rho[:,k] * indownleft + hasdownleft[k] * rho[:,k] * (1 - indownleft))
                    #downrightflux = ddownrightVm * (getdownright[k] * rho[:,k] * indownright + hasdownright[k] * rho[:,k] * (1 - indownright))
                    # (mind the order of multiplications, stupidly overloaded operator is stupidly overloaded :P )

                    #leftflux = dleftV * (getleft[k] * rho[:,k] + hasleft[k] * rho[:,k]) / 2
                    #rightflux = drightV * (getright[k] * rho[:,k] + hasright[k] * rho[:,k]) / 2
                    #upleftflux = dupleftV * (getupleft[k] * rho[:,k] + hasupleft[k] * rho[:,k]) / 2
                    #uprightflux = duprightV * (getupright[k] * rho[:,k] + hasupright[k] * rho[:,k]) / 2
                    #downleftflux = ddownleftV * (getdownleft[k] * rho[:,k] + hasdownleft[k] * rho[:,k]) / 2
                    #downrightflux = ddownrightV * (getdownright[k] * rho[:,k] + hasdownright[k] * rho[:,k]) / 2

                    divrhogradV[:,k] = (leftflux + rightflux + upleftflux + uprightflux + downleftflux + downrightflux) / (6 * dx)
                    # (...am i sure the normalization is correct? seems so, same as laplacian...)


                    ##leftfluxop = sparse.diags(dleftV, 0) * (sparse.diags(inleft, 0) * getleft[k] + sparse.diags(1 - inleft, 0) * hasleft[k])
                    ##rightfluxop = sparse.diags(drightV, 0) * (sparse.diags(inright, 0) * getright[k] + sparse.diags((1 - inright), 0) * hasright[k])
                    ##upleftfluxop = sparse.diags(dupleftV, 0) * (sparse.diags(inupleft, 0) * getupleft[k] + sparse.diags((1 - inupleft), 0) * hasupleft[k])
                    ##uprightfluxop = sparse.diags(duprightV, 0) * (sparse.diags(inupright, 0) * getupright[k] + sparse.diags((1 - inupright), 0) * hasupright[k])
                    ##downleftfluxop = sparse.diags(ddownleftV, 0) * (sparse.diags(indownleft, 0) * getdownleft[k] + sparse.diags((1 - indownleft), 0) * hasdownleft[k])
                    ##downrightfluxop = sparse.diags(ddownrightV, 0) * (sparse.diags(indownright, 0) * getdownright[k] + sparse.diags((1 - indownright), 0) * hasdownright[k])
          
                    #leftfluxop = sparse.diags(dleftV * inleft, 0) * getleft[k] + sparse.diags(dleftV * (1 - inleft), 0) * hasleft[k]
                    #rightfluxop = sparse.diags(drightV * inright, 0) * getright[k] + sparse.diags(drightV * (1 - inright), 0) * hasright[k]
                    #upleftfluxop = sparse.diags(dupleftV * inupleft, 0) * getupleft[k] + sparse.diags(dupleftV * (1 - inupleft), 0) * hasupleft[k]
                    #uprightfluxop = sparse.diags(duprightV * inupright, 0) * getupright[k] + sparse.diags(duprightV * (1 - inupright), 0) * hasupright[k]
                    #downleftfluxop = sparse.diags(ddownleftV * indownleft, 0) * getdownleft[k] + sparse.diags(ddownleftV * (1 - indownleft), 0) * hasdownleft[k]
                    #downrightfluxop = sparse.diags(ddownrightV * indownright, 0) * getdownright[k] + sparse.diags(ddownrightV *(1 - indownright), 0) * hasdownright[k]

                    #divxgradV_op[k] = (leftfluxop + rightfluxop + upleftfluxop + uprightfluxop + downleftfluxop + downrightfluxop) / (6 * dx)

                    # Fucking hideous operator preparation overhead! much more than the actual CG!!

                    # This works fine but at least for now i've moved back to explicit for the advection term, since CG starts to bog down if you push
                    # step size too much anyway... apparently can still use not-quite-bogged-down step sizes just with implicit diffusion.
                    # situations where fully implicit CG gets bogged down seem like they may correspond to explicit advection going unstable



                    ##print(leftflux)
                    ##print(leftfluxop * rho[:,k])
                    ##print(numpy.sum(abs(leftflux - leftfluxop * rho[:,k])))
                    #assert(numpy.sum(abs(leftflux - leftfluxop * rho[:,k])) < 1e-8)

                    ##print(divrhogradV[:,k],  divxgradV_op[k] * rho[:,k])
                    ##print(numpy.sum(abs(divrhogradV[:,k] - divxgradV_op[k] * rho[:,k])))
                    #assert(numpy.sum(abs(divrhogradV[:,k] - divxgradV_op[k] * rho[:,k])) < 1e-8)



                #rho = rho + dt * (alpha * divrhogradV + mu - nu * rho * rho * rho + gamma * lap_rho);

                #V = V + dt * ((1 - V) * gpos + (-1 - V) * gneg + lap_V)


                #rhoexp = rho + dt * (mu - nu * rho * rho * rho)

                #for k in range(species):
                #    imp_op_rho = ident - dt * alpha[k] * divxgradV_op[k] - dt * gamma[k] * lap[k]
                #    (cgresult, cginfo) = sparse.linalg.cg(imp_op_rho, rhoexp[:,k], rho[:,k])
                #    rho[:,k] = numpy.array(cgresult)


                rhoexp = rhoexp + dt / adv_substeps * (alpha * divrhogradV + netprodrho);
                #rhoexp = rhoexp + dt / adv_substeps * (alpha * divrhogradV + mu - nu * sim.rho * sim.rho * sim.rho);
                #rhoexp = rhoexp + dt / adv_substeps * (alpha * divrhogradV + mu - nu * rhoexp * rhoexp * rhoexp);

                # (might use rhoexp instead of rho in nonlinear term... but the accuracy issues with substepping
                #  are already so bad it probabl doesn't matter :P )
                # (also, i've observed that in extreme cases actually causing CG failure later. ?! )
                # (and other times rescuing it from negative values..!)
                # (chemical stiffness and intermediate advective negatives might be an issue with some nonlinarities, 
                #  but not these...)
                # (of course, stiff nonlinearities should be done implicitly anyway :P )
           
               

            #if (rhoexp < 0).any() and not warnedneg:
            #    print("Warning: negative intermediate concentration at iteration", i)
            #    warnedneg = True

            #if all(rhoexp >= 0):
            #    # Acceptable answer
            #    break
            #else:
            #    adv_substeps *= 2

            #if adv_substeps > 1000:
            #    print("Warning: gave up trying to avoid negative intermediate concentration at iteration", i)
            #    break
            redo_advection = False

            for k in range(species):

                if variable_diffusion:
                    # (possibly move up outside of advection loop... lose locality but avoid pointless redos on advection retries)
                    #lap[k] = (diasum(leftgate * dleft[k], diasum(rightgate * dright[k], diasum(upleftgate * dupleft[k], diasum(uprightgate * dupright[k], diasum(downleftgate * ddownleft[k], diasum(downrightgate * ddownright[k], -6 / dx * ident_outofmask_wide))))))) / (6 * dx)
                    #lap[k] = (diasum(leftgate * dleft_w[k], diasum(rightgate * dright_w[k], diasum(upleftgate * dupleft_w[k], diasum(uprightgate * dupright_w[k], diasum(downleftgate * ddownleft_w[k], diasum(downrightgate * ddownright_w[k], -6 / dx * ident_outofmask_w))))))) / (6 * dx)
                    #oldlap = lap[k]
                    #lap[k] = sparse.dia_matrix(((leftgate * dleft_w[k].data + rightgate * dright_w[k].data + upleftgate * dupleft_w[k].data + uprightgate * dupright_w[k].data + downleftgate * ddownleft_w[k].data + downrightgate * ddownright_w[k].data - 6 / dx * ident_outofmask_w.data) / (6 * dx),
                    #                           zeros_wide.offsets), zeros_wide.shape)
                    lap[k] = sparse.dia_matrix(((leftgate * dleft_wt[k].data + rightgate * dright_wt[k].data + upleftgate * dupleft_wt[k].data + uprightgate * dupright_wt[k].data + downleftgate * ddownleft_wt[k].data + downrightgate * ddownright_wt[k].data - 6 / dx * ident_outofmask_wt.data) / (6 * dx),
                                               zeros_widet.offsets), zeros_widet.shape).transpose()
                    #assert numpy.sum(abs(lap[k] - oldlap)) < 1e-8  #<-- sanity check valid only if non-variable

                    # ^-- super-expensive, the rest below is trivial

                    imp_op_rho[k] = diasum(ident, -dt * param.gamma[k] * lap[k])
                    diag_pc_rho = sparse.diags(1 / imp_op_rho[k].diagonal(), 0)
                    imp_op_rho_pc[k] = diag_pc_rho

                #diag_pc_rho = sparse.diags(1 / imp_op_rho[k].diagonal(), 0)
                #(cgresult, cginfo) = sparse.linalg.cg(imp_op_rho[k], rhoexp[:,k], sim.rho[:,k], M = diag_pc_rho) #, tol = 1e-8)

                #imp_op_rho = ident - dt * gamma[k] * lap[k]
                (cgresult, cginfo) = sparse.linalg.cg(imp_op_rho[k], rhoexp[:,k], sim.rho[:,k], M = imp_op_rho_pc[k]) #, tol = 1e-8)

                if any(cgresult < 0) and adv_substeps < 1000:
                    if not warnedneg:
                        print("Warning: negative candidate concentration at iteration", i, "-- retrying")
                        warnedneg = True

                    adv_substeps *= 2
                    redo_advection = True
                    break

                rhoexp[:,k] = numpy.array(cgresult)

                # not clear whether preconditioning helps meaningfully here, but it doesn't hurt

                if math.isnan(numpy.sum(rhoexp[:,k])) or cginfo != 0:
                    print("Error: concentration CG failed at iteration", i)
                    return (sim.V, rhoexp)

        sim.rho = rhoexp

        if (sim.rho < 0).any():

            # # i am getting lots of failures here with poor man's inhibitory ligand, and i don't know why
            # oops, downwind differencing :P 
            
            print(adv_substeps)
            if (sim.rho > -sum(sim.rho, 0) / sum(inmask) * 0.001).all():
                print("Warning: small negative concentration at iteration", i)
                # (this may not be necessary anymore)
            else:
                print("Error: negative concentration at iteration", i)
                print(numpy.sum(sim.rho, 0), max(numpy.abs(alpha)) * max(numpy.max(abs(dleftV)), numpy.max(abs(drightV)), numpy.max(abs(dupleftV)), numpy.max(abs(duprightV)), numpy.max(abs(ddownleftV)), numpy.max(abs(ddownrightV))))
                return (sim.V, sim.rho)

        # wtf i'm getting small (~10^-4) diffusive transport errors??
        # it's the preconditioner!
        # may be a coincidence, though, as the transport errors go with the tol parameter, so
        # preconditioning may simply be letting it quit sooner without incidentally hammering
        # the DC error down to zero
        # happens with a plain diagonal Jacobi preconditioner too, possibly even worse! maybe
        # preconditioning breaks accidental linear invariant preservation or something?


        # V = V + dt * ((1 - V) * gpos + (-1 - V) * gneg + lap_V)


        #homo_opV = dt * (lap[-1] - sparse.diags(gpos + gneg, 0))
        #inhomo_V = dt * (gpos - gneg)
        ##imp_opV = ident - imp_theta * homo_opV
        #imp_opV = ident - homo_opV

        #imp_opV = sparse.diags(dt * (gpos + gneg), 0) + imp_opV_const
        
        inhomo_V = dt * Inorton
        #imp_opV = sparse.diags(dt * Gnorton, 0) + imp_opV_const

        #imp_opV_diags = dt * ident_diags * Gnorton + imp_opV_const_diags
        ## possibly could speed up with fewer dead zeros carried along re ident_diags and such...
        ## but this is already faster than the old sparse operator logic that expanded to CSR!
        #imp_opV = sparse.spdiags(imp_opV_diags, [-w - 1, -w, -w + 1, -1, 0, 1, w - 1, w, w + 1], n, n)

        if variable_diffusion:
            #lap[-1] = (diasum(leftgate * dleft[-1], diasum(rightgate * dright[-1], diasum(upleftgate * dupleft[-1], diasum(uprightgate * dupright[-1], diasum(downleftgate * ddownleft[-1], diasum(downrightgate * ddownright[-1], -6 / dx * ident_outofmask_wide))))))) / (6 * dx)
            #lap[-1] = (diasum(leftgate * dleft_w[-1], diasum(rightgate * dright_w[-1], diasum(upleftgate * dupleft_w[-1], diasum(uprightgate * dupright_w[-1], diasum(downleftgate * ddownleft_w[-1], diasum(downrightgate * ddownright_w[-1], -6 / dx * ident_outofmask_w))))))) / (6 * dx)
            #lap[-1] = sparse.dia_matrix(((leftgate * dleft_w[-1].data + rightgate * dright_w[-1].data + upleftgate * dupleft_w[-1].data + uprightgate * dupright_w[-1].data + downleftgate * ddownleft_w[-1].data + downrightgate * ddownright_w[-1].data - 6 / dx * ident_outofmask_w.data) / (6 * dx),
            #                            zeros_wide.offsets), zeros_wide.shape)
            lap[-1] = sparse.dia_matrix(((leftgate * dleft_wt[-1].data + rightgate * dright_wt[-1].data + upleftgate * dupleft_wt[-1].data + uprightgate * dupright_wt[-1].data + downleftgate * ddownleft_wt[-1].data + downrightgate * ddownright_wt[-1].data - 6 / dx * ident_outofmask_wt.data) / (6 * dx),
                                        zeros_widet.offsets), zeros_widet.shape).transpose()
            # ^-- super-expensive, the rest below is trivial

            imp_opV_const = diasum(ident, -dt * lap[-1])


        imp_opV = diasum(sparse.diags(dt * Gnorton, 0), imp_opV_const)


        ## (I wonder if it affects things that (epsilon - 1) * U is not included in imp_op?)
        #if invmethod == "direct":
        #    #imp_opV_inv = sparse.linalg.inv(imp_opV)
        #    if n * n > 1.0e8:
        #        raise ValueError("Don't be stupid!")
        #    imp_opV_inv = numpy.linalg.inv(imp_opV.todense())

        #if precondition:
        #    imp_opV_pc = scipy.sparse.linalg.LinearOperator((n,n), sparse.linalg.spilu(imp_opV.tocsc()).solve)
        #    # TODO understand this line ;-D


        #diag_pc_V = sparse.diags(1 / imp_opV.diagonal(), 0)
        # unclear whether this helps. pretty close to a wash, in any case.
        diag_pc_V.data[0,:] = 1 / imp_opV.diagonal()  # measurably slightly less overhead

        #Vexp = V + homo_opV * V * (1 - imp_theta) + inhomo_V
        Vexp = sim.V + inhomo_V
        (cgresult, cginfo) = sparse.linalg.cg(imp_opV, Vexp, sim.V, M = diag_pc_V) #, M = imp_opV_pc)
        #(cgresult, cginfo) = sparse.linalg.gmres(imp_opV, Vexp, sim.V, M = diag_pc_V) #, M = imp_opV_pc)
        # gmres seems to be a tad faster on small problems, cg on large problems. odd.
        # (differences in error measurement might account for the apparent gmres speedup, though..?)
        sim.V = numpy.array(cgresult)

        if math.isnan(numpy.sum(sim.V)) or cginfo != 0:
            print("Error: voltage CG failed at iteration", i)
            return (sim.V, sim.rho)

        if any(sim.V > 1.001) or any(sim.V < -1.001):
            # TODO this'll be useless with more complex parameterization
            print("Error: voltage out of range at iteration", i)
            return (sim.V, sim.rho)

 
        ## (The overloading of matrix multiply and elementwise multiply kinda makes me queasy. Is there a better way?)

        #Uexp = U + dt * ((epsilon - numpy.ones(species)) - (U * U) + g2 * U + c * lap_U * U) * U + dt * exp_coeff * (-lambd * lambd * lap2_U - 2 * lambd * lap_U - (vx * ddx_U)) + dt * U * mixmat
        #if math.isnan(numpy.sum(Uexp)):
        #    return U

        #if method == None:
        #    # # FIXME: with FTCS and mixmat you get an error because Uexp is converted to a matrix and not converted back!
        #    U = numpy.array(Uexp)
        #else:
        #    # (see notes in sh1.py about perf... have not repeated experiments with shh1, but larger explicit dt gives it a slight relative advantage unless you shrink dx)
        #    # (but also the preconditioner added here helps, even outside the pathologies it was introduced in hopes of fixing!)

        #    for k in range(species):
        #        if invmethod == "cg":
        #            if precondition:
        #                (cgresult, cginfo) = sparse.linalg.cg(imp_op[k], Uexp[:,k], U[:,k], M = imp_op_pc[k])
        #                # (Of note: adding nonlinearity to the operator e.g. variable drift velocity does not seem to be compatible with a precomputed preconditioner)
        #            else:
        #                (cgresult, cginfo) = sparse.linalg.cg(imp_op[k], Uexp[:,k], U[:,k])
        #            U[:,k] = numpy.array(cgresult)
        #        elif invmethod == "direct":
        #            U[:,k] = numpy.array(numpy.transpose(imp_op_inv[k] * numpy.transpose(numpy.asmatrix(Uexp[:,k]))))[0]
        #            # WTF isn't there a better way to do a plain old matrix multiply???
                
        #            # (In any case this is only faster than CG on _tiny_ substrates, like 20x20!)
        #        else:
        #            U[:,k] = numpy.array(sparse.linalg.spsolve(imp_op[k], Uexp[:,k]))

        if monitor and (i + 1) % plotinterval == 0:
            #monitor(numpy.reshape(numpy.array([V, *rho]), (h, w, species + 1)), in_mask)
            monitor.update(numpy.reshape(numpy.concatenate((numpy.reshape(sim.V, (h * w, 1)), sim.rho), 1), (h, w, species + 1)), in_mask)

            #print(dleftV)
            #print(getleft[k] * drightV)
            #print(inleft)
            #print(getleft[k] * inright)
            #print(rho[:,k])
            #print(hasleft[k] * rho[:,k])
            #print(getleft[k] * rho[:,k])
            #print(leftflux)
            #print(getleft[k] * rightflux)
            
            print(numpy.sum(sim.rho, 0), max(numpy.abs(alpha)) * max(numpy.max(abs(dleftV)), numpy.max(abs(drightV)), numpy.max(abs(dupleftV)), numpy.max(abs(duprightV)), numpy.max(abs(ddownleftV)), numpy.max(abs(ddownrightV))))
            warnedneg = False

            #print(numpy.min(imp_opV.diagonal()), numpy.max(imp_opV.diagonal()), numpy.min(imp_op_rho[0].diagonal()), numpy.max(imp_op_rho[0].diagonal()))

    print(str(time.perf_counter() - tp0) + " sec elapsed")
    #return (sim.V, sim.rho)
    return sim



# gabee.minimodel.multi_minimodel_run(bcclosure=1, w=100, h=100, epsilon = 0.1, itercount=1000, dt = 10.24, plotinterval=25)
# gabee.minimodel.multi_minimodel_run(bcclosure=1, w=25, h=25, epsilon = 0.1, mu=[.011], nu=[.0068], itercount=1000, dt = 10.24, plotinterval=100   )
# gabee.minimodel.multi_minimodel_run(bcclosure=1, w=100, h=100, itercount=1000, dt = 1.28, epsilon=0.4, mu=[.011, 0], nu=[.0068, 0], plotinterval=25, species = 2, alpha=[-1.5, 1.5], beta=[5,-1], gamma=[0.1,0.3])



def bitsy_run_config(config : dict, basepath : str):
    speciesnames = []
    species = {}


    ss_args = {}
    param_args = {}
    sim_args = {}

    def grab_arg(kwargs, argname, shortname = None):
        if argname in config:
            kwargs[shortname if shortname else argname] = config[argname]

    grab_arg(sim_args, "itercount")
    grab_arg(sim_args, "sampling interval", "plotinterval")
    grab_arg(ss_args, "cell size", "dx")
    grab_arg(ss_args, "width", "w")
    grab_arg(ss_args, "height", "h")

    sim_args["dt"] = config.get("time step", 1.0)


    #if config.get("closed BC"):
    #    kwargs["bc"] = "closed"

    if "mask file" in config:
        sim_args["mask"] = scipy.misc.imread(os.path.join(basepath, config["mask file"]), flatten=1).transpose()

    vmem_bcclosure = 0
    if "vmem" in config:
        vmem_bcclosure = config["vmem"].get("boundary closure", vmem_bcclosure)
        param_args["epsilon"] = config["vmem"].get("epsilon", 0.2)
    
    species_param_vectors = set()
    for speciescfg in config["species"]:
        speciesname = speciescfg["name"]
        speciesnames.append(speciesname)
        species[speciesname] = {}

        for speciesparam in speciescfg:
            species[speciesname][speciesparam] = speciescfg[speciesparam]
            species_param_vectors.add(speciesparam)

        # Special legacy handling for these, for default values...
        species[speciesname]["alpha"] = speciescfg.get("alpha", -1.5)
        species[speciesname]["gamma"] = speciescfg.get("gamma", 0.1)
        ##species[speciesname]["mu"] = speciescfg.get("mu", 0)
        ##species[speciesname]["nu"] = speciescfg.get("nu", 0)
        ##species[speciesname]["beta"] = speciescfg.get("beta", 5)
        species[speciesname]["bcclosure"] = speciescfg.get("boundary closure", 0)  # (minor namespace aliasing hazard...)
        #species[speciesname]["mixmat"] = speciescfg.get("activations", 0) # <-- this is terrible

    ##param_args["alpha"] = [species[l]["alpha"] for l in speciesnames]
    ##param_args["gamma"] = [species[l]["gamma"] for l in speciesnames]
    ##param_args["mu"] = [species[l]["mu"] for l in speciesnames]
    ##param_args["nu"] = [species[l]["nu"] for l in speciesnames]
    ##param_args["beta"] = [species[l]["beta"] for l in speciesnames]
    param_args["vector_defaults"] = {parmname : [species[l][parmname] if parmname in species[l] else None
                                                 for l in speciesnames]
                                     for parmname in species_param_vectors}
    sim_args["bcclosure"] = [*[species[l]["bcclosure"] for l in speciesnames], vmem_bcclosure]
    #kwargs["mixmat"] = [numpy.ones(len(species)) * species[l]["mixmat"] for l in speciesnames]

    print(ss_args, flush = True)
    print(param_args, flush = True)
    print(sim_args, flush = True)

    saveimgname = os.path.join(basepath, "bitsy_plot_frame_")
    savedataname = os.path.join(basepath, "bitsy_data_frame_")

    sim = SimState(species = len(species), **ss_args)
    param = Parameterizer(species = len(species), **param_args)

    if "components" in config:
        for componentcfg in config["components"]:
            param.generate_component(componentcfg)


    U = multi_minimodel_run(sim, param, 
                           monitorgen = lambda: Monitor(saveimgname, savedataname), **sim_args)

    if isinstance(U, tuple):
        raise Exception("Simulation went unstable")
    assert isinstance(U, SimState)
    return U


if __name__ == '__main__':
    argparser = argparse.ArgumentParser(prog = "python -m gabee.bitsy", description = "Run Bitsy bio-electric mini-simulation")

    argparser.add_argument("configfile") 
    argparser.add_argument("--headless", action = "store_true") 

    args = argparser.parse_args()


    # Matplotlib is awful!
    import matplotlib
    if args.headless:
        matplotlib.use('Agg')
    import matplotlib.pyplot as plt

    with open(args.configfile, mode='rt') as f:
        config = yaml.load(f)

    bitsy_run_config(config, os.path.dirname(args.configfile))
else:
    import matplotlib
    import matplotlib.pyplot as plt
