## GABEE tools for analyzing simulation outputs, constructing fitness functions
##
## (C) Micah Z. Brodsky 2016-
##


import os
import math
import numbers
import functools
from functools import reduce
import operator
import copy

import gabee.simctl as simctl
from gabee.util import *


class simoutputview:
    # Class representing the current view of a simulation run's output data; immutable

    def __init__(self, simoutputs : dict, indiv_config : dict):
        self.data = simoutputs[None]  # fish out the (hackily defined) default assay results
        self.alloutputs = simoutputs  # map subdir -> simctl.SimOutput
        self.indiv_config = indiv_config
        self.cur_frame = -1
        self.cur_field = self.data.default_field
        self.domain_mask = simoutputview.no_mask  # (this is defined later)
        self.bindings = {}

    def _clone(self):
        # Shallow copy because we don't want to duplicate self.data. Non-public because 
        # nobody should ever mutate a simoutputview that has been released into the wild
        # (which would break memoization).
        return copy.copy(self)

    def gettrait(self, trait_name):
        if not self.indiv_config:
            raise Exception("Fitness function requested trait configuration but it is not available")            
        return self.indiv_config[trait_name][0]["Value"]

    def with_binding(self, key, value):
        new_view = self._clone()
        new_view.bindings = self.bindings.copy()
        new_view.bindings[key] = value
        return new_view

    def lookup_binding(self, key):
        return self.bindings[key]

    def atframe(self, new_frame):
        new_view = self._clone()
        new_view.cur_frame = new_frame
        return new_view

    def atfield(self, new_field):
        new_view = self._clone()
        new_view.cur_field = new_field
        return new_view

    def atassay(self, new_assay):
        if new_assay not in self.alloutputs:
            raise Exception("Fitness function requested unavailable assay outputs: " + new_assay)
        new_view = self._clone()
        new_view.data = self.alloutputs[new_assay]
        return new_view

    def within(self, domain_mask):
        new_view = self._clone()
        new_view.domain_mask = domain_mask * self.domain_mask

        if len(new_view.positions()) == 0:
            eprint("Warning: No cells inside 'within' domain mask!")

        return new_view

    def getvalue(self, position, delta_frame = 0, field = None):
        field = field or self.cur_field
        frame = self.cur_frame + delta_frame

        if frame >= len(self.data.output_fields[field]) or frame < -len(self.data.output_fields[field]):
            # Catchable exception, since this situation can happen due to truncated data
            # (whereas missing fields are usually a mistake and missing positions probably indicate a serious bug)
            raise MissingDataException()

        value = self.data.output_fields[field][frame][position]
        assert value >= 0 or value < 0  # sanity check on type
        return value

    def positions(self, delta_frame = 0, field = None, mask = None):
        # Fetch all cell positions within the domain mask

        field = field or self.cur_field
        frame = self.cur_frame + delta_frame
        mask = mask or self.domain_mask

        if frame >= len(self.data.output_fields[field]) or frame < -len(self.data.output_fields[field]):
            # Catchable exception, see above
            raise MissingDataException()

        return {p for p in self.data.output_fields[field][frame].keys() if mask(self, p)}

    def neighbor_positions(self, pos, delta_frame = 0):
        # Neighbors according to topology (excluding self)
        return self.data.topologies[self.cur_frame + delta_frame].get_neighbors(pos)

        # (possibly need MissingDataException here and below as well? hasn't come up, but...)
    
    def neighborhood_positions(self, pos, range, delta_frame = 0):
        # Neighborhood members (connected according to topology) within range, including self
        return self.data.topologies[self.cur_frame + delta_frame].get_neighborhood_within_range(pos, range)

    def frames(self):
        return range(0, len(self.data.output_fields[self.cur_field]))

class MissingDataException(Exception):
    pass

class OperatorFunc:
    # Base class for arithmetic-friendly functional operators (automatic lambda lifting)

    def __init__(self, function):
        self.function = function
        self.__name__ = function.__name__
        self.metadata = {}  # key -> (value, merger fn)
    
    def __call__(self, *args, **kwargs):
        return self.function(*args, **kwargs)
    
    def __str__(self):
        # (kinda kludgy all this name business)
        return self.__name__

    def unary_lift(self, func):
        # Generate a new OperatorFunc (or subclass) representing the composition of a function with this one.
        result = self.__class__(lambda *args, **kwargs: func(self(*args, **kwargs)))
        result.__name__ = func.__name__ + "(" + str(self) + ")"
        result.attach_metadata(func, self)
        return result

    def binary_lift(self, func, other):
        # Generate a new OperatorFunc (or subclass) representing the application of a binary operator to 
        # this and something else.
        if issubclass(self.__class__, other.__class__):
            # Promote to the more general type
            # (Not bothering to support the case where that would be some distinct common ancestor,
            #  as we only have a linear type hierarchy here)
            targetclass = other.__class__
        else:
            targetclass = self.__class__ 

        result = targetclass(lambda *args, **kwargs:
                             func(self(*args, **kwargs),
                             (other(*args, **kwargs) if isinstance(other, targetclass) else other)))
        result.__name__ = func.__name__ + "(" + str(self) + "," + str(other) + ")"
        result.attach_metadata(func, self, other)
        return result

    # Adapters for outside construction of lifted operations:
    @staticmethod
    def static_unary_lifter(func):
        def static_unary_lift(value):
            if isinstance(value, OperatorFunc):
                return value.unary_lift(func)
            else:
                return func(value)
        return static_unary_lift

    @staticmethod
    def static_binary_lifter(func):
        def static_binary_lift(value1, value2):
            if isinstance(value1, OperatorFunc):
                return value1.binary_lift(func, value2)
            elif isinstance(value2, OperatorFunc):
                return value2.binary_lift(lambda v2, v1: func(v1, v2), value1)
            else:
                return func(value1, value2)
        return static_binary_lift

    def query_metadata(self, key, default = None):
        if key in self.metadata:
            value, _ = self.metadata[key]
            return value
        else:
            return default

    def attach_metadata(self, node_operation, *child_ops):
        # just a simple key by key merge for now, and ignore absent values
        for op in child_ops:
            if isinstance(op, OperatorFunc):
                for key in op.metadata:
                    newvalue, newmerger = op.metadata[key]
                    self.adjoin_metadata_field(key, newvalue, newmerger)

    def adjoin_metadata_field(self, key, newvalue, newmerger):
        # merge in a single key/value pair in the default fashion
        # consider the merger to be part of the type of the value; it must be the same across all merged values
        if key in self.metadata:
            oldvalue, merger = self.metadata[key]
            assert merger == newmerger
            self.metadata[key] = merger(oldvalue, newvalue), merger
        else:
            self.metadata[key] = newvalue, newmerger

    @staticmethod
    def carry_metadata(*childops):
        def carrymeta_decorator_inner(op : OperatorFunc):
            op.attach_metadata(op.function, *childops)
            return op
        return carrymeta_decorator_inner

    @staticmethod
    def with_metadata_field(key, value, merger):
        def with_metadata_inner(op):
            op.adjoin_metadata_field(key, value, merger)
            return op
        return with_metadata_inner

    # Python built-in operations:
    def __add__(self, other):
        return self.binary_lift(operator.add, other)    
    def __sub__(self, other):
        return self.binary_lift(operator.sub, other)    
    def __mul__(self, other):
        return self.binary_lift(operator.mul, other)    
    def __truediv__(self, other):
        return self.binary_lift(operator.truediv, other)    
    def __pow__(self, other):
        return self.binary_lift(operator.pow, other)    

    def __neg__(self):
        return self.unary_lift(operator.neg)    
    def __pos__(self):
        return self.unary_lift(operator.pos)    

    def __radd__(self, other):
        return self + other
    def __rsub__(self, other):
        return -self + other
    def __rmul__(self, other):
        return self * other
    def __rtruediv__(self, other):
        return self.binary_lift(lambda a, b: b / a, other)   # fixme bad autoname?
    def __rpow__(self, other):
        return self.binary_lift(lambda a, b: b ** a, other)   # fixme bad autoname?

    def __lt__(self, other):
        return self.binary_lift(operator.lt, other)    
    def __le__(self, other):
        return self.binary_lift(operator.le, other)    
    def __gt__(self, other):
        return self.binary_lift(operator.gt, other)    
    def __ge__(self, other):
        return self.binary_lift(operator.ge, other)    
    def __eq__(self, other):
        return self.binary_lift(operator.eq, other)    
    def __ne__(self, other):
        return self.binary_lift(operator.ne, other)

    def __getitem__(self, other):
        return self.binary_lift(operator.getitem, other)

    @staticmethod
    def generic(type_template, propagate_metadata = True):
        # Generic constructor decorator -- wrap function in the same type of OperatorFunc as type_template, assuming it is one
        if isinstance(type_template, OperatorFunc):
            def opgen(func):
                op = type_template.__class__(func)
                if propagate_metadata:
                    op.attach_metadata(None, type_template)
                return op
            return opgen
        else:
            return lambda func: func


class PointOp(OperatorFunc):
    @staticmethod
    def with_metadata(*childops):
        return lambda func: OperatorFunc.carry_metadata(*childops)(PointOp(func))

# The null domain mask: no restrictions
simoutputview.no_mask = PointOp(lambda output, pos: True) 

class FitnessOp(PointOp):
    # (All FitnessOps are technically PointOps as well, though they ignore position of evaluation)

    def __call__(self, output : simoutputview, pos = None, *args, **kwargs):
        return self.function(output, *args, **kwargs)

    @staticmethod
    def with_metadata(*childops):
        return lambda func: OperatorFunc.carry_metadata(*childops)(FitnessOp(func))

    @staticmethod
    def caching(*childops):
        return lambda func: OperatorFunc.carry_metadata(*childops)(FitnessOp(functools.lru_cache()(func)))
        #return FitnessOp(functools.lru_cache()(func))


def eval_fitfunc_str(fitfuncstr : str, list_ok = False):
    # TODO some sensible solution for importing outside modules

    import builtins
    def eval_with_lifted_ops(exprstr : str):
        # Set up lifted function aliases, confined to this narrow scope
        abs = OperatorFunc.static_unary_lifter(math.fabs)
        sqrt = OperatorFunc.static_unary_lifter(math.sqrt)
        exp = OperatorFunc.static_unary_lifter(math.exp)
        log = OperatorFunc.static_unary_lifter(math.log)
        log10 = OperatorFunc.static_unary_lifter(math.log10)
        # TODO make a wrapper that returns a silent NaN rather than erroring out on nonpositive inputs?
        sin = OperatorFunc.static_unary_lifter(math.sin)
        cos = OperatorFunc.static_unary_lifter(math.cos)
        tan = OperatorFunc.static_unary_lifter(math.tan)
        atan = OperatorFunc.static_unary_lifter(math.atan)
        tanh = OperatorFunc.static_unary_lifter(math.tanh)
        count = OperatorFunc.static_unary_lifter(len)
        includes = OperatorFunc.static_binary_lifter(lambda settrait, item: item in settrait)
        max = OperatorFunc.static_binary_lifter(builtins.max)
        min = OperatorFunc.static_binary_lifter(builtins.min)
        smax = OperatorFunc.static_binary_lifter(log_sum_exp)
        atan2 = OperatorFunc.static_binary_lifter(math.atan2)

        #return eval(exprstr)

        # hack to fix python's lambda-in-eval-accessing-locals dysfunctional misbehavior:
        globalenv = globals().copy()
        globalenv.update(locals())
        try:
            return eval(exprstr, globalenv) 
        except Exception as e:
            eprint("Error: Exception while evaluating user fitness function!\n")
            raise e
        
        # (the new syntax would be cleaner -- {**globals(), **locals()} -- but my editor highlights it wrong so i don't use it ;) 

    func = eval_with_lifted_ops(fitfuncstr)

    if not (hasattr(func, '__call__') or (list_ok and isinstance(func, list) and
                                          all(hasattr(f, '__call__') for f in func))):
        # (Generally one should expect an OperatorFunc, but perhaps there might be exceptions, so we use a 
        #  more lenient sanity check.)
        eprint("Error: User fitness function does not evaluate to a valid function! (not callable)\n")
        raise ValueError(func)

    # Attach string names (kinda kludgy)
    if isinstance(func, list):
        # kludge for eval'ing lists of functions (while extracting sensible names)
        # this is __awful__
        # (why can't python just admit its parentage and openly be a LISP? ;_; )

        import ast
        try:
            ffs_trimmed = fitfuncstr.strip()
            listelements = ast.parse(ffs_trimmed).body[0].value.elts
            for i in range(len(listelements)):
                startoffset = listelements[i].col_offset
                if i + 1 < len(listelements):
                    endoffset = listelements[i + 1].col_offset
                else:
                    endoffset = len(ffs_trimmed)

                while ffs_trimmed[endoffset - 1] == "," or ffs_trimmed[endoffset - 1] == "]" or ffs_trimmed[endoffset - 1].isspace():
                    endoffset -= 1

                func[i].__name__ = ffs_trimmed[startoffset : endoffset]
        except Exception:
            # Don't even try to recover or complain just give up :P
            pass

    else: 
        func.__name__ = fitfuncstr

    return func


def compute_fitness(fitfunc : FitnessOp, indiv_name : str, indiv_config : dict, outputdata : dict) -> float:
    if all(data.is_empty for data in outputdata.values()):
        # failed to read any data? automatic zero
        # todo: allow expected number of frames as parameter?
        eprint("Warning: Individual " + indiv_name + " has no output data.\n")
        return 0

    try:
        fitness = fitfunc(simoutputview(outputdata, indiv_config))
    except MissingDataException:
        # Data may have been truncated; success_code probably != 0 anyway, but we need to catch this so we don't bail out
        fitness = 0
        if all(data.success_code == 0 for data in outputdata.values()):
            # In the odd case that the sim was successful, this may represent a bug in the fitness function,
            # so give the user a specific warning.
            eprint("Warning: Fitness function requested data not present for individual " + indiv_name + "!\n")
    except Exception as e:
        eprint("Error: Exception while evaluating user fitness function for individual " + indiv_name + "!\n")
        raise e

    try:
        # Sanity check for orderability
        assert fitness >= 0 or fitness < 0 
    except Exception as e:
        eprint("Error: User fitness function returned inappropriate type of value for individual " + indiv_name + ": " + str(fitness) + "\n")
        raise e

    if any(data.success_code != 0 for data in outputdata.values()) and not fitfunc.query_metadata("score_failed_runs", False):
        # Any of the simulations exited with failure? Automatic zero unless the fitness function specifically says it's ok.
        # (This whole business by default is because it's too easy to forget to check in your fitness function!)
        # Could be moved earlier (now that it's score_failed_runs is known statically)...
        eprint("Warning: Simulation of individual " + indiv_name + " failed.\n")
        fitness = 0

    return fitness

def compute_fitness_pop(fitfunc : FitnessOp, outputs : dict, pop : dict) -> dict:
    return {ind : compute_fitness(fitfunc, ind, pop[ind], outputs[ind]) for ind in pop}


# convenience functions for debugging:
def load_and_compute_fitness_pop(fitfunc : FitnessOp, pop : dict, config) -> dict:
    return compute_fitness_pop(fitfunc, {indiv : config.load_output(indiv) for indiv in pop}, pop)

def diagnostic_histogram(func : FitnessOp, pop : dict, config):
    import matplotlib.pyplot as plt

    popvalues = load_and_compute_fitness_pop(func, pop, config)

    fig = plt.figure()
    plt.hist([popvalues[k] for k in popvalues])
    fig.canvas.draw()
    fig.show()


def log_sum_exp(*args):
    # Compute log(e^x1 + e^x2 + ...) in a safe manner that avoids overflow
    # Useful as a softened version of max()
    maxargs = max(args)
    return math.log(sum(math.exp(x - maxargs) for x in args)) + maxargs

def bell(x, sigma):
    return math.exp(-0.5 * (x / sigma) ** 2)


#<--- begin obsolete...

def pos_dist(pos1, pos2): # (note: duplicate in simctl... and this one's not used except for obsolete stuff?)
    return math.hypot(pos2[0] - pos1[0], pos2[1] - pos1[1])

cached_allpos = None
cached_neighborhoods = {}
# TODO replace this manual caching with functools.lru_cache?
# also beware plans for multiprocessing!

def get_neighbors(pos, range, all_positions):
    global cached_allpos, cached_neighborhoods

    if not cached_allpos is all_positions:
       # (fixme by-value comparison could get ugly if you're combining different frames in one reduction?)
       if cached_allpos != all_positions:
           # refresh cache, brute force n by n measurements
           eprint("INFO: refreshing neighbor cache...\n") # Remove this? It doesn't seem very useful
           cached_neighborhoods = {}
           for p1 in all_positions:
               dists = []
               for p2 in all_positions:
                   dists.append((pos_dist(p1, p2), p2))
               cached_neighborhoods[p1] = sorted(dists)
        
       cached_allpos = all_positions
    
    results = []
    for dist, neighborpos in cached_neighborhoods[pos]:
        if dist > range:
            break
        results.append(neighborpos)
    
    return results
    # note: includes self among neighbors

#... end obsolete --->

def bound_box(all_positions):
    return ((min(x for x,y in all_positions), min(y for x,y in all_positions)),
            (max(x for x,y in all_positions), max(y for x,y in all_positions)))

def bound_box_dims(all_positions):
    bbox = bound_box(all_positions)
    return (bbox[1][0] - bbox[0][0], bbox[1][1] - bbox[0][1])


# Building block functions for fitness functions:

@PointOp
def defval(output : simoutputview, pos):
    return output.getvalue(pos)

@PointOp
def deltaval(output : simoutputview, pos):
    return output.getvalue(pos) - output.getvalue(pos, -1)

@PointOp
def posvec(output : simoutputview, pos):
    return pos

@PointOp
def neighborcount(output : simoutputview, pos):
    return len(output.neighbor_positions(pos))

def movingavg(range : float, func : PointOp = defval):
    @PointOp.with_metadata(func)
    def avgval(output : simoutputview, pos):
        #neighborhood = get_neighbors(pos, range, output.positions(mask = simoutputview.no_mask))
        #    #FIXME plays poorly with restrictions, lift to simoutputview class?
        neighborhood = output.neighborhood_positions(pos, range)
        return sum(func(output, neighborpos) for neighborpos in neighborhood) / len(neighborhood)

    return avgval

def movingavg_ec(range : float, func : PointOp = defval):
    return convolution(lambda o, p: 1, range, func)

def convolution(kernel, range : float, func : PointOp = defval, edge_effect_correction : bool = True):
    def dist(pos1, pos2):
        return math.hypot(pos1[0] - pos2[0], pos1[1] - pos2[1])

    @PointOp.with_metadata(func)
    def convolve(output : simoutputview, pos):
        #neighborhood = get_neighbors(pos, range, output.positions(mask = simoutputview.no_mask))
        #    #FIXME plays poorly with restrictions, lift to simoutputview class?
        neighborhood = output.neighborhood_positions(pos, range)

        if edge_effect_correction:
            # Experimental attempt at reducing edge effects by re-weighting.
            # (most sensible for rotationally symmetric kernels)
            # (also, it's slow, at least as implemented!)

            min_dist = min(dist(pos, p) for p in neighborhood if p != pos)
            weight = {}
            for p in neighborhood:
                shellthreshold = min_dist / 5
                count_at_range = len([q for q in neighborhood if abs(dist(pos, p) - dist(pos, q)) < shellthreshold])
                weight[p] = max(dist(pos, p), min_dist / 6) / count_at_range
                # Rationale: weight so that a regular hex lattice has uniform weights, while filling missing
                # parts of neighborhood shells with what's available.
                # (YMMV on highly irregular lattices)

            return sum(func(output, neighborpos) * weight[neighborpos] *
                       kernel((pos[0] - neighborpos[0], pos[1] - neighborpos[1]))
                       for neighborpos in neighborhood) / sum(weight[p] for p in neighborhood)
        else:
            return sum(func(output, neighborpos) *
                       kernel((pos[0] - neighborpos[0], pos[1] - neighborpos[1]))
                       for neighborpos in neighborhood) / len(neighborhood)

    return convolve


def qdhat(scale : float = 1.0):
    def qdhat_kernel(pos): 
        # quick and dirty 2d hat function pulled out of nowhere
        # (positive approx r < 1, negative approx 1 < r < 2, zero by r = 3)
        r = math.hypot(pos[0], pos[1]) / scale
        if r < 3.14:
            u = 2 * math.tan(r / 2)
            u_over_r = u / r if r > 1.0e-6 else 1
            return -(u ** 2 / 2 - 1) * math.exp(-u ** 2 / 2) * (u ** 2 / 4 + 1) * u_over_r
        else:
            return 0

    return qdhat_kernel
# note: the practical properties of this fn are considerably worse than you might expect, due to
# discretization and edge effects. in a continuum, you should get zero from, say, 
#   output_anal.diagnostic_plot(tso, output_anal.convolution(output_anal.qdhat(20), 100, lambda o, p: 1))
# but you don't.

def medianfilter(range : float, func : PointOp = defval):
    import statistics
    @PointOp.with_metadata(func)
    def median_inner(output : simoutputview, pos):
        neighborhood = output.neighborhood_positions(pos, range)

        return statistics.median(func(output, neighborpos) for neighborpos in neighborhood)

    return median_inner

def absvariation(func : PointOp = defval):
    # Sum this to get "Total Variation"
    @PointOp.with_metadata(func)
    def absvar_inner(output : simoutputview, pos):
        neighbors = output.neighbor_positions(pos)
        localval = func(output, pos)
        return sum(abs(func(output, neighborpos) - localval) for neighborpos in neighbors)

    return absvar_inner


def imagemask(pathname : str, worldsize : float):
    # (would be nice to get basepath and worldsize from config info, but the logical distance is large...)
    import scipy.misc
    image = scipy.misc.imread(pathname, flatten=1).transpose()

    @PointOp
    def in_mask(output : simoutputview, pos):
        maskx = round(pos[0] / worldsize * image.shape[0])
        masky = round(pos[1] / worldsize * image.shape[1])

        if maskx < 0 or maskx >= image.shape[0] or masky < 0 or masky >= image.shape[1]:
            return False
        return bool(image[maskx, masky] == 0)
        # (Astonishingly, numpy returns its own bool that has different behavior under arithmetic operations.
        #  Get rid of it!)

    return in_mask    


def diagnostic_plot(output : simoutputview, func : PointOp):
    # Quick and dirty diagnostic plot (similar to monitorui.pointfunc_monitor)
    import matplotlib.pyplot as plt

    positions = [pos for pos in output.positions()]
    fig = plt.figure()
    plt.scatter([p[0] for p in positions], [p[1] for p in positions], 100,
                cmap = 'RdBu_r', c = [func(output, p) for p in positions])
    plt.colorbar()
    fig.canvas.draw()
    fig.show()


def vadd(a, b):
    # DWIM polymorphic vector addition
    # (don't pass in silly things like OperatorFuncs. it won't end well! same with the others.)
    if a == 0:
        return b
    elif b == 0:
        return a
    elif isinstance(a, numbers.Number):
        return a + b
    else:
        return tuple(vadd(x,y) for x,y in zip(a, b))

def vdot(a, b):
    # DWIM vector dot product
    if isinstance(a, numbers.Number):
        return a * b
    else:
        return sum(vdot(x,y) for x,y in zip(a, b))

def vsmul(v, s):
    # DWIM vector-scalar multiplication
    if isinstance(v, numbers.Number):
        return v * s
    else:
        return tuple(vsmul(x, s) for x in v)

def vsqr(v):
    # DWIM vector squared magnitude
    if isinstance(v, numbers.Number):
        return v ** 2
    else:
        return sum(vsqr(x) for x in v)

def moment(order : int, func : PointOp = defval, center : tuple = (0, 0), normalize_dims : tuple = (1, 1)):
    @PointOp.with_metadata(func)
    def moment_induc(output : simoutputview, pos):
        funcval = func(output, pos)
        return (vsmul(funcval, (pos[0] - center[0]) / normalize_dims[0]),
                vsmul(funcval, (pos[1] - center[1]) / normalize_dims[1]))

    if order == 0:
        return func
    elif order < 0:
        raise ValueError("Moment must be positive integer order")
    else:
        return moment(order - 1, moment_induc, center, normalize_dims)


def within(maskfunc, fitfunc):
    @FitnessOp.caching(maskfunc, fitfunc)
    def within_inner(output : simoutputview):
        return fitfunc(output.within(maskfunc))
    return within_inner

def atframe(frame, fitfunc):
    @FitnessOp.caching(fitfunc)
    def atframe_inner(output : simoutputview):
        return fitfunc(output.atframe(frame))
    return atframe_inner

def assay(assaysubdir, fitfunc):
    @FitnessOp.caching(fitfunc)
    def assay_inner(output : simoutputview):
        return fitfunc(output.atassay(assaysubdir))
    return assay_inner

def time_reducer(reduction_op, fitfunc, range = None):
    @FitnessOp.caching(fitfunc)
    def time_reducer_inner(output : simoutputview):
        nonlocal range # <-- screw you too, python
        if range == None:
            range = output.frames()
        return reduce(reduction_op, (fitfunc(output.atframe(frame)) for frame in range))
    return time_reducer_inner

# todo it would be more consistent if there were some way to specify range relative to the current frame rather than absolute?

def time_sum(fitfunc, range = None):
    return time_reducer(vadd, fitfunc, range)

def time_max(fitfunc, range = None):
    return time_reducer(max, fitfunc, range)

def time_min(fitfunc, range = None):
    return time_reducer(min, fitfunc, range)

def time_avg(fitfunc, range = None):
    @FitnessOp.caching(fitfunc)
    def reduce_timeavg_inner(output : simoutputview):
        nonlocal range # <-- screw you too, python
        if range == None:
            range = output.frames()
        return time_sum(fitfunc, range)(output) / len(range)
    return reduce_timeavg_inner

# (should these be made generic so they can be used in pointfuncs? atframe might be straightforward,
#  but assay could run into issues with the points not being the same... and e.g. pointfunc_monitor
#  would plot the wrong pointset anyway.)


def generic_reducer(reduction_op, func : PointOp = defval, init_value = 0):
    @FitnessOp.caching(func)
    def reducer_inner(output : simoutputview):
        values = (func(output, pos) for pos in output.positions())
        if init_value == None:
            return reduce(reduction_op, values)
        else:
            return reduce(reduction_op, values, init_value)
    return reducer_inner

def reduce_sum(func : PointOp = defval):
    return generic_reducer(vadd, func)
    #@FitnessOp.caching(func)
    #def reduce_sum_inner(output : simoutputview):
    #    return reduce(vadd, (func(output, pos) for pos in output.positions()), 0)
    #    #return sum(func(output, pos) for pos in output.positions())
    #return reduce_sum_inner

def reduce_max(func : PointOp = defval):
    return generic_reducer(max, func, None)

def reduce_min(func : PointOp = defval):
    return generic_reducer(min, func, None)

def reduce_avg(func : PointOp = defval):
    @FitnessOp.caching(func)
    def reduce_avg_inner(output : simoutputview):
        return vsmul(reduce_sum(func)(output), 1 / len(output.positions()))
    return reduce_avg_inner

def reduce_rms(func : PointOp = defval):
    @FitnessOp.caching(func)
    def reduce_rms_inner(output : simoutputview):
        return math.sqrt(reduce_avg(lambda o, p: func(o, p) ** 2)(output))
    return reduce_rms_inner

def reduce_stdev(func : PointOp = defval):
    @FitnessOp.caching(func)
    def reduce_stdev_inner(output : simoutputview):
        avg = reduce_avg(func)(output)
        return reduce_rms(lambda o, p: func(o, p) - avg)(output)
        # (could be more efficient by not double-evaluating...)
    return reduce_stdev_inner

def reduce_stat_moment(order : int, func : PointOp = defval):
    @FitnessOp.caching(func)
    def reduce_stat_moment_inner(output : simoutputview):
        avg = reduce_avg(func)(output)
        stdev = reduce_stdev(func)(output)
        if stdev == 0:
            return 0
        return reduce_avg(lambda o, p: ((func(o, p) - avg) / stdev) ** order)(output)
        # (could be more efficient by not triple-evaluating...)
    return reduce_stat_moment_inner


def dipole(func : PointOp = defval, axis : tuple = None):
    @FitnessOp.caching(func)
    def dipole_inner(output : simoutputview):
        centroid = reduce_avg(posvec)(output)
        dmoment = reduce_avg(moment(1, func, centroid))(output)
        if axis:
            return vdot(dmoment, axis)
        else:
            return math.hypot(dmoment[0], dmoment[1])
    return dipole_inner

def moment_zms(order : int, func : PointOp = defval):
    # zero mean, scaled moments
    # (non-orthogonal moments. not attempting to use orthogonal polynomials like Zernike functions
    #  because their orthogonality is tied to substrate shape.)

    @FitnessOp.caching(func)
    def moment_zms_inner(output : simoutputview):
        centroid = reduce_avg(posvec)(output)
        dims = bound_box_dims(output.positions())
        vmoment = reduce_avg(moment(order, func, centroid, dims))(output)

        vmoment_dc = reduce_avg(moment(order, lambda o, p: 1, centroid, dims))(output)
        favg = reduce_avg(func)(output) # double evaluation sigh...
        #print(vmoment)
        #print(vsmul(vmoment_dc, -favg))
        return vadd(vmoment, vsmul(vmoment_dc, -favg))

    return moment_zms_inner
# fixme i am concerned there is a subtle bug here because i'm seeing nonzero YY moments
# for even functions of x alone (and vice versa) in tests.

def moment_magnitude(order : int, func : PointOp = defval):
    mfunc = moment_zms(order, func)

    @FitnessOp.caching(func)
    def moment_magnitude_inner(output : simoutputview):
        return math.sqrt(vsqr(mfunc(output)))

    return moment_magnitude_inner


def global_directionality(width = 30, steps = 6, func : PointOp = defval):
    @FitnessOp.caching(func)
    def global_directionality_inner(output : simoutputview):
        centroid = reduce_avg(posvec)(output)

        outputvalues = {pos : func(output, pos) for pos in output.positions()}

        step_amplitudes = {}

        for i in range(steps):
            angle = i * math.pi / steps
            axis = (math.cos(angle), math.sin(angle))
            coaxis = (-math.sin(angle), math.cos(angle))

            lbound = min(vdot(pos, coaxis) for pos in output.positions())
            ubound = max(vdot(pos, coaxis) for pos in output.positions())

            nbuckets = math.ceil((ubound - lbound) / width)
            buckets = [[] for b in range(nbuckets)]
            for pos in output.positions():
                projected_pos = vdot(pos, coaxis)
                buckets[math.floor((projected_pos - lbound) / width)].append(outputvalues[pos])

            bucket_avgs = {sum(bucket) / len(bucket) for bucket in buckets if len(bucket) > 0}

            #print("step " + str(i) + ": " + str(bucket_avgs))

            if len(bucket_avgs) > 1:    
                # (RMS is a useless measure of amplitude of only one bucket is present)
                avg_overall = sum(bucket_avgs) / len(bucket_avgs)
                step_amplitudes[i] = math.sqrt(sum((bavg - avg_overall) ** 2 for bavg in bucket_avgs) / len(bucket_avgs))

        #print(step_amplitudes)

        avg_step_amplitude = sum(step_amplitudes.values()) / len(step_amplitudes)
        return math.sqrt(sum((step_amp - avg_step_amplitude) ** 2 for step_amp in step_amplitudes.values()) / len(step_amplitudes))

    return global_directionality_inner



@FitnessOp
def runtime(output : simoutputview):
    return output.data.running_time

@FitnessOp
def successcode(output : simoutputview):
    return output.data.success_code


def letvalue(valuefunc, lambdaexpr):
    # Evaluate valuefunc in local combinator scope and pass it as an argument to be bound to a variable

    @OperatorFunc.generic(valuefunc)
    def binding_getter(output : simoutputview, *args):
        return output.lookup_binding(id(binding_getter))
        # (We use id() because OperatorFuncs aren't hashable due to their weird lifted override of __eq__)

    # Bind the variable to a placeholder and continue evaluating the combinators in the subexpression special form
    inner_expr = lambdaexpr(binding_getter) 

    @OperatorFunc.generic(inner_expr)
    def letvalue_inner(output : simoutputview, *scope_args):

        bound_value = valuefunc(output, *scope_args)

        # thanks to threading we need to store binding on output, rather than simply using a closure
        # (i.e. as a proxy for storing it in an activation record, and recognizing that you can't call in recursively)

        return inner_expr(output.with_binding(id(binding_getter), bound_value), *scope_args) 

    return letvalue_inner

def letfn(*args):
    # Convenience routine parallel to letvalue -- defines macro functions evaluated in the scope in which they appear
    if len(args) < 1:
        raise TypeError("letfn requires at least one arg, lambdaexpr")

    lambdaexpr = args[-1]
    return lambdaexpr(*args[0:-1])


def require_success(func):
    @OperatorFunc.with_metadata_field("score_failed_runs", False, operator.and_)
    @FitnessOp
    def require_success_inner(output : simoutputview):
        #output.data.score_failed_runs = False
        if output.data.success_code != 0:
            return 0
        fitval = func(output)
        # Truncate fitness at zero so we don't accidentally select for simulator failure
        return max(fitval, 0)

    return require_success_inner
# ... what is this even for if it's the default...? is it obsolete and i should've removed it?

def allow_failures(func):
    @OperatorFunc.with_metadata_field("score_failed_runs", True, operator.and_)
    @FitnessOp
    def allow_failures_inner(output : simoutputview):
        #output.data.score_failed_runs = True
        return func(output)

    return allow_failures_inner


def trait(trait_name : str):
    @FitnessOp
    def trait_inner(output : simoutputview):
        return output.gettrait(trait_name)

    return trait_inner



# Old anal functions, kept only for compatibility

def final_frame(output : simoutputview) -> dict:
    frames = output.data.output_fields[output.cur_field]
    return frames[-1];

def final_delta(output : simoutputview) -> dict:
    frames = output.data.output_fields[output.cur_field]
    assert len(frames) >= 2
    return {k : frames[-1][k] - frames[-2][k] for k in frames[-1]}

def frame_count(output : simoutputview) -> int:
    frames = output.data.output_fields[output.cur_field]
    return len(frames)


def field_centroid(field : dict) -> tuple:
    n = 0
    xsum = 0
    ysum = 0
    for xy in field:
        n+=1
        xsum += xy[0]
        ysum += xy[1]

    return (xsum / n, ysum / n)

def field_moment(field : dict, order : int) -> float:
    #dunno if actually useful in such scalar form :P

    centroid = field_centroid(field)
    n = 0
    accum = 0

    for xy in field:
        n+=1
        accum += field[xy] * math.pow(math.hypot(xy[0] - centroid[0], xy[1] - centroid[1]), order)

    return accum / n

def field_mean(field : dict) -> float:
    return field_moment(field, 0)

def field_rms(field : dict) -> float:
    n = 0
    accum2 = 0

    for xy in field:
        n+=1
        accum2 += field[xy] * field[xy]

    return math.sqrt(accum2 / n)

def field_stdev(field : dict) -> float:
    n = 0
    accum = 0
    accum2 = 0

    for xy in field:
        n+=1
        accum += field[xy]
        accum2 += field[xy] * field[xy]

    return math.sqrt((accum2 - accum * accum / n) / n)

def field_dipole(field : dict) -> float:
    #dunno if actually useful in such scalar form :P

    centroid = field_centroid(field)
    n = 0
    accumx = 0
    accumy = 0

    for xy in field:
        n+=1
        accumx += field[xy] * (xy[0] - centroid[0])
        accumy += field[xy] * (xy[1] - centroid[1])

    return math.hypot(accumx / n, accumy / n)
