## GABEE mutation / variation routines
##
## (C) Micah Z. Brodsky 2016-
##


# Populations are represented as dictionaries, mapping individual names to their individual configurations.
# Individual configurations are represented as "bindings", in the same format as used by fragmatcher 
# to represent a patch file mapped onto a configuration: a dictionary mapping trait names to ordered pairs
# of trait configurations (dictionary containing the $match block fields) and original matched values.
# Original matched values have no real function here but are preserved vestigally to keep the format the
# same.

# TODO this simplistic design has far outlived its usefulness and should be replaced!
# And has way too many undeclared cross-dependencies with fragmatcher...


import copy
import random
import math

import gabee.selection as selection
import gabee.fragmatcher as fragmatcher
from gabee.util import *


def uniformrandom():
    return (random.random() - 0.5) * 2

def normalrandom():
    return random.gauss(0, 1)

def powerlawrandom():
    return (random.randrange(0, 2) * 2 - 1) * (random.paretovariate(1) - 1) / 10
    # Scaled so that mean abs deviation is comparable to the others (still a bit larger)

stepsamplers = {fn.__name__ : fn for fn in [uniformrandom, normalrandom, powerlawrandom]}


class evoptions:
    # Class encapsulating evolutionary tuning parameters (for both variation and selection)

    def __init__(self, expected_mutation_count : float = 1.0, tourney_size : int = 3,
                 step_size_multiplier : float = 1.0, stepsampler = uniformrandom, crossover_fraction : float = 0):
        self.expected_mutation_count = expected_mutation_count
        self.reprod_selector = selection.tournament_selector(tourney_size) if tourney_size > 0 else selection.fitprop_select
        self.step_size_multiplier = step_size_multiplier
        self.stepsampler = stepsampler
        self.crossover_fraction = crossover_fraction
        # todo survial selector (and child pop size)


def make_variant(base_indiv : dict, trait : str, value) -> dict:
    new_indiv = copy.deepcopy(base_indiv)
    new_indiv[trait][0]["Value"] = value

    recompute_derived_values(new_indiv)
    # fixme someday: grossly inefficient to do this here when you're making many changes at once...
    # (though this should only happen with bizarrely high mutation rates or during population initialization...)

    return new_indiv

def recompute_derived_values(indiv : dict):
    # Recompute values for derived traits, updating indiv in-place

    derived_traits = {k for k in indiv if "Derived" in indiv[k][0]}

    pending_traits = set()
    computed_values = {}

    def get_value(trait):
        # Retrieve or compute a derived value, recursively if necessary, dynamic programming-style

        if trait not in derived_traits:  # trivial case
            return indiv[trait][0]["Value"]
            # (todo maybe explicit check for trait in indiv and return a more informative error than vanilla KeyError?)

        if trait in computed_values:  # cached already
            return computed_values[trait]

        if trait in pending_traits:
            raise Exception("Cyclic dependency encountered computing derived trait: " + trait)

        pending_traits.add(trait)

        derivation_expr = indiv[trait][0]["Derived"]
        try: 
            newvalue = eval(derivation_expr, globals(), {"trait" : get_value})
        except Exception:
            eprint("Error: Exception while evaluating derived trait " + trait + "!\n")
            raise
        computed_values[trait] = newvalue
        return newvalue

    for k in derived_traits:
        indiv[k][0]["Value"] = get_value(k)


def generate_uniform_spanning_pop(base_indiv : dict, trait : str, steps : int) -> dict:
    # todo some validation of extant fields?
    patexpr = base_indiv[trait][0]
    range_min = patexpr.get("Min") or 0
    range_max = patexpr["Max"]

    return {gensym("INDIV_" + str(i) + "_") :
            make_variant(base_indiv, trait, range_min + i * (range_max - range_min) / steps)
            for i in range(0, steps)}

def perturb_trait(base_indiv : dict, trait : str, step_size_multiplier: float = 1.0,
                  randsampler = uniformrandom) -> dict:
    # todo some validation of extant fields?
    # (todo discrete traits handled differently)
    patexpr = base_indiv[trait][0]
    oldvalue = patexpr["Value"]
    if oldvalue is True or oldvalue is False:
        # Simple special case handling for Boolean traits
        newvalue = randsampler() > 0
    elif "Containing" in patexpr:
        if patexpr["Containing"].get("Type") == "set":
            # Perturb the membership of the set

            alternatives = patexpr["Containing"]["Alternatives"]
            swap_rate = 1.0 / len(alternatives) * step_size_multiplier

            itemset = set(oldvalue)
            for alt in alternatives:
                if alt in itemset:
                    if random.random() < swap_rate:
                        itemset.remove(alt)
                else:
                    if random.random() < swap_rate:
                        itemset.add(alt)
                # TODO someday this will need to be a matching comparison or something fancy, to support match subexpressions...
                # or something that identifies subexpr correspondence. definitely not just a set membership test.

            newvalue = list(itemset)
        elif patexpr["Containing"].get("Type") == "singleton":
            alternatives = patexpr["Containing"]["Alternatives"]
            newvalue = random.choice(alternatives)
            # TODO similar caveat as above
        else:
            raise ValueError("Unsupported 'Containing:' type")
    else:
        # Ordinary case for numerical traits
        oldvalue = float(oldvalue) # Handle rare case where we get fed a string representation from the YAML parser (not sure this actually fixes all cases...)
        range_min = patexpr.get("Min") or 0
        range_max = patexpr["Max"]
        logscale = patexpr.get("Logscale") or False
        if logscale:
            if range_min <= 0:
                raise ValueError("Mininum trait value must be positive under log scale: " + trait)

            stepsize = patexpr.get("Stepsize") or math.pow(range_max / range_min, 0.2)
            # (Experience so far suggests log scale requires larger step size)
            try:
                tentative_value = oldvalue * pow(10, math.log10(stepsize * step_size_multiplier) * 
                                                 randsampler())
            except OverflowError:
                tentative_value = range_max if randsampler() > 0 else range_min
        else:
            stepsize = patexpr.get("Stepsize") or (range_max - range_min) / 10
            tentative_value = oldvalue + stepsize * step_size_multiplier * randsampler()
        newvalue = max(range_min, min(range_max, tentative_value))
    return make_variant(base_indiv, trait, newvalue)

# todo allow perturbability to be contingent on other boolean fields (i.e. poor man's variable size genome)

def trait_mutable(base_indiv : dict, trait : str):
    patexpr = base_indiv[trait][0]
    if patexpr.get("Mutable") != False and "Derived" not in patexpr and not patexpr.get("Meta"):
        return True
    else:
        return False

def trait_always_mutates(base_indiv : dict, trait : str):
    return base_indiv[trait][0].get("AlwaysMutates") == True

# todo possibly add a "heritable" option that allows you to force reverting to default every generation?
# (or maybe that's better done with derived traits, in one way or another?)

def count_normalizable_traits(indiv : dict, options : evoptions) -> float:
    count = sum(1 for trait in indiv if not indiv[trait][0].get("Meta") and not indiv[trait][0].get("Derived"))
    # Note that we count "immutable" traits for normalization purposes. This is to keep per-locus mutation rates
    # constant in knock-out experiments. Perhaps this choice will be exposed as a command-line option eventually...

    if count == 0:
        raise ValueError("No traits available to be mutated")

    return count

    #return float(len(indiv))  # why the heck did this have an explicit cast to float? what was the point?

def perturb_indiv(base_indiv : dict, options : evoptions, templatebindings : dict = None) -> dict:
    new_indiv = copy.deepcopy(base_indiv) # <-- unnecessary since make_variant also copies??

    if templatebindings:
        # Roll over to new patch file parameters, if supplied
        new_indiv = fragmatcher.update_binding_values(templatebindings, new_indiv)
        recompute_derived_values(new_indiv)

    normalization_count = count_normalizable_traits(new_indiv, options)

    for k in base_indiv:
        if trait_mutable(new_indiv, k):
            if (trait_always_mutates(new_indiv, k) or
                random.random() < options.expected_mutation_count / normalization_count):
                new_indiv = perturb_trait(new_indiv, k, options.step_size_multiplier, options.stepsampler)

    return new_indiv

def perturb_indiv_repeatedly(base_indiv : dict, options : evoptions, repcount : int) -> dict:
    # FIXME Python doesn't do tail recursion so this can't go more than a few hundred iters :P
    return base_indiv if repcount <= 0 else perturb_indiv_repeatedly(perturb_indiv(base_indiv, options), options, repcount - 1)
# Note that there is some tendency to "walk off the edge" of the value range with large repcounts,
# producing a disproportionate number of values at min or max, since it's basically truncated random walk.
# (Well, sort of.)

def generate_perturbed_pop(base_indiv : dict, expected_perturb_count : float, count : int, repcount : int = 1) -> dict:
    # Generate a population by repeatedly mutating a base individual
    options = evoptions(expected_mutation_count = expected_perturb_count)
    return {gensym("INDIV_") :
            perturb_indiv_repeatedly(base_indiv, options, repcount)
            for i in range(0, count)}


def cross_individuals(parent1 : dict, parent2: dict, options : evoptions) -> dict:
    # Parameterized uniform cross-over

    new_indiv = copy.deepcopy(parent1)
    for trait in parent1:
        if random.random() < options.crossover_fraction:
            new_indiv[trait][0]["Value"] = parent2[trait][0]["Value"]

    recompute_derived_values(new_indiv)
    
    return new_indiv

def generate_successor_unperturbed(pop : dict, pop_fitness : dict, options : evoptions) -> dict:
    if options.crossover_fraction > 0:
        parent1 = options.reprod_selector(pop_fitness)
        parent2 = options.reprod_selector(pop_fitness)
        ancestry = [parent1, parent2]
        successor = cross_individuals(pop[parent1], pop[parent2], options)
    else:
        parent = options.reprod_selector(pop_fitness)
        ancestry = [parent]
        successor = copy.deepcopy(pop[parent])
        # (so many re-copies, sigh!)

    successor["$ancestry"] = ({"Value" : ancestry, "Meta" : True}, None)
    # (a kinda hacky scheme...)

    return successor

def generate_successor_pop(pop : dict, pop_fitness : dict, options : evoptions, templatebindings : dict = None) -> dict:
    return {gensym("INDIV_") :
            perturb_indiv(generate_successor_unperturbed(pop, pop_fitness, options), options, templatebindings)
            for i in range(0, len(pop))}

    #todo record lineage somewhere!


def population_trait_stats(pop: dict, normalize : bool = False) -> dict:
    # Compute simple summary statistics on the distribution of trait values in a population 
    # (currently restricted to numerical traits)

    trait_values = {}

    for indiv_name in pop:
        indiv = pop[indiv_name]
        for trait in indiv:
            if "Max" not in indiv[trait][0]:
                # Quick and dirty way to skip non-numerical traits
                continue
            if trait not in trait_values:
                trait_values[trait] = []
            value = indiv[trait][0]["Value"]
            if normalize:
                range_min = indiv[trait][0].get("Min") or 0
                range_max = indiv[trait][0]["Max"]
                trait_values[trait].append(value / (range_max - range_min))
            else:
                trait_values[trait].append(value)
    
    trait_stats = {}
    for trait in trait_values:
        values = trait_values[trait]
        mean = sum(values) / len(values)
        meansq = sum(x ** 2 for x in values) / len(values)
        stdev = math.sqrt(max(meansq - mean ** 2, 0))
        trait_stats[trait] = {"Min" : min(values), "Max" : max(values), "Mean" : mean, "Stdev" : stdev}

    return trait_stats

def population_traits_variance_ranked(pop : dict):
    # Compute summary statistics of population traits and return a list of traits ranked from least to most variable

    trait_stats = population_trait_stats(pop, normalize = True)
    ranked_pairs = sorted([(trait_stats[trait]["Stdev"], trait) for trait in trait_stats])
    return [(rp[1], rp[0]) for rp in ranked_pairs]

    # todo i suppose logscale traits should be ranked by a log-scale measure of variation...
    # otherwise the large-valued ones may be excessively overranked on their variability
    # (and conversely for small-valued ones)

