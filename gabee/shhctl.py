## GABEE control library for SwHoHex Swift-Hohenberg simulation
##
## (C) Micah Z. Brodsky 2016-
##


import os
import shutil
import time
import subprocess
import sys
import math
import platform
import csv

# import only global dependencies here.
# some difficulty finding local dependencies when launched remotely.
# (being launched as a script and not as a module...)


def shh_test():
    result = subprocess.run(["python", "-m", "gabee.swhohex", "--help"], stdout = subprocess.PIPE)
    return result.returncode == 0


def shh_runner_local(basepath : str):
    # NB: Use no GA dependencies; may be run standalone
    def run_shh(indiv_name : str):
        configpath = os.path.join(basepath, indiv_name, "config.yaml")
        # FIXME support alternatives to config.yaml here!!!!!!!!!
        resultcodepath = os.path.join(basepath, indiv_name, "resultcodes.csv")
        with open(resultcodepath, "wt") as rclog:
            rclog.write("# stage, result code, node, running time (s)\n")
            nodename = platform.node()
            tp0 = time.perf_counter()

            rc1 = subprocess.call(["python", "-m", "gabee.swhohex", "--headless", configpath])
            tp1 = time.perf_counter()
            rclog.write("sim," + str(rc1) + "," + nodename + "," + str(tp1 - tp0) + "\n")

    return run_shh

def shh_runner_slurm(basepath : str):
    return tasklauncher.slurm_runner(basepath, __file__)


if __name__ == '__main__':
    if len(sys.argv) != 3:
        print("Error: Expected 2 arguments, basepath and indiv_name\n", file = sys.stderr)
        sys.exit(1)

    # FIXME duplicated in betsectl maybe i should just fix importing so this doesn't need to happen :P
    target_nproc = int(os.environ.get("GABEE_WORKER_SET_NPROC_LIMIT", "0"))
    # (A bit hacky to pass thru env, but it's the easiest thing right now without lots of plumbing work...)
    if target_nproc:
        # Best-effort attempt to raise soft number-of-threads cap, in coordination with tasklauncher.initialize_tasklauncher
        # We don't actually launch many children, but apparently we still hit the limit due to the master if we happen to be
        # running on the same node, even if it's raised its own limit! This is pretty pathological, and it's entirely possible
        # that slurm will get entangled by the limit and we'll never launch, but in practice this seems to be adequate.....!
        try:
            import resource
            soft_nproc, hard_nproc = resource.getrlimit(resource.RLIMIT_NPROC)
            resource.setrlimit(resource.RLIMIT_NPROC, (target_nproc, hard_nproc))
            #print("INFO: worker nproc limit now " + str(resource.getrlimit(resource.RLIMIT_NPROC)[0]) + "\n")
        except Exception as e:
            # any exception would be a surprise, since the master already successfully got this limit
            print("Warning: unexpected exception raising worker nproc limit:", file=sys.stderr)
            print(e, file=sys.stderr)

    basepath = sys.argv[1]
    indiv_name = sys.argv[2]
    shh_runner_local(basepath)(indiv_name)
else:
    # here because launch-time dependencies argh    
    import gabee.tasklauncher as tasklauncher 
    import gabee.simctl as simctl
    import gabee.output_anal as output_anal 
    from gabee.util import *

def shh_complete_trimmer(basepath : str):
    def trim_output(indiv_name : str):
        # (note that indiv_name can presently include an assay subdir suffix)
        indiv_path = os.path.realpath(os.path.join(basepath, indiv_name))
        if (os.path.isdir(indiv_path)
            and os.path.isfile(os.path.join(indiv_path, "config.yaml")) ## fixme here too alternative config names
            and time.time() - os.path.getmtime(indiv_path) < 60 * 60 * 12 # modtime in last 12h
            and time.time() - os.path.getctime(indiv_path) < 60 * 60 * 12): # "ctime" in last 12h
            shutil.rmtree(indiv_path)
        elif os.path.exists(indiv_path):
            # If there was something there at that path, something peculiar is going on. Make some noise.
            # (At best just an old trajectory that was resumed after waiting a long time)
            eprint("Warning: Not removing data files for individual " + indiv_path
                   + " because sanity checks failed\n")
        else:
            # If there's nothing there, it may have been trimmed already e.g. by an aborted run.
            eprint("(Data files for individual " + indiv_path + " have already been removed)")

    return trim_output


def guess_outputpath(shh_config):
    return ""

def guess_monitorimages(shh_config):
    try:
        lastframe = math.floor(shh_config["itercount"] / shh_config["sampling interval"]) - 1
        return ["shh_plot_frame_%d.png" % (lastframe)] # TODO
    except Exception:
        # Best effort, just give up if it doesn't work
        return []


def get_simlauncher(basepath : str, outputpath : str):
    return simctl.SimLauncher(sim_name = "SwHoHex",
                              local_runner = shh_runner_local(basepath),
                              slurm_runner = shh_runner_slurm(basepath),
                              test_runner = shh_test,
                              output_loader = simctl.basic_output_loader(basepath, outputpath, "U0", "shh_data_frame_"),
                              output_trimmer = shh_complete_trimmer(basepath))
