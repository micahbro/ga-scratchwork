## Parallel task launcher for GABEE
##
## (C) Micah Z. Brodsky 2016-
##

import threading
import multiprocessing
import queue
import subprocess
import time
import os
import shlex
import random

from gabee.util import *
from gabee.perflog import Perflogger


class LaunchResult:
    # Simple data structure to hold the results of a parallel job run by launch_batch
    def __init__(self, job, result, starttime, servicetime, exception):
        self.job = job
        self.result = result
        self.starttime = starttime
        self.servicetime = servicetime
        self.exception = exception

def launch_batch(jobs, handler, max_concurrency : int, mp_if_fork : bool = False, raise_exceptions : bool = False,
                 reaper = None, timeout_fraction : float = 1.0, timeout_multiplier : float = 1.1, extra_wait : float = 1.0,
                 idler = None):
    # Launch parallel batch of jobs (simulations, housekeeping, or otherwise)

    if mp_if_fork and multiprocessing.get_start_method() == 'fork':
        # Use real multprocessing
        # (fork required for MP because pickling is inadequate for sharing required configuration structures)
        conclib = multiprocessing
        conc_constructor = conclib.Process
        resultqueue = multiprocessing.Queue()
        jobqueue = multiprocessing.Queue()
    else:
        # Use crummy Python threads
        conclib = threading
        conc_constructor = conclib.Thread
        resultqueue = queue.Queue()
        jobqueue = queue.Queue()

    assert(max_concurrency > 0)
    threads = set()
    results = {}


    # Entry point for workers
    def launch_thread_helper():
        while True:
            request = jobqueue.get(block = True)
            if request == None:
                break
            job = request[0]

            tp = time.perf_counter()
            try:
                resultqueue.put(LaunchResult(job, None, tp, None, None)) # Start message

                result = handler(job)

                resultqueue.put(LaunchResult(job, result, tp, time.perf_counter() - tp, None)) # Completion message
            except Exception as e:
                resultqueue.put(LaunchResult(job, None, tp, time.perf_counter() - tp, e)) # Abnormal completion message
                raise

    # Message-pumping helper function for master 
    def process_result_messages(timeout : float):
        try:
            result = resultqueue.get(block = True, timeout = timeout)
        except queue.Empty:
            result = None

        # Process this response and drain any others waiting
        while result:
            results[result.job] = result
            if raise_exceptions and result.exception:
                raise result.exception

            if resultqueue.empty():
                break
            result = resultqueue.get(block = False)
            # (nobody else is reading, therefore this had better not underflow)


    waitstart = time.perf_counter()

    # Launch workers and distribute jobs:
    for k in jobs:
        # Launch workers (and re-launch them if they crash -- though make no attempt to reissue lost jobs)
        while len([th for th in threads if th.is_alive()]) < min(len(jobs), max_concurrency) and len(threads) < len(jobs):
            # (Second condition above is a precaution if it turns out everything is crashing, so that we don't loop forever)
            newthread = conc_constructor(target = launch_thread_helper)
            newthread.start()
            threads.add(newthread)

        while True:
            # Since there doesn't seem to be a way to block on multiple queues at the same time, we have to poll
            try:
                jobqueue.put((k,), block = True, timeout = 0.1)
            except queue.Full:
                process_result_messages(timeout = 0.1)
                continue
            break


    # Collect results and wait for completion:
    time_to_threshold = None
    while any(th.is_alive() for th in threads):
        # (We rely on exit of workers for completion as a precaution, and not message receipt in resultqueue, 
        #  because processes may crash or otherwise disappear without sending messages.)

        remaining_workers = [th for th in threads if th.is_alive()]
        outstanding_jobs = [k for k in jobs if k not in results or results[k].servicetime == None]

        # Signal workers to quit once they finish what's already in the queue
        for th in remaining_workers:
            try:
                jobqueue.put(None, block = False)
            except queue.Full:
                # We'll just try again on the next loop 
                pass 
            
        process_result_messages(timeout = 1)

        if idler:
            # Give outside handlers an occasional chance to poll whatever it is they need to do...
            idler()

        if reaper and len(outstanding_jobs) / len(jobs) < 1.0 - timeout_fraction:
            # Enough jobs have completed to consider killing stragglers...

            if time_to_threshold is None:
                time_to_threshold = time.perf_counter() - waitstart
                eprint("INFO: %f%% (%f%% threshold) of batch completed in %s sec" %
                       ((1 - len(outstanding_jobs) / len(jobs)) * 100, timeout_fraction * 100, time_to_threshold))
                # Compute longest service time necessary for any job up to this point; this will help define
                # our timeout.
                percentile_service_time = max(results[k].servicetime for k in results if results[k].servicetime)

            for k in results:
                # (Can't do anything if we don't even have a start message!)
                if results[k].servicetime == None:
                    # Still no final result

                    job_waittime = time.perf_counter() - results[k].starttime
                    if job_waittime > percentile_service_time * timeout_multiplier + extra_wait:
                        reaper(k)
                        # (Note that nothing stops reaper from being called more than once on a single job. 
                        #  Beware of this!)


    # Mop up any remaining messages from threads that have quit:
    process_result_messages(timeout = 0.1) 
    # (timeout = 0 ought to be adequate but i don't know if queue likes that)

    # Record perf statistics and get going:
    Perflogger.cur().record("launch_batch_jobcount", len(results))
    Perflogger.cur().record("launch_batch_svctime", sum(r.servicetime for r in results.values() if r.servicetime))
    Perflogger.cur().record("launch_batch_maxsvctime", max(r.servicetime for r in results.values() if r.servicetime))
    Perflogger.cur().record("launch_batch_failcount", sum(1 for r in results.values() if r.exception or r.servicetime == None))
    if any(r.servicetime == None for r in results.values()):
        # (this was happening intermittently, but i think it's fixed --
        #  race between final message and child exited detected)
        eprint("\nWARNING! Some results not received in launch_batch\n")
        raise Exception("WTF")

    return results

def launch_batch_background(*args, **kwargs):
    def background_launch():
        return launch_batch(*args, **kwargs)

    batchthread = threading.Thread(target = background_launch, daemon = False)
    batchthread.start()
    return batchthread

def join_background_batch(batchthread : threading.Thread, timeout = None):
    batchthread.join(timeout)
    #todo might be nice to be able to return result set and also a ref to the thread's root perflogger...
    return not batchthread.is_alive()
    

## OLD launcher loop:
#def launch_batch(jobs, handler, max_concurrency : int, mp_if_fork : bool = False, raise_exceptions : bool = False,
#                 reaper = None, timeout_fraction : float = 1.0, timeout_multiplier : float = 1.1, extra_wait : float = 1.0):
#    if mp_if_fork and multiprocessing.get_start_method() == 'fork':
#        # (fork required for MP because pickling is inadequate for sharing required configuration structures)
#       conclib = multiprocessing
#       conc_constructor = conclib.Process
#       resultqueue = multiprocessing.Queue()
#    else:
#       conclib = threading
#       conc_constructor = conclib.Thread
#       resultqueue = queue.Queue()


#    assert(max_concurrency > 0)
#    sem = conclib.Semaphore(max_concurrency)
#    threads = {}
#    results = {}

#    def launch_thread_helper(job):
#        tp = time.perf_counter()
#        try:
#            resultqueue.put(LaunchResult(job, None, tp, None, None)) # Start message

#            result = handler(job)

#            resultqueue.put(LaunchResult(job, result, tp, time.perf_counter() - tp, None)) # Completion message
#        except Exception as e:
#            resultqueue.put(LaunchResult(job, None, tp, time.perf_counter() - tp, e)) # Abnormal completion message
#            raise
#        finally:
#            sem.release()


#    waitstart = time.perf_counter()

#    # Launch jobs, taking care not to exceed max concurrency:
#    for k in jobs:
#        sem.acquire()
#        threads[k] = conc_constructor(target = launch_thread_helper, args = (k,))
#        threads[k].start()


def slurm_runner(basepath : str, launchpath : str):
    def run_slurm(indiv_name : str):
        extra_args = []
        if "GABEE_SRUN_ARGS" in os.environ:
            extra_args = shlex.split(os.environ["GABEE_SRUN_ARGS"])
        
        # Disable multithreading in child sims
        # (It's never shown itself to be useful with sims sufficiently small to be practical under the GA, 
        #  and in any case the srun command below would need to be revised to prevent overcommitting and 
        #  consequent stupidity.)
        childenv = dict(os.environ)
        childenv["OMP_NUM_THREADS"] = "1"

        time.sleep(random.uniform(0, 3)) # add some spread to reduce hammering on fork
        # TODO allow shortening this if i'm trying to run things with gen time <30s
        # (env var?)                                                                      

        rc = subprocess.call(["srun", *extra_args, "-n1", "python", launchpath, basepath, indiv_name], env = childenv)

        if rc != 0:
            raise ChildProcessError("srun failed, code " + str(rc))

    return run_slurm


def initialize_tasklauncher(max_concurrency : int):
    # Do any initialization necessary for up to max_concurrency parallel tasks

    # Best-effort attempt to raise soft number-of-threads cap imposed by system to a sensible value
    # given the number of children we'll be launching
    try:
        import resource
        soft_nproc, hard_nproc = resource.getrlimit(resource.RLIMIT_NPROC)
        target_nproc = soft_nproc + 20 * max_concurrency
        resource.setrlimit(resource.RLIMIT_NPROC, (target_nproc, hard_nproc))
        os.environ["GABEE_WORKER_SET_NPROC_LIMIT"] = str(target_nproc)  # leave a note for children... hackish to go through env but easier this way...
        #eprint("INFO: nproc limit now " + str(resource.getrlimit(resource.RLIMIT_NPROC)[0]) + "\n")
    except ValueError:
        pass
    except ImportError:
        pass
    except Exception as e:
        eprint("Warning: unexpected exception raising nproc limit:")
        eprint(e)
