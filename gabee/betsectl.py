## GABEE control library for BETSE
##
## (C) Micah Z. Brodsky 2016-
##


import os
import shutil
import time
import math
import subprocess
import sys
import platform
import csv

# import only global dependencies here.
# some difficulty finding local dependencies when launched remotely.
# (being launched as a script and not as a module...)


def betse_test():
    result = subprocess.run(["python", "-m", "betse.cli", "--help"], stdout = subprocess.PIPE)
    return result.returncode == 0


def betse_runner_local(basepath : str):
    # NB: Use no GA dependencies; may be run standalone
    def run_betse(indiv_name : str):
        os.environ["BETSE_HEADLESS"] = "1" # (old hack, not used by currrent BETSE versions)
        if "DISPLAY" in os.environ:
            del os.environ["DISPLAY"]
        # (this is technically side-effectful but it shouldn't really matter.........)

        logpath = os.path.join(basepath, indiv_name, "betse-run.log")
        configpath = os.path.join(basepath, indiv_name, "config.yaml")
        # FIXME support alternatives to config.yaml here!!!!!!!!!
        # (also don't forget shhctl trimmer needs to know too)
        resultcodepath = os.path.join(basepath, indiv_name, "resultcodes.csv")
        with open(resultcodepath, "wt") as rclog:
            rclog.write("# stage, result code, node, running time (s)\n")
            nodename = platform.node()
            tp0 = time.perf_counter()

            rc1 = subprocess.call(["python", "-m", "betse.cli", "--log-file", logpath, "init", configpath])
            tp1 = time.perf_counter()
            rclog.write("init," + str(rc1) + "," + nodename + "," + str(tp1 - tp0) + "\n")

            rc2 = subprocess.call(["python", "-m", "betse.cli", "--log-file", logpath, "sim", configpath])
            tp2 = time.perf_counter()
            rclog.write("sim," + str(rc2) + "," + nodename + "," + str(tp2 - tp1) + "\n")

            rc3 = subprocess.call(["python", "-m", "betse.cli", "--log-file", logpath, "plot", "sim", configpath])
            tp3 = time.perf_counter()
            rclog.write("plot sim," + str(rc3) + "," + nodename + "," + str(tp3 - tp2) + "\n")

        # look at error return codes later. for some reason sometimes there's still full data written?
        # also partial data sometimes, which is easier but still not trivial to spot

    return run_betse

def betse_runner_slurm(basepath : str):
    return tasklauncher.slurm_runner(basepath, __file__)


if __name__ == '__main__':
    if len(sys.argv) != 3:
        print("Error: Expected 2 arguments, basepath and indiv_name\n", file = sys.stderr)
        sys.exit(1)

    # FIXME duplicated in shhctl maybe i should just fix importing so this doesn't need to happen :P

    target_nproc = int(os.environ.get("GABEE_WORKER_SET_NPROC_LIMIT", "0"))
    # (A bit hacky to pass thru env, but it's the easiest thing right now without lots of plumbing work...)
    if target_nproc:
        # Best-effort attempt to raise soft number-of-threads cap, in coordination with tasklauncher.initialize_tasklauncher
        # We don't actually launch many children, but apparently we still hit the limit due to the master if we happen to be
        # running on the same node, even if it's raised its own limit! This is pretty pathological, and it's entirely possible
        # that slurm will get entangled by the limit and we'll never launch, but in practice this seems to be adequate.....!

        # At least, as long as you only run one job on each node! If multiple jobs with same userid overlap on one's master
        # node, and the master is a high-concurrency job, bad things happen randomly. SLURM observed to fail to init child
        # processes, showing up as occasional mass simulation failures.

        try:
            import resource
            soft_nproc, hard_nproc = resource.getrlimit(resource.RLIMIT_NPROC)
            resource.setrlimit(resource.RLIMIT_NPROC, (target_nproc, hard_nproc))
            #print("INFO: worker nproc limit now " + str(resource.getrlimit(resource.RLIMIT_NPROC)[0]) + "\n")
        except Exception as e:
            # any exception would be a surprise, since the master already successfully got this limit
            print("Warning: unexpected exception raising worker nproc limit:", file=sys.stderr)
            print(e, file=sys.stderr)

    basepath = sys.argv[1]
    indiv_name = sys.argv[2]
    betse_runner_local(basepath)(indiv_name)
else:
    # here because launch-time dependencies argh    
    import gabee.tasklauncher as tasklauncher 
    import gabee.simctl as simctl
    from gabee.util import *

def betse_internal_data_trimmer(basepath : str):
    # Minimal trimmer that preserves all the human-readable output
    def trim_output(indiv_name : str):
        try:
            os.remove(os.path.join(basepath, indiv_name, "INITS", "world_1.betse.gz"))
            os.remove(os.path.join(basepath, indiv_name, "INITS", "init_1.betse.gz"))
            os.remove(os.path.join(basepath, indiv_name, "SIMS", "sim_1.betse.gz"))
        except FileNotFoundError:
            eprint("Warning: Couldn't find data files to remove for individual " + indiv_name + "\n")

    return trim_output

def betse_complete_trimmer(basepath : str):
    # Complete trimmer that erases targeted output dirs (as long as they look like output dirs and are relatively recent)
    def trim_output(indiv_name : str):
        # (note that indiv_name can presently include an assay subdir suffix)
        indiv_path = os.path.realpath(os.path.join(basepath, indiv_name))
        if (os.path.isdir(indiv_path)
            and os.path.isdir(os.path.join(indiv_path, "INITS"))
            and os.path.isdir(os.path.join(indiv_path, "SIMS"))
            and len(os.listdir(indiv_path)) < 40 # max 40 files (kinda hackish...)
            and time.time() - os.path.getmtime(indiv_path) < 60 * 60 * 12 # modtime in last 12h
            and time.time() - os.path.getctime(indiv_path) < 60 * 60 * 12): # "ctime" in last 12h

            # (We're super-cautious here, both because we don't want careless use to acccidentally lead to deletion of
            #  output dirs the user means to keep around, and also, so that a bug doesn't turn this into "rm -rf ~" or similar!)
            shutil.rmtree(indiv_path)
        elif os.path.exists(indiv_path):
            # If there was something there at that path, something peculiar is going on. Make some noise.
            # (At best just an old trajectory that was resumed after waiting a long time)
            eprint("Warning: Not removing data files for individual " + indiv_path
                   + " because sanity checks failed\n")
        else:
            # If there's nothing there, it may have been trimmed already e.g. by an aborted run.
            eprint("(Data files for individual " + indiv_path + " have already been removed)")

    return trim_output


def betse_output_loader(basepath : str, outputpath : str = None,
                        primaryfieldname : str = "Vmem", primaryfieldpath : str = os.path.join("Vmem2D_TextExport", "Vmem2D_")):
    if outputpath is None:
        outputpath = os.path.join("RESULTS", "sim_1")
        # (this path is not actually a constant, and this is just a halfassed default;
        #  it's normally determined by the "results file saving:" key in the BETSE config)

    return simctl.basic_output_loader(basepath, outputpath, primaryfieldname, primaryfieldpath)

    #vmem_pathsuffix = os.path.join(outputpath, primaryfieldpath)

    #def load_output(indiv_name : str):
    #    output = simctl.SimOutput(indiv_name)
        
    #    # Read and fill in Vmem data
    #    vmem_field = simctl.read_output_field(os.path.join(basepath, indiv_name, vmem_pathsuffix))

    #    output.output_fields[primaryfieldname] = vmem_field
    #    output.default_field = primaryfieldname
    #    output.is_empty = (len(vmem_field) == 0)

    #    # Assign default Voronoi-based cell topology; we're not (yet?) bothering trying to load what BETSE used internally
    #    # Use same immutable TopologyInfo object for consecutive, indentially arranged fields
    #    last_positions = None
    #    topoinfo = None
    #    for frame in vmem_field:
    #        frame_positions = frame.keys()
    #        if frame_positions != last_positions:
    #            topoinfo = simctl.TopologyInfo(frame_positions)
    #            topoinfo.set_default_topology()
    #            last_positions = frame_positions
    #        output.topologies.append(topoinfo)

    #    # Read and fill in metadata about the simulation run
    #    resultcodepath = os.path.join(basepath, indiv_name, "resultcodes.csv")
    #    resultstatus = read_output_resultstatus(resultcodepath)
    #    output.additional_data["resultcodes"] = resultstatus

    #    if len(resultstatus) > 0:
    #        # set success code as first failing code, if any
    #        codes = [row["resultcode"] for row in resultstatus]
    #        output.success_code = next((code for code in codes if code != 0), 0)

    #        output.running_time = sum((row["runtime"] for row in resultstatus))

    #    return output
    
    #return load_output


def guess_outputpath(betse_config):
    try:
        return betse_config["results file saving"]["sim directory"]
    except Exception:
        eprint("Warning: Couldn't figure out --outdatapath from BETSE config.\n")
        return "RESULTS/sim_1"

def guess_monitorimages(betse_config):
    try:
        outputpath = betse_config["results file saving"]["sim directory"]
        totaltime = betse_config["sim time settings"]["total time"]
        samplingrate = betse_config["sim time settings"]["sampling rate"]
        lastframe = math.ceil(totaltime / samplingrate) - 2  # BETSE's behavior is a little weird and so is this calculation...
        imgpath = os.path.join(outputpath, "anim_while_solving", "Vmem", "Vmem_%07d.png" % (lastframe)) # Hard-coded guess!
        return [imgpath]
    except Exception:
        # Best effort, just give up if it doesn't work
        return []


def get_simlauncher(basepath : str, outputpath : str):
    return simctl.SimLauncher(sim_name = "BETSE",
                              local_runner = betse_runner_local(basepath),
                              slurm_runner = betse_runner_slurm(basepath),
                              test_runner = betse_test,
                              output_loader = betse_output_loader(basepath, outputpath),
                              output_trimmer = betse_complete_trimmer(basepath))
