## GABEE simulation interface support library
##
## (C) Micah Z. Brodsky 2016-
##


import os
import csv
import math

from gabee.util import *


class SimLauncher:
    # Class encapsulating a subsidiary simulator controller; packages together all the handler routines

    def __init__(self, sim_name, local_runner, slurm_runner, test_runner, output_loader, output_trimmer):
        self.sim_name = sim_name
        self.local_runner = local_runner
        self.slurm_runner = slurm_runner
        self.test_runner = test_runner
        self.output_loader = output_loader
        self.output_trimmer = output_trimmer


# Classes representing simulation output data:

class SimOutput:
    # Class representing the output data of a single simulation run
    # Treat as immutable once filled out

    def __init__(self, indiv_name : str):
        self.output_fields = {}
        self.topologies = []
        self.success_code = None
        self.running_time = None
        self.is_empty = True
        self.default_field = None
        self.indiv_name = indiv_name
        self.additional_data = {}
        #self.score_failed_runs = False  # (the only field mutable after construction)


class TopologyInfo:
    # Class representing a set of cells and their neighbor relationships
    # Treat as immutable once constructed.

    def __init__(self, positions):
        self.positions = [p for p in positions]
        self.neighbors = {}
        # todo self.is_boundary

    def set_default_topology(self):
        # Default computation for inferring topology using Voronoi tesselation
        # This is unsatisfactory in some ways -- nodes at jagged boundaries may pick up phantom neighbors, e.g.,
        # but it's a start.

        import scipy.spatial
        vor = scipy.spatial.Voronoi(self.positions)
        
        for p in self.positions:
            self.neighbors[p] = set()

        for i in range(len(vor.ridge_points)):
            pointidx1, pointidx2 = vor.ridge_points[i]
            p1 = self.positions[pointidx1]
            p2 = self.positions[pointidx2]
            dist = pos_dist(p1, p2)
            vidx1, vidx2 = vor.ridge_vertices[i]
            # Exclude spurious "neighbors" for whom the edge itself is farther away than their separation distance
            # (not perfect, but does a decent job of weeding out many spurious boundary neighbors)
            if (vidx1 != -1 and pos_dist(p1, vor.vertices[vidx1]) < dist or
                vidx2 != -1 and pos_dist(p1, vor.vertices[vidx2]) < dist):
                self.neighbors[p1].add(p2)
                self.neighbors[p2].add(p1)

    def get_neighbors(self, p):
        return self.neighbors[p]

    def get_neighborhood_within_range(self, p, range : float):
        # Returns connected neighborhood within range distance (including origin point)
        neighborhood = set()

        def walk_neighbor(neighbor):
            if pos_dist(p, neighbor) <= range:
                neighborhood.add(neighbor)
                for n2 in self.get_neighbors(neighbor):
                    if n2 not in neighborhood:
                        walk_neighbor(n2)
        walk_neighbor(p)

        #assert neighborhood == {n for n in get_neighbors(p, range, self.positions)} ##########

        return neighborhood

def pos_dist(pos1, pos2): # (note: duplicate in output_anal...)
    return math.hypot(pos2[0] - pos1[0], pos2[1] - pos1[1])


# General purpose output loaders and helper routines:

def basic_output_loader(basepath : str, outputpath : str, primaryfieldname : str, primaryfieldpath : str):
    # Pretty basic output loader for BETSE, SwHoHex, and similarly designed simulators

    vmem_pathsuffix = os.path.join(outputpath, primaryfieldpath)

    def load_output(indiv_name : str):
        output = SimOutput(indiv_name)
        
        # Read and fill in Vmem data
        vmem_field = read_output_field(os.path.join(basepath, indiv_name, vmem_pathsuffix))

        output.output_fields[primaryfieldname] = vmem_field
        output.default_field = primaryfieldname
        output.is_empty = (len(vmem_field) == 0)

        # Assign default Voronoi-based cell topology; we're not (yet?) bothering trying to load what BETSE used internally
        # Use same immutable TopologyInfo object for consecutive, indentially arranged fields
        last_positions = None
        topoinfo = None
        for frame in vmem_field:
            frame_positions = frame.keys()
            if frame_positions != last_positions:
                topoinfo = TopologyInfo(frame_positions)
                topoinfo.set_default_topology()
                last_positions = frame_positions
            output.topologies.append(topoinfo)

        # Read and fill in metadata about the simulation run
        resultcodepath = os.path.join(basepath, indiv_name, "resultcodes.csv")
        resultstatus = read_output_resultstatus(resultcodepath)
        output.additional_data["resultcodes"] = resultstatus

        if len(resultstatus) > 0:
            # set success code as first failing code, if any
            codes = [row["resultcode"] for row in resultstatus]
            output.success_code = next((code for code in codes if code != 0), 0)

            output.running_time = sum((row["runtime"] for row in resultstatus))

        return output
    
    return load_output


def read_output_resultstatus(resultcodepath):
    # Read CSV file (written manally by the simrunner routine) with sim subprocess result codes, etc.

    stages = []
    if os.path.exists(resultcodepath):
        with open(resultcodepath, "rt") as csvfile:
            rdr = csv.reader(csvfile)
            for row in rdr:
                if len(row) > 0 and len(row[0]) > 0 and row[0][0] != "#":
                    # fixme ugh
                    stages.append({"stage" : row[0], "resultcode" : int(row[1]), "node" : row[2], "runtime" : float(row[3])})

    return stages


def read_output_field(fieldbasepath):
    # Read a batch of CSV files representing an output timeseries as typically formatted by GABEE, SwHoHex, etc.
    # (todo really would be nice to be more flexible and not insist we know the file naming scheme...)

    frames = []
    framecount = 0
    while os.path.exists(fieldbasepath + str(framecount) + ".csv"):
        with open(fieldbasepath + str(framecount) + ".csv", "rt") as csvfile:
            frames.append({})
            rdr = csv.reader(csvfile)
            linecount = 0
            for row in rdr:
                # Skip blank, comment, and degenerate lines
                if len(row) > 0 and len(row[0]) > 0 and row[0][0] != "#":
                    linecount += 1
                    try:
                        # todo validation / warning?
                        frames[framecount][(float(row[0]), float(row[1]))] = float(row[2])
                    except ValueError:
                        # Drop format errors on the first line, it's probably column headings. Otherwise raise.
                        if linecount > 1:
                            raise
        framecount += 1

    return frames

