## GABEE progress monitor GUI
##
## (C) Micah Z. Brodsky 2016-
##


import math
import os
import matplotlib.pyplot as plt
import matplotlib.image

import gabee.launcher as launcher
import gabee.output_anal as output_anal
from gabee.util import *


snapshot_path = None

class UiInitError(Exception):
    pass

class GabeePlotABC:
    def __init__(self, trajectory : launcher.EvTrajectory):
        try:
            self.fig = plt.figure()
        except Exception as e:
            # Guard for backend / X errors
            raise UiInitError(str(e)) from e

        self.assign_default_caption()

        self.closed = False
        self.close_cid = self.fig.canvas.mpl_connect('close_event', self._close_handler)
        # (ugh beware mpl_connect holds only a weakref -- http://matplotlib.org/users/event_handling.html)

        self.trajectory = trajectory
        trajectory.updatehandlers.append(self.update)


    def _close_handler(self, evt):
        self.close_handler()

    def close_handler(self):
        self.closed = True

    def update(self):
        if self.closed:
            return

        plt.figure(self.fig.number)
        self.fig.clear()

        self.update_content()

        self.update_fig()

    def update_content(self):
        # Abstract method for common plot update path 
        raise NotImplementedError()

    def update_fig(self):
        self.fig.canvas.draw()
        self.fig.show()
        if snapshot_path:
            rawtitle = self.caption  # self.fig.canvas.get_window_title()
            cleanedtitle = rawtitle.replace("/", "_").replace("\\", "_").replace(":", " - ")
            # Obviously this is not complete sanitization -- it's just to remove things that occur in existing 
            # captions (which includes filename fragments). If weird characters are added to captions, they need
            # to be added sanitization above too. 
        
            if not os.path.exists(snapshot_path):
                os.mkdir(snapshot_path)
            self.fig.savefig(os.path.join(snapshot_path, cleanedtitle + ".png"))
            
        plt.pause(0.001)

    @property
    def caption(self):
        return self._caption

    @caption.setter
    def caption(self, value):
        self._caption = value
        self.fig.canvas.set_window_title(value)

    global_figurecount = 0

    def assign_default_caption(self):
        # Generate a caption our way, because matplotlib's is backend-dependent and broken for Agg
        GabeePlotABC.global_figurecount += 1
        self.caption = "Figure " + str(GabeePlotABC.global_figurecount)


class trajectory_monitor(GabeePlotABC):
    def __init__(self, trajectory : launcher.EvTrajectory, show_avg : bool = True,
                 show_median : bool = False, show_zeros : bool = True, show_ancestral : bool = False, caption : str = None):
        super().__init__(trajectory)

        if caption:
            self.caption += " - " + caption

        self.show_avg = show_avg
        self.show_median = show_median
        self.show_zeros = show_zeros
        self.show_ancestral = show_ancestral

    def update_content(self):
        plt.title("Fitness vs. generation #")
        plt.plot(self.trajectory.maxfitnesshistory(), label = "Best fitness")
        if self.show_avg:
            plt.plot(self.trajectory.avgfitnesshistory(), "g:", label = "Avg. fitness")
        if self.show_median:
            plt.plot(self.trajectory.medianfitnesshistory(), "r--", label = "Median fitness")
        if self.show_zeros:
            # Plot the simulation failure count as 'x'es
            zfh = self.trajectory.zerofitnesshistory()
            if any(zfh):
                sec_axis = self.fig.axes[0].twinx()
                sec_axis.set_ylim(0)
                sec_axis.plot([i for i in range(0, len(zfh)) if zfh[i] > 0],
                              [n for n in zfh if n > 0], "rx", label = "Sim failures")
                #_, ymax = sec_axis.get_ylim()
                #sec_axis.set_yticks(range(0, int(round(ymax)) + 3))
                ymax = max(zfh)
                ytick = max(int(ymax / 10), 1)
                sec_axis.set_yticks(range(0, ymax + ytick + 2, ytick))
        if self.show_ancestral:
            # Plot the full fitness history of the current best individual's lineage
            cur_best, _ = self.trajectory.best_current()
            if cur_best:
                maxfit_ancestry = self.trajectory.trace_ancestry(cur_best)
                ancestor_fithistory = []
                ancestor_gennums = []
                for i in range(len(maxfit_ancestry)):
                    # fixme the logic shouldn't be digging around like this from outside of EvTrajectory
                    ancestor_fithistory.append(self.trajectory.history[self.trajectory.length() - i - 1][maxfit_ancestry[i]])
                    ancestor_gennums.append(self.trajectory.length() - i - 1)

                self.fig.axes[0].plot(ancestor_gennums, ancestor_fithistory, "y--", label = "Ancestral fitness")
                # (Specify axes explicitly because the pyplot model is a PoS and statefully picks up the sim failures axis)

        gens = self.trajectory.length()
        if gens > 0:
            xbase = math.pow(10, math.floor(math.log10(2 * gens)))
            xrange = xbase * math.pow(2, math.ceil(math.log2(gens / xbase)))
            plt.xlim((0, xrange))

        self.fig.axes[0].set_xlabel("Generation #")
        self.fig.axes[0].set_ylabel("Fitness")
        self.fig.axes[0].legend(sum([ax.get_legend_handles_labels()[0] for ax in self.fig.axes], []),
                                sum([ax.get_legend_handles_labels()[1] for ax in self.fig.axes], []),
                                loc = "lower right", framealpha = 0.5)
        # (kinda stupid to have to do this...)
        # (http://matplotlib.org/examples/axes_grid/demo_parasite_axes2.html might have a better way...)


class imagefile_monitor(GabeePlotABC):
    def __init__(self, imagepath : str, trajectory : launcher.EvTrajectory, config : launcher.GaConfig):
        super().__init__(trajectory)

        self.imagepath = imagepath
        self.config = config

    def update_content(self):
        best_indiv = self.trajectory.best_current()[0]
        if best_indiv:
            image_fullpath = os.path.join(self.config.basepath, best_indiv, self.imagepath)

        try:
            img = matplotlib.image.imread(image_fullpath)
        except Exception:
            img = None

        if not img is None:
            plt.title("Best individual: " + best_indiv)
            plt.imshow(img)


class imagetableau_monitor(GabeePlotABC):
    # A monitor showing a tableau of images from individuals spaced throughout the population

    def __init__(self, imagepath : str, count : int, trajectory : launcher.EvTrajectory, config : launcher.GaConfig):
        super().__init__(trajectory)

        self.imagepath = imagepath
        self.count = count
        self.config = config

    def update_content(self):
        pop_fitness = self.trajectory.pop_fitness()
        ranked_pairs = sorted([(pop_fitness[k], k) for k in pop_fitness], reverse = True)

        imgcount = min(self.count, len(pop_fitness))
        if imgcount > 0:
            dim1 = math.ceil(math.sqrt(imgcount))
            dim2 = math.ceil(imgcount / dim1)

            for i in range(imgcount):
                index = math.floor(i * len(pop_fitness) / imgcount)
                indiv = ranked_pairs[index][1]

                image_fullpath = os.path.join(self.config.basepath, indiv, self.imagepath)

                try:
                    img = matplotlib.image.imread(image_fullpath)
                except Exception:
                    img = None

                ax = self.fig.add_subplot(dim1, dim2, i + 1)
                ax.clear()
                if not img is None:
                    ax.imshow(img)

                ax.xaxis.set_visible(False)
                ax.yaxis.set_visible(False)

                ax.set_title("%s -- %f" % (indiv, pop_fitness[indiv]))
                ax.title.set_fontsize("small")


class imageanimate_monitor(GabeePlotABC):
    def __init__(self, imagepath : str, trajectory : launcher.EvTrajectory, config : launcher.GaConfig, interval_ms : int = 500):
        super().__init__(trajectory)

        self.imagepath = imagepath
        self.config = config
        self.curgennum = 0

        self.fig.show()
        self.timer = self.fig.canvas.new_timer(interval_ms)
        self.timer.add_callback(self.advance_frame)
        self.timer.start()
        #self.close_handler = lambda evt: (self.timer.remove_callback(self.advance_frame) and False) or (self.timer.stop()  and False) or (self.fig.canvas.mpl_disconnect(self.close_cid) and False) or print("glog") #holds only weakref? wtf? http://matplotlib.org/users/event_handling.html

        # QT backend gives an error about timer during shutdown?! TimerQT.__del__ -- TypeError: 'method' object is not connected

    def close_handler(self):
        self.timer.stop()
        super().close_handler()

    def advance_frame(self):
        if len(self.trajectory.bestindivs) > 0:
            self.curgennum = (self.curgennum + 1) % len(self.trajectory.bestindivs)
            self.update()

    def update(self):
        # (Override update, not update_content, because we don't want disk snapshots)
        if self.closed:
            return

        if len(self.trajectory.bestindivs) > 0:
            best_indiv = self.trajectory.bestindivs[self.curgennum][0]
            indiv_fitness = self.trajectory.bestindivs[self.curgennum][1]
            image_fullpath = os.path.join(self.config.basepath, best_indiv, self.imagepath)

        try:
            img = matplotlib.image.imread(image_fullpath)
        except Exception:
            img = None

        plt.figure(self.fig.number)
        self.fig.clear()

        if not img is None:
            plt.title("Gen #" + str(self.curgennum) + " best individual:\n" + best_indiv + " -- " + str(indiv_fitness))
            plt.imshow(img)

        self.fig.canvas.draw()


def get_path_name_truncated(pathstr, maxlenchars : int = 30):
    # simple helper function for legends -- truncate string to size from left
    if len(pathstr) > maxlenchars:
        pathstr = "..." + pathstr[len(pathstr) - maxlenchars + 3:]
    return pathstr

def get_func_name_truncated(func, maxlenchars : int = 30):
    # simple helper function for legends -- truncate function name to size
    labelstr = str(func)
    if len(labelstr) > maxlenchars:
        labelstr = labelstr[:maxlenchars - 3] + "..."
    return labelstr

class auxfunc_monitor(GabeePlotABC):
    def __init__(self, auxfuncs, trajectory : launcher.EvTrajectory, config : launcher.GaConfig):
        super().__init__(trajectory)

        self.auxfuncs = auxfuncs if isinstance(auxfuncs, list) else [auxfuncs]
        self.auxhistory = [[] for f in self.auxfuncs]
        self.config = config

        for indiv, _ in trajectory.bestindivs:
            self.record_new_values(indiv)
             # fixme this could be more efficient -- using a parallel batch load, and memoizing 
             # so that multiple aux plots didn't repeat the same loads...

    def record_new_values(self, indiv):
        indiv_output = self.config.load_output(indiv)

        for i in range(len(self.auxfuncs)):            
            indiv_config = self.trajectory.saved_configs.get(indiv) # fish out of max history
            newval = output_anal.compute_fitness(self.auxfuncs[i], indiv, indiv_config, indiv_output)
            self.auxhistory[i].append(newval)

    def update_content(self):
        while len(self.trajectory.bestindivs) > len(self.auxhistory[0]):
            self.record_new_values(self.trajectory.best_current()[0])

        plt.title("Auxiliary functions")

        for i in range(len(self.auxfuncs)):
            plt.plot(self.auxhistory[i], label = get_func_name_truncated(self.auxfuncs[i]))

        gens = self.trajectory.length()
        if gens > 0:
            xbase = math.pow(10, math.floor(math.log10(2 * gens)))
            xrange = xbase * math.pow(2, math.ceil(math.log2(gens / xbase)))
            plt.xlim((0, xrange))

        self.fig.axes[0].set_xlabel("Generation #")

        legend = self.fig.axes[0].legend(loc = "lower right", framealpha = 0.5)
        if len(self.auxfuncs) > 2:
            # Shrink font to prevent unwieldy, large legend
            plt.setp(self.fig.axes[0].get_legend().get_texts(), fontsize='xx-small')


class pointfunc_monitor(GabeePlotABC):
    def __init__(self, pointfunc, trajectory : launcher.EvTrajectory, config : launcher.GaConfig):
        super().__init__(trajectory)

        self.func = pointfunc
        self.config = config

    def update_content(self):
        plt.title("Auxiliary point function: " + get_func_name_truncated(self.func))

        best_indiv = self.trajectory.best_current()[0]
        if best_indiv:
            indiv_output = self.config.load_output(best_indiv)
            indiv_config = self.trajectory.population[best_indiv]
            outputview = output_anal.simoutputview(indiv_output, indiv_config)
            # todo add a way to plot using non-default assays?

            positions = [pos for pos in outputview.positions()]
            plt.scatter([p[0] for p in positions], [p[1] for p in positions], 100,
                        cmap = 'RdBu_r', c = [self.func(outputview, p) for p in positions])
            plt.colorbar()

            # Center the plot nicely while maintaining 1:1 aspect ratio... (annoying!)
            #plt.axis("square")
            xmin, xmax = plt.xlim()
            ymin, ymax = plt.ylim()
            maxdim = max(xmax - xmin, ymax - ymin)
            xmid = (xmin + xmax) / 2
            ymid = (ymin + ymax) / 2
            plt.xlim(xmid - maxdim / 2, xmid + maxdim / 2)
            plt.ylim(ymid - maxdim / 2, ymid + maxdim / 2)


# todo less crappy performance monitor

class live_perf_qdmonitor(GabeePlotABC):
    # really crappy hacky performance monitor...
    def __init__(self, trajectory : launcher.EvTrajectory, config : launcher.GaConfig, extra_detail : bool = False):
        global Perflogger
        from gabee.perflog import Perflogger

        super().__init__(trajectory)

        self.perfhistory = [[], [], []]
        self.fieldlabels = ["Generation time", "Sim batch time", "Avg sim runtime"]
        self.fields = ["ga_gen_time", "launch/time", self.query_avgsimtime]
        self.hackedfields  = {0} # fields to apply hemi-batch alignment hack to :p
        if extra_detail:
            self.perfhistory += [[], [], [], [], []]
            self.fieldlabels += ["Output load time", "Fitness eval time", "Checkpoint time", "Trim time", "Config prep time"]
            self.fields += ["load_output/time", "fitness/time", "checkpoint/time", "trim/time", "prep_config/time"]
        self.config = config

        self.caption = "PerfMonitor"
        if os.environ.get("SLURM_JOB_ID"):
            self.caption += " - Job #" + os.environ.get("SLURM_JOB_ID")
            
    def query_avgsimtime(self):
        return Perflogger.query_path("launch/launch_batch_svctime") / Perflogger.query_path("launch/launch_batch_jobcount")

    def append_sample(self):
        gens = max(len(perfline) for perfline in self.perfhistory)
            
        for i in range(len(self.fields)):
            if isinstance(self.fields[i], str):
                newdatum = Perflogger.query_path(self.fields[i])
            else:
                newdatum = self.fields[i]()

            # (first hemi-batch gen time isn't recorded, and second isn't recorded _yet_. Hack around this. :P )
            # (making sure to get correct alignment on both fresh runs and checkpoint resumes is _annoying_.)
            # ((currently unsure if checkpoint time is correctly aligned on resume))
            if i not in self.hackedfields or (newdatum or gens > 0): # hack :P
                self.perfhistory[i].append(newdatum)

    def update_content(self):
        self.append_sample()

        plt.title("Generation latency")

        for i in range(len(self.perfhistory)):
            plt.plot(self.perfhistory[i], label = self.fieldlabels[i])

        gens = max(len(perfline) for perfline in self.perfhistory)
        if gens > 0:
            xbase = math.pow(10, math.floor(math.log10(2 * gens)))
            xrange = xbase * math.pow(2, math.ceil(math.log2(gens / xbase)))
            plt.xlim((0, xrange))

        self.fig.axes[0].set_xlabel("Batch #")

        self.fig.axes[0].set_ylim(ymin = 0)
        self.fig.axes[0].set_ylabel("Time (s)")

        legend = self.fig.axes[0].legend(loc = "lower right", framealpha = 0.5)
        # Shrink font to prevent unwieldy, large legend
        plt.setp(self.fig.axes[0].get_legend().get_texts(), fontsize='xx-small')


def attach_ui(trajectory : launcher.EvTrajectory, config : launcher.GaConfig, image_path : str = None,
              show_avg : bool = True, show_median : bool = False, show_ancestral : bool = False, caption : str = None):    
    fitmon = trajectory_monitor(trajectory, show_avg = show_avg, show_median = show_median, 
                                show_ancestral = show_ancestral, caption = caption) # (may raise UiInitError)
    fitmon.update()
    if image_path:
        immon = imagefile_monitor(image_path, trajectory, config)
         # (may raise UiInitError in theory, probably not in practice)
        immon.update()
    
    assert(config.idler == None or config.idler == ui_idler)  # (If we're fighting over idlers, then the design needs to be changed, e.g. chaining)
    config.idler = ui_idler


def ui_idler():
    # process UI messages
    plt.pause(0.001)

def wait_until_closed():
    plt.show()    

