## GABEE command line tool for extracting from trajectory files
##
## (C) Micah Z. Brodsky 2016-
##


import argparse
import yaml

import gabee.launcher as launcher
import gabee.output_anal as output_anal
#import gabee.monitorui as monitorui
from gabee.util import *



def run_trajtool_cli():

    # Lots of commented out code here, sketch-bits for future planned features!


    argparser = argparse.ArgumentParser(prog = "python -m gabee.trajtool", description = "Extract data from a trajectory file")

    argparser.add_argument("trajectory", metavar = "TRAJECTORY_FILE", help = "trajectory file to read (required)")

    argparser.add_argument("--extra-stats", action = "store_true", help = "show additional summary statistics")

    argparser.add_argument("--history", action = "store_true", help = "export fitness history in CSV format")

    argparser.add_argument("--trait", action = "append", help = "include trait TRAIT in the history table")

    argparser.add_argument("--plot", action = "store_true", help = "show fitness history plot")

    #argparser.add_argument("--aux-function", action = "append") 

    argparser.add_argument("--summarize-indiv", metavar = "INDIV_NAME", help = "export YAML summary of the given individual's configuration")

    #argparser.add_argument("--extract-indiv-delta")

    #argparser.add_argument("--configfile") 

    #argparser.add_argument("--templatepath") 


    ##add_argument("--workpath", required = True) 


    args = argparser.parse_args()


    #if args.configfile:
    #    configpath = args.configfile
    #elif args.templatepath:
    #    configpath = os.path.join(args.templatepath, "config.yaml")
    #else:
    #    configpath = None

    #if configpath:
    #    config = load_config(args.configfile)


    ##config = load_config(args.configfile)
    ##patches = load_config(args.patchfile)
    

    traj = launcher.EvTrajectory()
    traj.restore_checkpoint(load_config(args.trajectory))

    #if args.aux_function:
    #    auxfuncs = []
    #    for func in args.aux_function:
    #        auxmons.append(output_anal.eval_fitfunc_str(func))


    if args.history:

        avghistory = traj.avgfitnesshistory()
        failhistory = traj.zerofitnesshistory()

        heading = ", ".join(["Generation", "Best_Individual", "Best_Fitness", "Avg_Fitness", "Num_Failures"])
        if args.trait:
            heading += ", " + ", ".join(args.trait)
        print(heading)

        for i in range(len(traj.history)):
            genline = "%d, %s, %f, %f, %d" % (i, traj.bestindivs[i][0], traj.bestindivs[i][1], avghistory[i], failhistory[i])
            if args.trait:
                indiv_config = traj.saved_configs[traj.bestindivs[i][0]]
                genline += ", " + ", ".join(str(indiv_config[traitname][0]["Value"]) for traitname in args.trait)
                # TODO sensible error message when traitname is not present
                # FIXME escaping (use csv package?) for compound traits like sets

            #if args.aux_function:
            #    indiv_config = traj.saved_configs[traj.bestindivs[i][0]]
            #    indiv_output = config.load_output(traj.bestindivs[i][0])

            #    for i in range(len(auxfuncs)):            
            #        funcval = output_anal.compute_fitness(auxfuncs[i], traj.bestindivs[i][0], indiv_config, indiv_output)
            #        genline += ", " + str(funcval)

            print(genline)


    if args.summarize_indiv:
        indiv_name = args.summarize_indiv
        if indiv_name in traj.saved_configs:

            bindings = traj.saved_configs[indiv_name]

            # hacky reformatting bindings to be less hacky for human consumption:
            print(yaml.dump({name : [bindings[name][0], {"OrigValue" : bindings[name][1]}] for name in bindings}))
        else:
            eprint("Error: No such individual '" + indiv_name + "' saved in trajectory")
            sys.exit(1)



    #if args.extract_indiv_delta:
    #    indiv_name = args.extract_indiv_delta
    #    if indiv_name in traj.saved_configs:

    #        newpattern = fragmatcher.update_pattern_values(config.patchpattern, config.patchbindings, indiv_bindings)
    #        (newconfig, patchpattern, patchbindings) = fragmatcher.match_and_patch_pattern(config, patches)

    #        print(yaml.dump(traj.saved_configs[indiv_name]))
    #    else:
    #        eprint("Error: No such individual '" + indiv_name + "' saved in trajectory")
    #        sys.exit(1)


    if not args.summarize_indiv and not args.history:
        # Default summary

        print("Trajectory summary")
        print("------------------")
        print("")
        print("Population size:", len(traj.population))
        print("Generations:", traj.length())
        print("Final best fitness:", traj.best_current()[1]);
        if args.extra_stats:
            maxfithistory = traj.maxfitnesshistory()
            avgfithistory = traj.avgfitnesshistory()
            failhistory = traj.zerofitnesshistory()

            print("Final avg fitness: ", avgfithistory[-1])
            print("Final best individual:", traj.best_current()[0]);
            print("")

            print("Entire run avg best fitness: ", sum(maxfithistory) / traj.length())
            print("Entire run avg fitness: ", sum(avgfithistory) / traj.length())
            print("Entire run failure rate: ", sum(failhistory) / traj.length())
            print("Worst gen failure rate: ", max(failhistory)) # <-- worth keeping watch on to catch cluster troubles

            windowlen = 10
            maxfitwindow = maxfithistory[traj.length() - windowlen : traj.length()]
            avgfitwindow = avgfithistory[traj.length() - windowlen : traj.length()]
            failwindow = failhistory[traj.length() - windowlen : traj.length()]
            print("Last " + str(windowlen) + " gens avg best fitness: ", sum(maxfitwindow) / len(maxfitwindow))
            print("Last " + str(windowlen) + " gens avg fitness: ", sum(avgfitwindow) / len(avgfitwindow))
            print("Last " + str(windowlen) + " gens failure rate: ", sum(failwindow) / len(failwindow))

        print("")

        print("Command history:")

        import shlex
        for cmdargs in traj.command_history:
            print(" ".join(shlex.quote(arg) for arg in cmdargs), "\n")
    
    if args.plot:
        import gabee.monitorui as monitorui
        fitmon = monitorui.trajectory_monitor(traj)
        fitmon.update()
        monitorui.wait_until_closed()
            


if __name__ == '__main__':
    run_trajtool_cli()