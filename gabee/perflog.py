## Simple logger for performance measurements (for GABEE)
##
## (C) Micah Z. Brodsky 2017-
##

import threading
import time


# Logical environment stack implemented with thread local storage:

def tlspush(tlscell, value):
    oldstack = getattr(tlscell, "stack", [])
    oldstack.append(value)
    tlscell.stack = oldstack

def tlspeek(tlscell, default = None):
    stack = getattr(tlscell, "stack", [])
    if stack:
        return stack[-1]
    else:
        return default

def tlspop(tlscell):
    oldstack = getattr(tlscell, "stack", [])
    return oldstack.pop()


# Actual data collector class:

class Perflogger:
    curlogger_tls_ = threading.local()

    def __init__(self):
        self.data = {}  # slot -> value
        self.last_data = None  # slot -> value
        self.datatotals = {}  # slot -> value
        self.subloggers = {}  # slot -> subsidiary perflogger
        self.entertime = None
        self.last_time = None
        self.total_time = 0
        self.total_count = 0

    def __enter__(self):
        assert(self.entertime == None) 
        tlspush(Perflogger.curlogger_tls_, self)
        self.entertime = time.perf_counter()
        self.total_count += 1

    def __exit__(self, *exception_args):
        self.last_time = time.perf_counter() - self.entertime
        self.total_time += self.last_time
        self.entertime = None
        self.record("time", self.last_time)
        self.collect()
        popped = tlspop(Perflogger.curlogger_tls_)
        assert(popped == self)

    @staticmethod
    def cur():
        cur_logger = tlspeek(Perflogger.curlogger_tls_)
        if cur_logger:
            return cur_logger
        else:
            # Set up thread-default logger
            tlspush(Perflogger.curlogger_tls_, Perflogger())
            return Perflogger.cur()

    @staticmethod
    def sub(slot):
        cur_logger = Perflogger.cur()
        if slot not in cur_logger.subloggers:
            cur_logger.subloggers[slot] = Perflogger()

        return cur_logger.subloggers[slot]

    @staticmethod
    def query_path(path : str, base = None, querytype = "last_data"):
        if base == None:
            logger = Perflogger.cur()
        else:
            logger = base

        path_elems = path.split("/")
        for subname in path_elems[0:-1]:
            logger = logger.sub(subname)

        if querytype == "last_data":
            return logger.last_data[path_elems[-1]] if logger.last_data else None
        elif querytype == "cur_data":
            return logger.data[path_elems[-1]] if logger.data else None
        elif querytype == "logger":
            return logger.sub(path_elems[-1])
        else:
            raise ValueError("bad querytype " + str(querytype))

    def record(self, slot, value):
        adjoin_sum_kv(self.data, slot, value)
        
    def collect(self):
        self.last_data = self.data
        self.data = {}
        for k in self.last_data:
            adjoin_sum_kv(self.datatotals, k, self.last_data[k])
        return self.last_data

def adjoin_sum_kv(target, slot, value):
    if slot in target:
        target[slot] += value
    else:
        target[slot] = value


