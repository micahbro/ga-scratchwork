## GABEE population selection routines
##
## (C) Micah Z. Brodsky 2016-
##


import random

from gabee.util import *

def tournament_select(pop_fitness : dict, tourney_size : int) -> str:
    pop_ids = list(pop_fitness.keys())

    best_fitness = None
    best_id = None
    for i in range(tourney_size):
        trial_id = pop_ids[random.randrange(0, len(pop_ids))]
        if best_fitness is None or pop_fitness[trial_id] > best_fitness:
            best_fitness = pop_fitness[trial_id]
            best_id = trial_id

    return best_id

def tournament_selector(tourney_size : int) -> str:
    def tournament_select_n(pop_fitness : dict):
        return tournament_select(pop_fitness, tourney_size)

    return tournament_select_n


def fitprop_select(pop_fitness : dict):
    # naive proportional selection. don't throw _giant_ population sizes at it or it could get slow!
    total_fitness = sum(pop_fitness.values())
    r = random.random() * total_fitness
    
    # (todo screen for negative fitness values?)

    fitness_tallied = 0
    for id in pop_fitness:
        fitness_tallied += pop_fitness[id]
        if fitness_tallied >= r:
            return id
    

def max_select(pop_fitness : dict) -> str:
    return argmax(pop_fitness)


