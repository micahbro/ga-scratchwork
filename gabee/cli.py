## GABEE command line interface
##
## (C) Micah Z. Brodsky 2016-
##


import argparse
import os
import importlib
import time
import traceback

import gabee.launcher as launcher
import gabee.tasklauncher as tasklauncher
import gabee.variation as variation
import gabee.output_anal as output_anal
from gabee.util import *

opthelp = {
    "workpath"       :  "directory to store mutated configurations (REQUIRED unless GABEE_DEFAULT_WORKPATH environment variable is provided)",
    "templatepath"   :  "template configuration directory (REQUIRED)",
    "fitness"        :  "Python expression for fitness function (REQUIRED)",
    "outputdatapath" :  "path suffix in configuration dir where output results can be found post-simulation",
    "configfile"     :  "path to template configuration file [WARNING: Option not completely working] (defaults to TEMPLATEPATH/config.yaml)",
    "patchfile"      :  "path to configuration patch file (defaults to TEMPLATEPATH/patches.yaml)",
    "assay"          :  "template subdirectory containing an assay to execute; repeat to include multiple assays (first becomes the default assay)",
    "popsize"        :  "population size to initialize",
    "init-mix-rounds":  "number of rounds of selection-less mutations to apply to a newly initialized population",
    "max-concurrency":  "maximum number of concurrent simulation jobs to launch (defaults to population size)",
        # fixme actually that default is no longer accurate... the reality is better but harder to explain....
    "assay-mode"     :  "scheduling mode used with multiple assays (defaults to serial)",
    "gen-count"      :  "number of generations to simulate",
    "init-trajectory":  "initialize from specified trajectory file (containing population and history)",
    "save-trajectory":  "save checkpoints to specified trajectory file (otherwise, a default filename is chosen)",
    "checkpoint-trajectory":  "initialize from (if exists) & save checkpoints to specified trajectory file, and reckon --gen-count from total number of generations; makes GABEE restartable",
    "monitor-image"  :  "display image from best current individual found at specified path suffix in configuration directory",
    "monitor-image-tableau"  :  "display tableau of images from NUM_INDIVS individuals evenly spaced throughout the population (uses --monitor-image argument)",
    "aux-plot-function"    : "plot additional fitness-like function (or bracketed list of functions), evaluated on max fitness individuals",
    "aux-plot-pointfunc"   : "plot a point function evaluated at each cell of the current max fitness individual",
    "monitor-perf"  :   "display a plot tracking GA running time metrics",
    "disable-ui"    :   "disable all GUI monitoring features",
    "save-plots"    :   "snapshot figures and plots to a directory accompanying the trajectory file",
    "avg-mutation-count"   : "expected number of mutations per individual (defaults to 1.0)",
    "mutation-distribution": "distribution used for real-valued mutations",
    "crossover-fraction"   : "expected fraction of traits from second parent in parameterized uniform crossover (defaults to 0)",
    "tournament-size"      : "tournament size for parent tournament selection (defaults to 3); specify 0 for fitness-proportional selection",
    "relative-step-size"   : "multiplicative scale factor to apply to all locus step sizes",
    "wait"           :  "wait until all figure windows have been dismissed before exiting",
    "animate"        :  "show looping animation of best individuals (uses --monitor-image argument)",
    "smoke-test"     :  "configuration smoke test; do not actually prepare and launch population",
    "rerun-trajectory"     : "re-run best scoring individuals under new settings, named with the given suffix attached (requires --init-trajectory)",
    "rerun-reps"     : "number of repetitions to run (and average) with --rerun-trajectory",
    "record-ancestry"      : "record configuration details of all individuals with surviving descedents (increases trajectory file size)",
    "local-concurrency"    : "number of threads or child processes to use for housekeeping tasks (defaults to 8; try reducing if running into memory quota limits); does not affect concurrency of simulation jobs",
    "backendsim"     : "name of Python module GA will use to run backend simulation (default is gabee.betsectl, which runs BETSE)",
    }

def run_cli():
    argparser = argparse.ArgumentParser(prog = "python -m gabee.cli", description = "Run GA search on BETSE configurations")

    def add_argument(name, *args, **kwargs):
        argparser.add_argument(name, *args, help = opthelp[name[2:]], **kwargs)

    default_workpath = os.environ.get("GABEE_DEFAULT_WORKPATH")
    add_argument("--workpath", required = not default_workpath, default = default_workpath) 

    add_argument("--templatepath", required = True) 

    add_argument("--fitness", required = True) 

    add_argument("--outputdatapath") 

    add_argument("--configfile") 

    add_argument("--patchfile") 

    add_argument("--assay", action = "append", metavar = "ASSAYDIR") 

    add_argument("--popsize", type = int, default = 10) 

    add_argument("--init-mix-rounds", type = int, default = 1) 

    add_argument("--max-concurrency", type = int) 

    add_argument("--assay-mode", type = str, choices = {"serial", "parallel"}, default = "serial") 

    add_argument("--gen-count", type = int, default = 50) 

    add_argument("--init-trajectory") 

    add_argument("--save-trajectory") 

    add_argument("--checkpoint-trajectory") 

    add_argument("--monitor-image", action = "append") 

    add_argument("--monitor-image-tableau", type = int, metavar = "NUM_INDIVS") 

    add_argument("--aux-plot-function", action = "append") 

    add_argument("--aux-plot-pointfunc", action = "append") 

    add_argument("--monitor-perf", action = "store_true") 

    add_argument("--disable-ui", action = "store_true") 

    add_argument("--save-plots", action = "store_true") 

    add_argument("--avg-mutation-count", type = float) 

    add_argument("--mutation-distribution", type = str, choices = variation.stepsamplers.keys()) 

    add_argument("--crossover-fraction", type = float) 

    add_argument("--tournament-size", type = int) 

    add_argument("--relative-step-size", type = float) 

    add_argument("--wait", action = "store_true") 

    add_argument("--smoke-test", action = "store_true") 

    add_argument("--animate", action = "store_true") 

    add_argument("--rerun-trajectory", metavar = "RERUN_SUFFIX") 

    add_argument("--rerun-reps", type = int) 

    add_argument("--record-ancestry", action = "store_true") 

    add_argument("--local-concurrency", type = int, metavar = "NUMBER_OF_THREADS") 

    add_argument("--backendsim", type = str, default = "gabee.betsectl") 
    

    args = argparser.parse_args()


    if args.rerun_trajectory is not None:
        if not args.init_trajectory:
            eprint("ERROR: --rerun-trajectory requires --init-trajectory to be supplied.\n")
            sys.exit(1)
    else:
        if args.rerun_reps is not None:
            eprint("ERROR: --rerun-reps is valid only with --rerun-trajectory.\n")
            sys.exit(1)

    if args.save_trajectory and args.checkpoint_trajectory:
        eprint("ERROR: --save-trajectory and --checkpoint-trajectory may not be specified simultaneously.\n")
        sys.exit(1)


    eprint("INFO: GABEE starting up...\n")

    # Prepare configuration information...

    backend_ctl = importlib.import_module(args.backendsim) 

    if args.configfile:
        configpath = args.configfile
    else:
        configpath = os.path.join(args.templatepath, "config.yaml")

    try:
        sim_config = load_config(configpath)
    except Exception:
        sim_config = None
        eprint("Warning: Couldn't read template config file; will probably fail to run.\n")

    if args.outputdatapath:
        outputpath = args.outputdatapath
    else:
        outputpath = backend_ctl.guess_outputpath(sim_config)

    workpath_canonical = os.path.realpath(args.workpath)
    templatepath_canonical = os.path.realpath(args.templatepath)
    if os.path.commonprefix([workpath_canonical, templatepath_canonical]) == templatepath_canonical:
        # Yes, multiple users have done this. x_x
        eprint("ERROR: Workpath must not reside inside template!\n")
        sys.exit(1)

    config = launcher.GaConfig(args.workpath, args.templatepath, outputpath = outputpath,
                               simsetup = backend_ctl.get_simlauncher, assay_exec_mode = args.assay_mode)
    
    if args.assay:
        for subdir in args.assay:
            config.add_subconfiguration(subdir) 
            #, args.configfile, args.patchfile) # TODO config and patch args should be the basenames, once that's actually supported?
            # also ug this uglifies if "." is in there, because then you want the full paths... maybe that should just be disallowed? :P

        if "." not in args.assay:
            config.add_subconfiguration(".", args.configfile, args.patchfile, live = False)
        config.master_subconfig = "."
    else:
        config.add_subconfiguration(".", args.configfile, args.patchfile)
    # FIXME test and fix this config / patch arg shit


    fitfunc = output_anal.eval_fitfunc_str(args.fitness)

    evopts_kwargs = {}
    if args.avg_mutation_count:
        evopts_kwargs["expected_mutation_count"] = args.avg_mutation_count
    if args.tournament_size:
        evopts_kwargs["tourney_size"] = args.tournament_size
    if args.relative_step_size:
        evopts_kwargs["step_size_multiplier"] = args.relative_step_size
    if args.mutation_distribution:
        evopts_kwargs["stepsampler"] = variation.stepsamplers[args.mutation_distribution]
    if args.crossover_fraction:
        evopts_kwargs["crossover_fraction"] = args.crossover_fraction

    evopts = variation.evoptions(**evopts_kwargs)


    # Prepare (new) evolutionary trajectory...
    if args.checkpoint_trajectory and os.path.exists(args.checkpoint_trajectory):
        init_trajpath = args.checkpoint_trajectory
    elif args.init_trajectory:
        init_trajpath = args.init_trajectory
    else:
        init_trajpath = None

    traj = launcher.EvTrajectory()
    if init_trajpath:
        traj.restore_checkpoint(load_config(init_trajpath))

    traj.command_history.append(sys.argv)

    if args.checkpoint_trajectory:
        save_trajpath = args.checkpoint_trajectory
    elif args.save_trajectory != None:
        save_trajpath = args.save_trajectory
    else:
        # Pick a default pathname for trajectory checkpoints
        # (can override by passing "" to --save-trajectory)
        save_trajpath = os.path.join(args.workpath, "gabee-checkpoint-" + time.strftime("%Y%m%d-%H%M%S") + ".yaml")
        eprint("INFO: Saving default trajectory checkpoints to " + save_trajpath + "\n")

    generation_count = args.gen_count
    if args.checkpoint_trajectory and init_trajpath:
        generation_count -= traj.length() - 1


    # Set up basic UI...
    disable_ui = args.disable_ui
    if not disable_ui:
        import gabee.monitorui as monitorui
        figcaption = monitorui.get_path_name_truncated(args.templatepath, 25)
        try:
            monitorui.attach_ui(traj, config, caption = figcaption, show_ancestral = args.record_ancestry)
        except monitorui.UiInitError as e:
            eprint("Warning: Couldn't initialize user interface plots; disabling --")
            eprint("".join(traceback.format_exception(e.__cause__.__class__, e.__cause__, e.__cause__.__traceback__)) + "\n\n")
            
            if args.save_plots:
                # Try again with non-interactive backend
                eprint("INFO: Falling back to non-interactive Matplotlib backend.\n")
                monitorui.plt.switch_backend("Agg")
                monitorui.attach_ui(traj, config, caption = figcaption, show_ancestral = args.record_ancestry)
            else:
                disable_ui = True

    # Handle various diagnostic plots...
    if not disable_ui:
        if args.monitor_image:
            if any(args.monitor_image):
                monitor_image_paths = args.monitor_image
            else:
                # Override default image path guessing if supplied "" for image path
                monitor_image_paths = None
        else:
            # Use default -- One monitor for each suggestion from the simctl, for each separate assay
            monitor_image_paths = [os.path.join(assay, imgsubpath)
                                   for imgsubpath in backend_ctl.guess_monitorimages(sim_config)
                                   for assay in config.live_assays]

        if monitor_image_paths:
            imgmons = []
            for image_path in monitor_image_paths:
                imgmons.append(monitorui.imagefile_monitor(image_path, traj, config))
                imgmons[-1].update()

                if args.monitor_image_tableau:
                    imgmons.append(monitorui.imagetableau_monitor(image_path, args.monitor_image_tableau, traj, config))
                    imgmons[-1].update()
                    # (Note that this puts different classes of monitors in the same imgmons array.)

        if args.aux_plot_function:
            auxmons = []
            for func in args.aux_plot_function:
                auxmons.append(monitorui.auxfunc_monitor(output_anal.eval_fitfunc_str(func, list_ok = True), traj, config))
                auxmons[-1].update()

        if args.aux_plot_pointfunc:
            auxpointmons = []
            for func in args.aux_plot_pointfunc:
                auxpointmons.append(monitorui.pointfunc_monitor(output_anal.eval_fitfunc_str(func), traj, config))
                auxpointmons[-1].update()

        if args.monitor_perf:
            perfmon = monitorui.live_perf_qdmonitor(traj, config, extra_detail = True)

        if args.animate:
            if not monitor_image_paths:
                eprint("ERROR: --animate requires --monitor-image\n")
                sys.exit(1)
            animatemons = []
            for image_path in monitor_image_paths:
                animatemons.append(monitorui.imageanimate_monitor(image_path, traj, config))
        # wishlist todo: make the animation windows synchronously update frames... or at least track the same frame index
        
        if args.save_plots and save_trajpath:                
            monitorui.snapshot_path = os.path.splitext(save_trajpath)[0] + "-plots"


    if init_trajpath:
        # Pull population out of saved trajectory rather than generating a new one
        pop = traj.population
        #if (args.popsize != len(pop)):
        #    eprint("Warning: initial trajectory has different population than requested size.\n")
    else:
        pop = variation.generate_perturbed_pop(config.get_initial_bindings(), 1, args.popsize, repcount = args.init_mix_rounds)
        # todo more options...?
        # possibly carry through the other evoptions? it's a little weird silently ignoring them here
        # on the other hand it'd be confusing to have experiements that change both mutation behavior _and_ initial pop distribution
        # varying one at a time would be preferable
        # FIXME at least document this, current doc is misleading

    default_max_concurrency = len(pop)
    if args.assay and args.assay_mode == "parallel":
        default_max_concurrency *= len(args.assay) 
    max_concurrency = args.max_concurrency or default_max_concurrency

    if args.local_concurrency is not None:
        if args.local_concurrency < 1:
            eprint("ERROR: --local-concurrency must be >= 1\n")
            sys.exit(1)
        config.local_concurrency = args.local_concurrency


    if config.simctl.test_runner() == False:
        eprint("Warning: Couldn't successfully test-launch " + config.simctl.sim_name + "; simulations will probably fail to run.")
        eprint("(Make sure " + config.simctl.sim_name + " is either installed, available from the current directory, or in your PYTHONPATH.)\n")


    if False:  # Diagnostic dump of configuration
        print("Configuration dump:")
        print(yaml.dump(evopts))
        print(yaml.dump(config))


    # Actually do the work!
    if not args.smoke_test:
        tasklauncher.initialize_tasklauncher(max_concurrency)

        if args.rerun_trajectory is not None:
            # Don't run GA, just re-run recorded individuals

            assert(args.init_trajectory)
            rerun_list = [indiv_name for indiv_name, fitness in traj.bestindivs]
            suffix = "_" + args.rerun_trajectory
            repcount = args.rerun_reps if args.rerun_reps else 1
            new_indiv_fitnesses = launcher.rerun_indivs(rerun_list, suffix, fitfunc, config, max_concurrency, traj.saved_configs, repcount)
            new_traj = launcher.make_patched_trajectory(traj, new_indiv_fitnesses, suffix, False)

            # TODO option to do full rerun, not just best indivs?

            traj.restore_checkpoint(new_traj.get_checkpoint()) # Hackish way of displaying the results, if the user wants to see them

            if save_trajpath:
                save_config(new_traj.get_checkpoint(), save_trajpath)

            # FIXME (re-)reruns with duplicate names that already exist don't error out or even sensibly overwrite configs!

        else:
            # Normal case
            if generation_count > 0:
                 launcher.simp_ga_iter(pop, fitfunc, generation_count, config, max_concurrency, traj,
                                      options = evopts, checkpoint_file = save_trajpath, record_ancestry = args.record_ancestry)
            else:
                eprint("INFO: No generations left to run.\n")

    if args.wait:
        monitorui.wait_until_closed()


if __name__ == '__main__':
    run_cli()


# Example: 
# salloc -n 30 -t 3:00:00
# srun --x11=first --pty -N1 -n1 python -m gabee.cli --workpath testruns/autotestpl1s/lruns/ --templatepath testruns/autotestpl1s/basengx/
#  --fitness "reduce_stdev() / (1 + reduce_stdev(deltaval))" --popsize 28 --gen-count 10 --save-trajectory testruns/autotestpl1s/chkptf-ngx-stdev2.yaml
#  --monitor-image "RESULTS/sim_planaria_1/anim_while_solving/Vmem/Vmem_0000038.png"
