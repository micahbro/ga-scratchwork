## SwHoHex Swift-Hohenberg simple test backend simulation for GABEE
##
## (C) Micah Z. Brodsky 2017-
##

import math
import os
import sys
import matplotlib.image
import numpy
import scipy
import scipy.sparse as sparse
import scipy.sparse.linalg
import time
import yaml
import argparse


  #    dataout[y + dataheight * x] =
  #      u + dt * ((epsilon - 1 - u * u + (g2 + c * lap_u) * u) * u - lap2_u - 2 * lap_u);


def defmonitor(saveimgname = None, savedataname = None):
    fig = plt.figure()
    fig.show()
    framecount = 0

    def monitor_update(U_matrix, in_mask):
        nonlocal fig, framecount
        fig = plt.figure(fig.number)
        fig.clear()

        layers = U_matrix.shape[2]
        dim1 = math.ceil(math.sqrt(layers))
        dim2 = math.ceil(layers / dim1)

        for k in range(layers):
            ax = fig.add_subplot(dim1, dim2, k + 1)
            ax.clear()
            im = ax.imshow(U_matrix[:, :, k], cmap = 'RdBu_r')
            plt.colorbar(im)
        fig.canvas.draw()
        plt.pause(0.001)

        if saveimgname:
            fig.savefig(saveimgname + str(framecount) + ".png")

        if savedataname:
            scale = 10 # To be roughly consistent with BETSE default cell spacing... (ought to be a parameter)
            with open(savedataname + str(framecount) + ".csv", "wt") as datafile:
                datafile.write("# x, y, U...\n")

                w = U_matrix.shape[1]
                h = U_matrix.shape[0]
                for y in range(0, h):
                    for x in range(0, w):
                        if in_mask(x, y):
                            oddrow = y % 2
                            x_rect = x + oddrow / 2
                            y_rect = y * math.sqrt(3) / 2
                            datafile.write(str(x_rect * scale) + ", " + str(y_rect * scale) + ", " + ", ".join([str(u) for u in U_matrix[y, x, :]]) + "\n")

        framecount += 1

    return monitor_update

# Quick & dirty compat function for sh1
def animate_swifhohe_run(itercount = 10000, w = 50, h = 50, epsilon = 2, g2 = 0, c = 0, vx = 0, dt = 0.006, dx = 0.3, plotinterval = 500,
                         method = None, invmethod = "cg", precondition = True, bc = "open", mask = None, monitorgen = defmonitor):
    return multi_swifhohe_run(itercount, w, h, 1, [epsilon], [g2], [c], [vx], 0, 1, dt, dx, plotinterval, method, invmethod, precondition, 0 if bc == "open" else 1, mask, monitorgen)


def multi_swifhohe_run(itercount = 10000, w = 50, h = 50, layers = 1, epsilon = [2], g2 = [0], c = [0], vx = [0], mixmat = 0, lambd = 1, dt = 0.006, dx = 0.3, plotinterval = 500,
                         method = None, invmethod = "cg", precondition = True, bcclosure = 0, mask = None, monitorgen = defmonitor):

    n = w * h

    # (Note the nearly 2x increase in default dt. hex is more stable!)
    # ((should check whether wavelengh has decreased _slightly_, though, which could explain it as well...
    #   though if anything it looks like it _increased_ slightly, at least in the vertical direction (which makes sense)))

    if isinstance(mixmat, list):
        mixmat = numpy.matrix(mixmat)
    lambd = numpy.ones(layers) * lambd
    bcclosure = numpy.ones(layers) * bcclosure

    U = numpy.random.uniform(0, 0.01, (n, layers))
    onevec = numpy.ones(n)

    def in_mask(x, y):
        if x < 0 or x >= w or y < 0 or y >= h:
            return False

        oddrow = y % 2
        x_rect = x + oddrow / 2
        y_rect = y * math.sqrt(3) / 2
        w_rect = w + 1/2
        h_rect = h * math.sqrt(3) / 2

        if isinstance(mask, str) and mask == "circle":
            return math.hypot(x_rect - w_rect / 2, y_rect - h_rect / 2)  < min(w_rect / 2, h_rect / 2)
        elif mask is None:
            return True
        else:           
            # Keep mask aspect ratio, truncate on shorter dimension
            xc_rect = x_rect - w_rect / 2
            yc_rect = y_rect - h_rect / 2
            maxdim = max(w_rect, h_rect)
            maskx = round((xc_rect / maxdim + .5) * mask.shape[0])
            masky = round((yc_rect / maxdim + .5) * mask.shape[1]) 

            # Stretch:
            #maskx = round(x_rect / w_rect * mask.shape[0])
            #masky = round(y_rect / h_rect * mask.shape[1]) 

            if maskx < 0 or maskx >= mask.shape[0] or masky < 0 or masky >= mask.shape[1]:
                return False # (Just to be safe)
            return mask[maskx, masky] == 0
            # (if mask comes from scipy.misc.imread, don't forget to transpose it. imread is funny?)


    leftdiag = numpy.zeros(n)
    rightdiag = numpy.zeros(n)
    updiags = [numpy.zeros(n), numpy.zeros(n), numpy.zeros(n)] # -w - 1, -w, -w + 1
    downdiags = [numpy.zeros(n), numpy.zeros(n), numpy.zeros(n)] # +w - 1, +w, +w + 1
    maindiag_open = numpy.zeros(n)
    maindiag_closed = numpy.zeros(n)
    for y in range(0, h):
        for x in range(0, w):
            i = x + w * y
            oddrow = y % 2
            evenrow = (y + 1) % 2
            neighborcount = 0

            if in_mask(x - 1, y):
                neighborcount += 1
                leftdiag[i] = 1
            if in_mask(x + 1, y):
                neighborcount += 1
                rightdiag[i] = 1
            if in_mask(x - evenrow, y - 1): # up-left
                neighborcount += 1
                updiags[oddrow][i] = 1
            if in_mask(x - evenrow + 1, y - 1): # up-right
                neighborcount += 1
                updiags[oddrow + 1][i] = 1
            if in_mask(x - evenrow, y + 1): # down-left
                neighborcount += 1
                downdiags[oddrow][i] = 1
            if in_mask(x - evenrow + 1, y + 1): # down-right
                neighborcount += 1
                downdiags[oddrow + 1][i] = 1

            maindiag_open[i] = -6
            maindiag_closed[i] = -neighborcount
            # possibly replace this with a Robin-like boundary condition coefficient

            if not in_mask(x, y):
                U[i, :] = 0

                maindiag_open[i] = -6
                maindiag_closed[i] = -6
            # masked + CG has horrible issues if you make dx significantly larger than normal

            # also has issues also on certain small masks, perhaps when wavelength is barely fitting??
            # reducing dt seems to help un-wedge it

            # also i think i'm not imagining progressive slowdown issues here with such bad cases!! restarting python helps!?!

            # preconditioning fixes it??
            # (almost all the time... but not _all_ the time?)

            # possibly the extremal values inside compressed tight loops cause trouble? i've once seen one actually go unstable and abort!
            # this would seem to make sense in conjunction with the dt observation -- excessive dt could be driving the nonlinearities near-unstable
            # (indeed, just cutting the timestep in half to dt = 0.1 seems to stop such loops from going unstable)
    
                
    lap = [None] * layers
    for k in range(layers):
        lapdiags = numpy.array([[*updiags[0][w + 1:], *numpy.zeros(w + 1)],
                                [*updiags[1][w:], *numpy.zeros(w)],
                                [*updiags[2][w - 1:], *numpy.zeros(w - 1)],
                                [*leftdiag[1:], 0],
                                maindiag_open * (1 - bcclosure[k]) + maindiag_closed * bcclosure[k],
                                [0, *rightdiag[:n - 1]],
                                [*numpy.zeros(w - 1), *downdiags[0][:n - (w - 1)]],
                                [*numpy.zeros(w), *downdiags[1][:n - w]],
                                [*numpy.zeros(w + 1), *downdiags[2][:n - (w + 1)]]]) / (6 * dx * dx)
        # (positioning the diagonals is confusing as hell because of how spdiags truncates the diagonal data arrays)
    
        lap[k] = sparse.spdiags(lapdiags, [-w - 1, -w, -w + 1, -1, 0, 1, w - 1, w, w + 1], n, n)
        # Of note: i don't really see a big perf difference switching to CSC or CSR format


    #ddx = sparse.spdiags([[*-leftdiag[1:], 0], numpy.ones(n)], [-1, 0], n, n) / dx 
    ddx = sparse.spdiags([[*-leftdiag[1:], 0], [0, *rightdiag[:n - 1]]], [-1, 1], n, n) / 2 / dx

    # Don't know which is best. But, while upwind and downwind both work implicitly, they don't give the same qualitative results.
    # Centered has qualitative behaviors that seem to mix from both of them?? (some might be due to BC interactions... not sure)
    # (behavioral differences include orientation of new stripes, streamer vs bubble shaping, and smooth streaming vs. crumpling)
    # (Whether or not ddx is crank or fully implicit also changes the mix of behaviors!)
    # All seem to be stable up to CFL and even somewhat beyond (implicit/crank, at least -- can't even test that properly with explicit!)

    ident = sparse.identity(n)

    monitor = monitorgen()

    if method == "crank":
        imp_coeff = 1 / 2
        exp_coeff = 1 / 2
    elif method == "implicit":
        imp_coeff = 1
        exp_coeff = 0
    else:
        imp_coeff = 0
        exp_coeff = 1

    imp_op = [None] * layers
    imp_op_inv = [None] * layers
    imp_op_pc = [None] * layers
    for k in range(layers):
        imp_op[k] = ident + dt * imp_coeff * (lambd[k] ** 2 * lap[k] * lap[k] + 2 * lambd[k] * lap[k] + ((numpy.ones(layers) * vx)[k] * ddx))
        # (I wonder if it affects things that (epsilon - 1) * U is not included in imp_op?)
        if invmethod == "direct":
            #imp_op_inv = sparse.linalg.inv(imp_op)
            if n * n > 1.0e8:
                raise ValueError("Don't be stupid!")
            imp_op_inv[k] = numpy.linalg.inv(imp_op[k].todense())

        if precondition:
            imp_op_pc[k] = scipy.sparse.linalg.LinearOperator((n,n), sparse.linalg.spilu(imp_op[k].tocsc()).solve)
            # TODO understand this line ;-D

    lap_U = numpy.zeros((n, layers))
    lap2_U = numpy.zeros((n, layers))

    tp0 = time.perf_counter()
    for i in range(itercount):
        for k in range(layers):
            lap_U[:,k] = lap[k] * U[:,k]
            lap2_U[:,k] = lap[k] * lap_U[:,k]
        ddx_U = ddx * U

        #U = U + dt * (((epsilon - 1) - (U * U) + g2 * U + c * lap_U * U) * U - lap2_U - 2 * lap_U)

        # U' - dt * (-lap2 - 2 * lap) * U' = U + dt * nonlinearcrap
        # i.e. imp_op * U' = Uexp

        # (The overloading of matrix multiply and elementwise multiply kinda makes me queasy. Is there a better way?)

        Uexp = U + dt * ((epsilon - numpy.ones(layers)) - (U * U) + g2 * U + c * lap_U * U) * U + dt * exp_coeff * (-lambd * lambd * lap2_U - 2 * lambd * lap_U - (vx * ddx_U)) + dt * U * mixmat
        if math.isnan(numpy.sum(Uexp)):
            return (U, Uexp)  # Return both as our lame way to signal the error (otherwise the intermediate overflow would go unnoticed)

        if method == None:
            # # FIXME: with FTCS and mixmat you get an error because Uexp is converted to a matrix and not converted back!
            U = numpy.array(Uexp)
        else:
            # (see notes in sh1.py about perf... have not repeated experiments with shh1, but larger explicit dt gives it a slight relative advantage unless you shrink dx)
            # (but also the preconditioner added here helps, even outside the pathologies it was introduced in hopes of fixing!)

            for k in range(layers):
                if invmethod == "cg":
                    if precondition:
                        (cgresult, cginfo) = sparse.linalg.cg(imp_op[k], Uexp[:,k], U[:,k], M = imp_op_pc[k])
                        # (Of note: adding nonlinearity to the operator e.g. variable drift velocity does not seem to be compatible with a precomputed preconditioner)
                    else:
                        (cgresult, cginfo) = sparse.linalg.cg(imp_op[k], Uexp[:,k], U[:,k])
                    U[:,k] = numpy.array(cgresult)
                elif invmethod == "direct":
                    U[:,k] = numpy.array(numpy.transpose(imp_op_inv[k] * numpy.transpose(numpy.asmatrix(Uexp[:,k]))))[0]
                    # WTF isn't there a better way to do a plain old matrix multiply???
                
                    # (In any case this is only faster than CG on _tiny_ substrates, like 20x20!)
                else:
                    U[:,k] = numpy.array(sparse.linalg.spsolve(imp_op[k], Uexp[:,k]))

        if monitor and (i + 1) % plotinterval == 0:
            monitor(numpy.reshape(U, (h, w, layers)), in_mask)

    print(str(time.perf_counter() - tp0) + " sec elapsed")
    return U

# URRGH why does this have subtle asymmetry in boundary streaming behavior w/left vs. right velocities???
# swifhohe.shh1.multi_swifhohe_run(w = 50, h = 50, epsilon = 1.5, g2 = 1.5, plotinterval=4 * 10, itercount=50 * 10, method="crank", dt = 0.2, mask = scipy.misc.imread("testruns\\deftest0\\geo\\circle\\circle_base.png", flatten=1).transpose(), bc = "closed", vx = [0.5, -0.5, 0], layers=3)


#def multi_swifhohe_run(itercount = 10000, w = 50, h = 50, layers = 1, epsilon = [2], g2 = [0], c = [0], vx = [0], mixmat = 0, lambd = 1, dt = 0.006, dx = 0.3, plotinterval = 500,
#                         method = None, invmethod = "cg", precondition = True, bc = "open", mask = None, monitorgen = defmonitor):



def swifhohe_run_config(config : dict, basepath : str):
    layernames = []
    layers = {}


    kwargs = {}

    def grab_arg(argname, shortname = None):
        if argname in config:
            kwargs[shortname if shortname else argname] = config[argname]

    grab_arg("itercount")
    grab_arg("sampling interval", "plotinterval")
    grab_arg("grid size", "dx")
    grab_arg("width", "w")
    grab_arg("height", "h")

    kwargs["dt"] = config.get("time step", 0.2)


    if config.get("closed BC"):
        kwargs["bc"] = "closed"

    if config.get("mask file"):
        kwargs["mask"] = scipy.misc.imread(os.path.join(basepath, config["mask file"]), flatten=1).transpose()
    
    for layercfg in config["layers"]:
        layername = layercfg["name"]
        layernames.append(layername)
        layers[layername] = {}

        layers[layername]["epsilon"] = layercfg.get("epsilon", 2)
        layers[layername]["g2"] = layercfg.get("g2", 0)
        layers[layername]["c"] = layercfg.get("c", 0)
        layers[layername]["vx"] = layercfg.get("vx", 0)
        layers[layername]["lambd"] = layercfg.get("lambda", 1)
        layers[layername]["bcclosure"] = layercfg.get("boundary closure", 0)
        layers[layername]["mixmat"] = layercfg.get("activations", 0) # <-- this is terrible

    kwargs["epsilon"] = [layers[l]["epsilon"] for l in layernames]
    kwargs["g2"] = [layers[l]["g2"] for l in layernames]
    kwargs["c"] = [layers[l]["c"] for l in layernames]
    kwargs["vx"] = [layers[l]["vx"] for l in layernames]
    kwargs["lambd"] = [layers[l]["lambd"] for l in layernames]
    kwargs["bcclosure"] = [layers[l]["bcclosure"] for l in layernames]
    kwargs["mixmat"] = [numpy.ones(len(layers)) * layers[l]["mixmat"] for l in layernames]

    print(kwargs, flush = True)

    saveimgname = os.path.join(basepath, "shh_plot_frame_")
    savedataname = os.path.join(basepath, "shh_data_frame_")

    U = multi_swifhohe_run(layers = len(layers), method = "crank",
                           monitorgen = lambda: defmonitor(saveimgname, savedataname), **kwargs)

    if math.isnan(numpy.sum(U)):
        raise Exception("Simulation went unstable")


if __name__ == '__main__':
    argparser = argparse.ArgumentParser(prog = "python -m gabee.swhohex", description = "Run SwHoHex Swift-Hohenberg simulation")

    argparser.add_argument("configfile") 
    argparser.add_argument("--headless", action = "store_true") 

    args = argparser.parse_args()


    # Matplotlib is awful!
    import matplotlib
    if args.headless:
        matplotlib.use('Agg')
    import matplotlib.pyplot as plt

    with open(args.configfile, mode='rt') as f:
        config = yaml.load(f)

    swifhohe_run_config(config, os.path.dirname(args.configfile))
else:
    import matplotlib.pyplot as plt
