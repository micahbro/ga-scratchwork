## GABEE GA control loop and simulation launcher
##
## (C) Micah Z. Brodsky 2016-
##

# General strategy: 
# Copy template directory, add patchfile and patched configuration (prep_config_dir)
# Run a batch of python threads, one per simulation (tasklauncher.launch_batch)
# Execute simulations via shell command (with aid of betsectl or similar)


import shutil
import os
import time
import copy
import yaml

import gabee.fragmatcher as fragmatcher
import gabee.output_anal as output_anal
import gabee.variation as variation
import gabee.simctl as simctl
from gabee.variation import evoptions
from gabee.util import *
from gabee.perflog import Perflogger
from gabee.tasklauncher import *  # (for shorthand, meh)

#class simlauncher:
#    # Class encapsulating a subsidiary simulator controller; packages together all the handler routines

#    def __init__(self, sim_name, local_runner, slurm_runner, test_runner, output_loader, output_trimmer):
#        self.sim_name = sim_name
#        self.local_runner = local_runner
#        self.slurm_runner = slurm_runner
#        self.test_runner = test_runner
#        self.output_loader = output_loader
#        self.output_trimmer = output_trimmer


class GaConfig:
    # Class encapsulating the high-level, non-mutating configuration of a GA run    
    # (excludes both results data and evolutionary tuning parameters)

    def __init__(self, basepath : str, templatepath : str,
                 simsetup = None, # pat_and_bindings : tuple = (None, None),
                 simrunner = None, outputtrimmer = None, outputloader = None,
                 outputpath : str = None, local_concurrency : int = 8,
                 idler = None, assay_exec_mode = "serial"):
        self.basepath = basepath
        self.templatepath = templatepath

        if simsetup is None:
            import gabee.betsectl as betsectl
            simsetup = betsectl.get_simlauncher

        self.simctl = simsetup(basepath, outputpath)
        self.outputtrimmer = (outputtrimmer if outputtrimmer else 
                              self.simctl.output_trimmer)
        self.outputloader = (outputloader if outputloader else
                             self.simctl.output_loader)
        if simrunner:
            self.simrunner = simrunner
        elif os.environ.get("SLURM_JOB_ID"):
            self.simrunner = self.simctl.slurm_runner
        else:
            eprint("INFO: no SLURM job allocation; running simulations locally.")
            eprint("(If you are on a cluster, try running 'salloc' first.)\n")
            self.simrunner = self.simctl.local_runner

        self.idler = idler

        self.local_concurrency = local_concurrency
        # (Concurrency for housekeeping tasks; does NOT affect simulation concurrency, even if running locally)

        self.subconfigs = {}  # subdir name -> Subconfiguration
        self.master_subconfig = None  # "master" template for defining individual's traits (which may or may not be executed directly)
        self.live_assays = []
        self.assay_exec_mode = assay_exec_mode

    class Subconfiguration:
        # Mini-object capturing a tuple of configuration file(s), associated pattern, and bindings
        def __init__(self, simconfigs, patches):
            self.simconfigs = simconfigs
            self.patches = patches

            (_, self.patchpattern, self.patchbindings
             ) = fragmatcher.match_and_patch_pattern(self.simconfigs, self.patches)

    def add_subconfiguration(self, subdir : str = ".", configpath : str = None, patchpath : str = None, live : bool = True):
        if configpath is None:
            configpath = os.path.join(self.templatepath, subdir, "config.yaml")
        if patchpath is None:
            patchpath = os.path.join(self.templatepath, subdir, "patches.yaml")

        configfilename = os.path.basename(configpath)
        simconfigs = {configfilename : load_config(configpath)}
        patches = {configfilename : load_config(patchpath)}

        subconfig = GaConfig.Subconfiguration(simconfigs, patches)

        if subconfig.patchpattern is None:
            eprint("Error: patchfile %s fails to match against configuration file %s\n" %
                  (patchpath, configpath))
            eprint("Mismatched pattern elements ---")
            eprint(yaml.dump(fragmatcher.get_mismatched_keys_recursive(simconfigs, patches)))
            
            raise Exception("Patchfile failed to match config file")
            # (Is there a better way of bailing out)

        if subdir in self.subconfigs:
            raise ValueError("Duplicate subconfiguration already present: " + subdir)

        self.subconfigs[subdir] = subconfig

        if self.master_subconfig == None:
            # Default to the first one configured
            self.master_subconfig = subdir

        if live:
            self.live_assays.append(subdir)

    def get_initial_bindings(self):
        return self.subconfigs[self.master_subconfig].patchbindings

    def load_output(self, indiv_name):
        output = {subdir : self.outputloader(os.path.join(indiv_name, subdir)) # <-- hacky?
                    for subdir in self.live_assays}
        output[None] = output[self.live_assays[0]] # alias the first assay to a 'default' slot
            # (definitely hacky! :P )
        return output

    def trim_output(self, indiv_name):
        for subdir in self.live_assays:
            self.outputtrimmer(os.path.join(indiv_name, subdir))
        # fixme this may behave weird if "." as well as subdirs are live assays depending on the order removed
        # (not that i expect that to be a normal case)
        # more generally, if any assay is a subdir of another, this will be troublesome


    #def config_patches(self, configpath : str = None, patchpath : str = None):  ###########################
    #    if configpath is None:
    #        configpath = os.path.join(self.templatepath, "config.yaml")
    #    if patchpath is None:
    #        patchpath = os.path.join(self.templatepath, "patches.yaml")

    #    configfilename = os.path.basename(configpath)
    #    self.simconfigs = {configfilename : load_config(configpath)}
    #    self.patches = {configfilename : load_config(patchpath)}

    #    (_, self.patchpattern, self.patchbindings
    #     ) = fragmatcher.match_and_patch_pattern(self.simconfigs, self.patches)

    #    if self.patchpattern is None:
    #        eprint("Error: patchfile %s fails to match against configuration file %s\n" %
    #              (patchpath, configpath))
    #        eprint("Mismatched pattern elements ---")
    #        eprint(yaml.dump(fragmatcher.get_mismatched_keys_recursive(self.simconfigs, self.patches)))
            
    #        raise Exception("Patchfile failed to match config file")
    #        # (Is there a better way of bailing out)

    # todo output field(s) specification


def prep_config_dir(indiv_name : str, indiv_bindings : dict, config : GaConfig) -> None:

    # todo validation?

    targetpath = os.path.join(config.basepath, indiv_name)

    # half-assed attempt at making this idempotent:
    if os.path.exists(targetpath):
        # oops, path exists, and we're too paranoid to issue rm -rf equivalents blindly
        # (only situation where this is known to occur is if user issues multiple rerun-trajectory commands with same suffix)
        # let's try a half-assed measure: invoke the trimmer
        # this only works if the trimmer is the full trimmer (not the light trimmer not currently in use)
        # and if the dir is recent enough to not fail the timestamp sanity checks
        config.outputtrimmer(indiv_name)

        # really, the best long-term solution is probably replacing copytree with something custom that can deal
        # with in-place overwrites. that may also give opportunities to reduce the NFS bottlenecking.
    
    shutil.copytree(
        src = config.templatepath,
        dst = targetpath,
        symlinks = True)  # todo filter for configs so they can be written with does-not-exist validation?
    # note: this is the most costly step of the operation
    # can be significantly sped up by symlinking the read-only portions of the template dir, though
    # (and disk usage is reduced a little too!)

    for subcfgdir in config.subconfigs:
        subconfig = config.subconfigs[subcfgdir]

        newpattern = fragmatcher.update_pattern_values(subconfig.patchpattern, subconfig.patchbindings, indiv_bindings)
        # TODO allow recompute of derived values?
        templateconfig = copy.deepcopy(subconfig.simconfigs) # <-- unnecessary copy??
        (newsimconfigs, _, _) = fragmatcher.match_and_patch_pattern(templateconfig, newpattern)

        for fname in subconfig.simconfigs:
            save_config(newpattern[fname], os.path.join(targetpath, subcfgdir, "delta_" + fname))
            save_config(newsimconfigs[fname], os.path.join(targetpath, subcfgdir, fname))
   

def launch_pop(pop : dict, config : GaConfig, max_concurrency : int):
    #if config.patchpattern is None or config.patchbindings is None:
    #    raise ValueError("Configuration patches not configured, cannot launch")
    if len(config.live_assays) == 0:
        raise ValueError("No assays configured; cannot launch")
    
    with Perflogger.sub("prep_config"):
        #for k in pop:
        #    prep_config_dir(k, pop[k], config)
        def prep_helper(k):
            prep_config_dir(k, pop[k], config)
        launch_batch(pop, prep_helper, config.local_concurrency, True) # Fixed-size local concurrency to cover NFS latency
        # FIXME propagate back exceptions
    eprint("INFO: Configured batch for launching in " + str(Perflogger.sub("prep_config").last_time) + "s\n")
    
    # todo this takes a lot longer than i hoped. may want to reorganize so that
    # all prep does is push out the modified config or maybe even just the delta,
    # and have the rest done as part of the worker job. (pushing just the delta
    # means that the worker has to figure out patchpattern & patchbindings, though.)

    with Perflogger.sub("launch"):
        if config.assay_exec_mode == "serial":
            def serial_assay_runner(indiv_name):
                for subdir in config.live_assays:
                    config.simrunner(os.path.join(indiv_name, subdir)) # todo cleaner invocation method :P

            launch_batch(pop, serial_assay_runner, max_concurrency, idler = config.idler,
                         reaper = lambda j: eprint("Job %s has timed out, should be reaped... (TODO)" % (j,)), timeout_fraction = 0.9)
        elif config.assay_exec_mode == "parallel":
            pop_x_assays = {os.path.join(indiv_name, subdir) : pop[indiv_name] for indiv_name in pop for subdir in config.live_assays}

            launch_batch(pop_x_assays, config.simrunner, max_concurrency, idler = config.idler,
                         reaper = lambda j: eprint("Job %s has timed out, should be reaped... (TODO)" % (j,)), timeout_fraction = 0.9)
        else:
            raise ValueError("Unsupported assay execution mode: " + config.assay_exec_mode)

        #launch_batch(pop, config.simrunner, max_concurrency, idler = config.idler,
        #             reaper = lambda j: eprint("Job %s has timed out, should be reaped... (TODO)" % (j,)), timeout_fraction = 0.9)
    
        # (Amusingly ugly pathology to worry about if reaping is ever implemented for real: if 90% of jobs fail due to instability,
        #  the remaining 10% will get flagged as stragglers! Might want a way to discount failures in estimating running time...)
    
    runstats = Perflogger.sub("launch").last_data
    #print(runstats)
    print("INFO: " + str(100 * runstats["launch_batch_svctime"] /
                         (Perflogger.sub("launch").last_time * max_concurrency)) + "% utilization ratio")


class EvTrajectory:
    # Evolutionaty trajectory class -- records history of a GA run

    def __init__(self):
        self.history = []              # {indiv_name : fitness} dicts for each generation
        self.bestindivs = []           # (indiv_name, fitness) pairs for best of each generation (yes, it's normally redundant)
        self.population = None         # Last population recorded ({indiv_name : config} dict) 
        self.updatehandlers = []
        self.last_fitness_desc = None  # Human-readable description of fitness function
        self.command_history = []      # Command-line invocation history
        self.saved_configs = {}        # Cached copies of top-scoring configs (not guaranteed to be complete, could be trimmed down)

    def appendgen(self, pop : dict, fitness : dict, record_ancestry : bool = False):
        oldpop = self.population

        self.history.append(fitness)
        bestindiv = argmax(fitness)
        self.bestindivs.append((bestindiv, fitness[bestindiv]))
        self.saved_configs[bestindiv] = pop[bestindiv]
        self.population = pop

        if record_ancestry: # record ancestors of entire surviving population?
            for indiv in pop:
                if "$ancestry" in pop[indiv]:
                    ancestry = pop[indiv]["$ancestry"][0].get("Value", [])
                    for parent in ancestry:
                        if parent in oldpop:
                            self.saved_configs[parent] = oldpop[parent]

        # Record immediate ancestors of best indiv (always)
        if "$ancestry" in pop[bestindiv]:
            ancestry = pop[bestindiv]["$ancestry"][0].get("Value", [])
            for parent in ancestry:
                if parent in oldpop:
                    self.saved_configs[parent] = oldpop[parent]
            
        self.update_updatehandlers()

    def update_updatehandlers(self):
        for handler in self.updatehandlers:
            handler()

    def maxfitnesshistory(self):
        return [p[1] for p in self.bestindivs]

    def avgfitnesshistory(self):
        return [sum([genf[k] for k in genf]) / len(genf) for genf in self.history]

    def medianfitnesshistory(self):
        import statistics
        return [statistics.median([genf[k] for k in genf]) for genf in self.history]

    def zerofitnesshistory(self):
        return [len([k for k in genf if genf[k] == 0]) for genf in self.history]

    def pop_fitness(self):
        return self.history[-1] if len(self.history) > 0 else {}

    def best_current(self):
        return self.bestindivs[-1] if len(self.bestindivs) > 0 else (None, None)

    def length(self):
        return len(self.history)

    def trace_ancestry(self, indiv : str):
        # Trace lineage via primary ancestor as far back as we have records
        lineage = [indiv]
        while lineage[-1] in self.saved_configs:
            ancestor = self.saved_configs[lineage[-1]]
            if "$ancestry" in ancestor:
                lineage.append(ancestor["$ancestry"][0]["Value"][-1])
            else:
                break
        return lineage

    def get_checkpoint(self):
        return copy.deepcopy({"history" : self.history,
                              "bestindivs" : self.bestindivs,
                              "population" : self.population,
                              "last_fitness_desc" : self.last_fitness_desc,
                              "command_history" : self.command_history,
                              "saved_configs" : self.saved_configs})
        # (Of note: saved_configs may be written with YAML backreferences to population)

    def restore_checkpoint(self, checkpoint : dict):
        self.history = checkpoint["history"]
        self.bestindivs = checkpoint["bestindivs"]
        self.population = checkpoint["population"]
        self.last_fitness_desc = checkpoint.get("last_fitness_desc")
        self.command_history = checkpoint.get("command_history") or []
        self.saved_configs = checkpoint.get("saved_configs") or {}
        self.update_updatehandlers()


def compute_fitness_pop_concurrent(fitfunc : output_anal.FitnessOp, outputs : dict, pop : dict, config : GaConfig) -> dict:
    def compute_fitness_helper(indiv : str) -> float:
        return output_anal.compute_fitness(fitfunc, indiv, pop[indiv], outputs[indiv])

    fitnessoutputs = launch_batch(pop, compute_fitness_helper, config.local_concurrency, True, True) # Fixed-size local concurrency
    return {indiv : fitnessoutputs[indiv].result for indiv in pop}

def print_pop_summary(pop : dict):
    #TODO something less quick and dirty...
    print(pop)
    #print(yaml.dump(variation.population_trait_stats(pop)))

    print("\nTrait summary:")
    trait_stats = variation.population_trait_stats(pop)
    ranked_pairs = variation.population_traits_variance_ranked(pop)
    summary_size = 8
    max_namelen = max((4, *(len(k) for k in trait_stats.keys())))
    for i in range(len(ranked_pairs)):
        if i < summary_size or i >= len(ranked_pairs) - summary_size:
            trait = ranked_pairs[i][0]
            #print(trait + ": " + yaml.dump(trait_stats[trait]), end="") # laaazy formatting
            ts_trait = trait_stats[trait]
            padding = " " * max(min(max_namelen, 8) - len(trait), 0)
            print("%s:%s {Min: %7.4g, Mean: %7.4g, Max: %7.4g, Stdev: %7.4g (%f%%)}" %
                  (trait, padding,
                   ts_trait["Min"], ts_trait["Mean"], ts_trait["Max"], ts_trait["Stdev"],
                   ranked_pairs[i][1] * 100))
            # (maybe todo possibly normalize up by max's exponent?)
        elif i == summary_size:
            print("...")
    print("")

def print_fitness_summary(fitness : dict):
    #TODO something less quick and dirty...?
    #print(fitness)

    print("Fitness summary:")
    ranked_pairs = sorted([(fitness[k], k) for k in fitness], reverse = True)
    summary_size = 4
    for i in range(len(ranked_pairs)):
        if i < summary_size or i >= len(ranked_pairs) - summary_size:
            indiv_name = ranked_pairs[i][1]
            indiv_fitness = ranked_pairs[i][0]
            print("%s -- %f" % (indiv_name, indiv_fitness))
        elif i == summary_size:
            print("...")

    print("(Mean: %f)\n" % (sum(fitness.values()) / len(fitness)))


# Main GA controller loop
def simp_ga_iter(initpop : dict, fitfunc, itercount : int, config : GaConfig, max_concurrency : int, 
                 trajectory : EvTrajectory = None, options : evoptions = evoptions(), checkpoint_file : str = None,
                 record_ancestry : bool = False):
    pop = initpop if initpop else trajectory.population
    if trajectory is None:
        trajectory = EvTrajectory()
    trajectory.last_fitness_desc = str(fitfunc)

    trimjob = None

    outputs = {indiv : config.load_output(indiv) for indiv in pop}
    if all([all(output.is_empty for output in outputs[indiv].values()) for indiv in outputs]):
        # Run initial generation if not already run
        launch_pop(pop, config, max_concurrency)

        with Perflogger.sub("load_output"):
            #outputs = {indiv : config.load_output(indiv) for indiv in pop}
            readoutputs = launch_batch(pop, config.load_output, config.local_concurrency, True, True) # Fixed-size local concurrency
            outputs = {indiv : readoutputs[indiv].result for indiv in pop}

        # TODO do something saner if partially present?
        # (especially important if cluster starts failing jobs partway through, leading to a partial batch!)
        # (also todo idempotency... if all failed and you force a retry, then this may error trying to re-copy --
        #  normally not an issue because we don't write a gen to a trajectory until it finishes successfully,
        #  but there are cases where it would be nice, e.g. re-running or resuming when some but not all runs
        #  were culled from disk.)
        # (OTOH note that the logic here is subtle and the common case for resumes is a degenerate form of
        #  partial presence -- the un-trimmed winner remains and the others are gone. This stuff probably
        #  could use a refactor... The real concern ought to be 'are fitnesses determined yet', which is only
        #  an issue when starting a new pop.  Also not sure the idempotency concern above is quite accurate.
        #  In any case, idempotency is slightly improved now because of auto attempted trimming in
        #  prep_config_dir, though if trimming crashes partway thru a directory it may not be automatically
        #  re-trimmed due to sanity checks. Only known case where idempotency has actually presented a 
        #  user-facing concern is with repeated 'rerun-trajectory's.)

    if trajectory.population == pop:
        # Fetch initial fitness from historical trajectory if we're continuing a run
        fitness = trajectory.history[-1]
        assert fitness.keys() == pop.keys()

    t_gen_start = None

    for iter in range(itercount):
        if all([all(output.is_empty for output in outputs[indiv].values()) for indiv in outputs]):
            raise Exception("Entire population failed to produce output; aborting")
            # todo pick a better exception type
            # fixme in the unusual circumstance this evolves naturally in the population, we might want to keep trying??           

        print_pop_summary(pop)

        if trajectory.population != pop:
            # (Skip computing fitness and recording new generation if it's already the latest generation, i.e. we're continuing a run)

            with Perflogger.sub("fitness"):
                #fitness = output_anal.compute_fitness_pop(fitfunc, outputs, pop)
                fitness = compute_fitness_pop_concurrent(fitfunc, outputs, pop, config)

                # PROBBBABLY should reogranize this loop so that the invariant is having a finished parent population ready and beginning 
                # with printing stats, trimming, and generating a successor population

            print_fitness_summary(fitness)
            eprint("INFO: Computed population fitness in " + str(Perflogger.sub("fitness").last_time) + "s\n")

            trajectory.appendgen(pop, fitness, record_ancestry)

            if checkpoint_file:
                with Perflogger.sub("checkpoint"):
                    save_config_safe(trajectory.get_checkpoint(), checkpoint_file)
                eprint("INFO: Wrote updated trajectory file " + checkpoint_file + "\n")
        else:
            # Don't need to recompute fitness or update checkpoint; just print the summary
            print_fitness_summary(fitness)        

        bestindiv = argmax(fitness)

        if t_gen_start:
            gentime = time.perf_counter() - t_gen_start
            Perflogger.cur().record("ga_gen_time", gentime)
            Perflogger.cur().collect()
            eprint("INFO: Generation completed in " + str(gentime) + "s total\n" + "-" * 40 + "\n")            
        t_gen_start = time.perf_counter()

        if config.outputtrimmer:
            # Note: input gen is trimmed as well!
            # (and output gen is _not_)
 
            with Perflogger.sub("trim"):
                # Fixed-size local concurrency to cover NFS latency
                #launch_batch([k for k in pop if k != bestindiv], config.trim_output, config.local_concurrency, True)
                # FIXME propagate back exceptions?

                # Trim can get really bogged down on NFS, so we do it in a background thread while we move on
                if trimjob:
                    join_background_batch(trimjob)
                trimjob = launch_batch_background([k for k in pop if k != bestindiv], config.trim_output, config.local_concurrency, mp_if_fork = False)
                trimdone = join_background_batch(trimjob, timeout = 5) # Give it a moment to get going and possibly display warnings before we move on

            #eprint("INFO: Trimmed old generation's data in " + str(Perflogger.sub("trim").last_time) + "s\n")
            eprint("INFO: " + ("Trimmed" if trimdone else "Scheduled trim of") + " old generation's data in " + str(Perflogger.sub("trim").last_time) + "s\n")

        pop = variation.generate_successor_pop(pop, fitness, options, config.get_initial_bindings())
        launch_pop(pop, config, max_concurrency)
        eprint("INFO: Batch #" + str(iter) + " complete\n")

        with Perflogger.sub("load_output"):
            #outputs = {indiv : config.load_output(indiv) for indiv in pop}
            readoutputs = launch_batch(pop, config.load_output, config.local_concurrency, True, True) # Fixed-size local concurrency
            outputs = {indiv : readoutputs[indiv].result for indiv in pop}
            # todo possibly invoke a thing here to extract custom performance metrics?
        eprint("INFO: Loaded output data in " + str(Perflogger.sub("load_output").last_time) + "s\n")

    if all([all(output.is_empty for output in outputs[indiv].values()) for indiv in outputs]):
        # (yet another casualty of the ugly loop structure -- need to check here also for a total failure, such as due to
        #  slurm timeout, on the very last generation)
        raise Exception("Entire population failed to produce output; aborting")

    print_pop_summary(pop)

    with Perflogger.sub("fitness"):
        fitness = compute_fitness_pop_concurrent(fitfunc, outputs, pop, config)
    print_fitness_summary(fitness)

    trajectory.appendgen(pop, fitness, record_ancestry)
    if checkpoint_file:
        with Perflogger.sub("checkpoint"):
            save_config_safe(trajectory.get_checkpoint(), checkpoint_file)
        eprint("INFO: Wrote updated trajectory file " + checkpoint_file + "\n")

    eprint("INFO: Finished running " + str(itercount) + " generations\n")

    return (pop, fitness, trajectory)


def rerun_indivs(orig_indivs : dict, suffix : str, fitfunc, config : GaConfig, max_concurrency : int, cached_configs : dict = {},
                 reps : int = 1):
    pop = {}

    for indiv_name in orig_indivs:
        if indiv_name in cached_configs:
            # If possible, used cached configuration (from trajectory, usually), so user doesn't need
            # to keep full configs around on disk
            patchbindings = cached_configs[indiv_name]
        else:
              # TODO RETEST ME
            targetpath = os.path.join(config.basepath, indiv_name, config.master_subconfig)
            pattern = {}
            indiv_configs = {}
            masterconfigs = config.subconfigs[config.master_subconfig].simconfigs
            for fname in masterconfigs:
                pattern[fname] = load_config(os.path.join(targetpath, "delta_" + fname))
                indiv_configs[fname] = load_config(os.path.join(targetpath, fname))
        
            templateconfig = copy.deepcopy(masterconfigs) # <-- unnecessary copy??
            (_, _, patchbindings) = fragmatcher.match_and_patch_pattern(indiv_configs, pattern)
            if patchbindings is None:
                raise Exception("Delta file for individual " + indiv_name + " failed to match config file")

        for i in range(reps):
            new_name = indiv_name + (suffix or "")
            if reps > 1:
                # extend name with an index number... kinda hacky
                new_name += "_" + str(i + 1)

            pop[new_name] = patchbindings

    launch_pop(pop, config, max_concurrency)
    # FIXME do something more sensible if the config dir already exists? (currently a trim is attempted
    # and if it fails an exception is shown and is left as is)
    # (would also be rather nice to be able to use this to re-run sims of whom only the delta file remains!)

    outputs = {indiv : config.load_output(indiv) for indiv in pop}

    fitness = output_anal.compute_fitness_pop(fitfunc, outputs, pop)

    if reps > 1:
        # if running multiple reps, return a dict of avg fitnesses.
        # note that the named individual does not actually exist on disk, since it's an average;
        # this breaks features like monitor-image and aux-plot-function for a patched trajectory.
        avgfitness = {}
        for indiv_name in orig_indivs:
            new_name = indiv_name + (suffix or "")
            fitaccum = 0
            for i in range(reps):
                run_name = new_name + "_" + str(i + 1)
                # (must match above naming formula. hacky.)
                fitaccum += fitness[run_name]
            avgfitness[new_name] = fitaccum / reps
        return avgfitness 
    else:
        return fitness

# debugging tool -- construct a hacked trajectory showing what fitnesses would have been given the reruns
def make_patched_trajectory(traj : EvTrajectory, new_indiv_fitnesses : dict, suffix : str, patch_full_pop : bool = False):
    newtraj = EvTrajectory()
    newtraj.restore_checkpoint(traj.get_checkpoint())

    for gen in range(len(newtraj.bestindivs)):
        indiv_name, old_fitness = newtraj.bestindivs[gen]
        new_name = indiv_name + (suffix or "")
        if new_name in new_indiv_fitnesses:
            newtraj.bestindivs[gen] = (new_name, new_indiv_fitnesses[new_name])

    if patch_full_pop:
        for gen in range(len(newtraj.history)):
            for indiv_name, old_fitness in newtraj.history[gen].items():
                new_name = indiv_name + (suffix or "")
                if new_name in new_indiv_fitnesses:
                    del newtraj.history[gen][indiv_name]
                    newtraj.history[gen][new_name] = new_indiv_fitnesses[new_name]

    return newtraj




# cnf = gabee.launcher.GaConfig("testruns\\autotest1\\runs", "testruns\\autotest1\\base")
# cnf.config_patches()
# cnf.outputtrimmer = gabee.betsectl.betse_complete_trimmer(cnf.basepath)
# pop = gabee.variation.generate_perturbed_pop(cnf.patchbindings, 1, 8)
# (pop, popf, bestindivs, bestf) = gabee.launcher.simp_ga_iter(pop, lambda x: gabee.output_anal.field_stdev(gabee.output_anal.final_frame(x)), 2, cnf, 4)
