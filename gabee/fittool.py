## GABEE command-line fitness evaluation tool
##
## (C) Micah Z. Brodsky 2017-
##


import argparse
import yaml
import importlib

import gabee.launcher as launcher
import gabee.output_anal as output_anal
from gabee.util import *


def run_fittool_cli():
    argparser = argparse.ArgumentParser(prog = "python -m gabee.fittool", description =
                                        "Compute a fitness function on the output of an existing simulation (i.e. the results of 'plot sim')")

    argparser.add_argument("path", metavar = "INDIV_PATH", help = "path to individual simulation directory")

    argparser.add_argument("--fitness", required = True, help = "fitness function (or list of functions) to evaluate") 

    argparser.add_argument("--configfile", help = "path to config file (defaults to INDIV_PATH/config.yaml)") 

    argparser.add_argument("--patchfile", help = "path to an appropriate patch file with which trait values can be extracted (defaults to INDIV_PATH/patches.yaml)") 

    argparser.add_argument("--backendsim", type = str, default = "gabee.betsectl") 

    args = argparser.parse_args()


    backend_ctl = importlib.import_module(args.backendsim) 

    fitfunc = output_anal.eval_fitfunc_str(args.fitness, list_ok = True)
     
    config = launcher.GaConfig(args.path, args.path, #outputpath = outputpath,
                               simsetup = backend_ctl.get_simlauncher, simrunner = "none")


    # todo support assays??
    # (probably have to copypaste the logic from cli)

    #if args.assay:
    #    for subdir in args.assay:
    #        config.add_subconfiguration(subdir) 
    #    if "." not in args.assay:
    #        config.add_subconfiguration(".", args.configfile, args.patchfile, live = False)
    #    config.master_subconfig = "."
    #else:

    config.add_subconfiguration(".", args.configfile, args.patchfile)   
    # Requiring a patch file is probably a bit more than necessary... perhaps even blank would suffice.
    # But, if you want fitness functions that reference individuals' trait valus, then you need it.

    indiv_output = config.load_output(".")

    indiv_config = config.get_initial_bindings()

    fitlist = fitfunc if isinstance(fitfunc, list) else [fitfunc]

    for ff in fitlist:
        fitval = output_anal.compute_fitness(ff, ".", indiv_config, indiv_output)
        print(fitval)



if __name__ == '__main__':
    run_fittool_cli()