## GABEE command line tool for patch files
##
## (C) Micah Z. Brodsky 2016-
##


import argparse
import yaml

import gabee.fragmatcher as fragmatcher
from gabee.util import *


def run_patchtool_cli():
    argparser = argparse.ArgumentParser(prog = "python -m gabee.patchtool", description = "Apply, summarize, or update a patch file")

    argparser.add_argument("--configfile", required = True) 

    argparser.add_argument("--patchfile", required = True) 

    argparser.add_argument("--summarize", action="store_true", help = "produce a human-readable summary of the match") 

    argparser.add_argument("--updatepatch", action="store_true", help = "generate an updated patch file, copying matched values from CONFIGFILE") 

    argparser.add_argument("--joinpatch", help = "generate a updated patch file, copying match options from patch file (or individual) JOINPATCH") 

    argparser.add_argument("--from-trajectory", help = "load trajectory file and treat JOINPATCH as an individual name from the trajectory") 

    args = argparser.parse_args()

    if sum((args.summarize, args.updatepatch, args.joinpatch != None)) > 1:
        eprint("Error: Cannot specify more than one of --summarize, --updatepatch, and --joinpatch.\n")
        sys.exit(1)


    config = load_config(args.configfile)
    patches = load_config(args.patchfile)
    
    (newconfig, patchpattern, patchbindings) = fragmatcher.match_and_patch_pattern(config, patches)
    if patchbindings is None:
        eprint("Error: patchfile fails to match against configuration file.\n")
        eprint("Mismatched pattern elements ---")
        eprint(yaml.dump(fragmatcher.get_mismatched_keys_recursive(config, patches)))
        sys.exit(1)

    if args.summarize:
        # hacky reformatting bindings to be less hacky for human consumption:
        print(yaml.dump({name : [patchbindings[name][0], {"OrigValue" : patchbindings[name][1]}] for name in patchbindings}))

    elif args.updatepatch:
        # Export updated pattern with values from configfile:

        fragmatcher.backpatch_old_values(patchbindings, overwrite = True)
        print(yaml.dump(patchpattern))

    elif args.joinpatch:
        # Export an updated pattern with options added from alternate patch file (or an indiv):
        # (only adds $match options, not new match blocks; pairs by Name field)

        # TODO maybe: allow multiple --joinpatch'es on a single command

        if args.from_trajectory:
            # Extract bindings from a given indiv in a trajectory
            import gabee.launcher as launcher

            traj = launcher.EvTrajectory()
            traj.restore_checkpoint(load_config(args.from_trajectory))

            if args.joinpatch in traj.saved_configs:
                altpatchbindings = traj.saved_configs[args.joinpatch]
            else:
                eprint("Error: No such individual '" + args.joinpatch + "' saved in trajectory")
                sys.exit(1)
        else:
            # Extract bindings from an alternate patch file
            altpatches = load_config(args.joinpatch)
        
            (altnewconfig, altpatchbindings) = fragmatcher.match_pattern(config, altpatches) 
            # (we use the raw match_pattern because we don't want backpatched values & such)
            if (altnewconfig, altpatchbindings) == fragmatcher.NO_MATCH:
                eprint("Error: second patchfile fails to match against configuration file.\n")
                eprint("Mismatched pattern elements ---")
                eprint(yaml.dump(fragmatcher.get_mismatched_keys_recursive(config, altpatches)))
                sys.exit(1)

        # and also redo the original patchfile, without backpatching
        (basenewconfig, basepatchbindings) = fragmatcher.match_pattern(config, patches)
        assert((basenewconfig, basepatchbindings) != fragmatcher.NO_MATCH)

        for k in altpatchbindings:
            (altmatchexpr, origvalue) = altpatchbindings[k]
            if k in basepatchbindings:
                for f in altmatchexpr:
                    basepatchbindings[k][0][f] = altmatchexpr[f]
                    # destructively modifies _patches_

                    # todo maybe add an option to just copy values (i.e. for upgrading indiv from patch file to another)
            else:
                if not altmatchexpr.get("Meta"):
                    eprint("Warning: Match binding " + str(k) + " not present in primary patch file.\n")
                # drop any metafields silently, they don't really have a role in patch files anyway

        print(yaml.dump(patches))

    else:
        # Export updated configfile with values from patchfile:

        print(yaml.dump(newconfig))


if __name__ == '__main__':
    run_patchtool_cli()