## "Fragment Matcher" pattern matching library (for GABEE)
##
## (C) Micah Z. Brodsky 2016-
##


import copy

from gabee.util import *


NO_MATCH = (None, None)

# match_* return format:  (updated_substrate, bindings)

def match_pattern(substrate, pattern) -> tuple:
    # Finds an instance of <pattern> in <substrate>, and performs two functions simultaneously: 
    # Produces an updated version of <substrate> with new values copied out of <pattern>, 
    # and produces a set of "bindings" pairing match expressions in <pattern> with the _old_
    # values in <substrate> that they originally matched to. Bindings deliberately capture
    # elements of <pattern> by reference, and so <pattern> can be subsequently be modified by 
    # modifying the bound fragments. 
    
    if isinstance(pattern, list):
        return match_list(substrate, pattern)
    elif isinstance(pattern, dict):
        return match_dict(substrate, pattern)
    else:
        return match_atom(substrate, pattern)


def match_dict(substrate, pattern : dict):
    #assert(isinstance(pattern, dict))
    #assert(isinstance(substrate, dict))

    if len(pattern) == 1 and not pattern.get("$match") is None:  ## todo a more yaml-y syntax for match metaexpressions?
        matchname = None
        newvalue = None
        containexpr = None
        patexpr = pattern["$match"]
        if isinstance(patexpr, dict):            
            matchname = patexpr.get("Name")
            newvalue = patexpr.get("Value")
            containexpr = patexpr.get("Containing")
        else:
            patexpr = {"Value" : patexpr, "PatPatchpoint" : pattern}  # expand shorthand (PatPatchpoint grabs the mutable pattern reference FIXME support elsewhere or delete!)
        if matchname is None:               
            matchname = gensym("MATCH_") # generate a nonsense name

        bindings = {matchname : (patexpr, substrate)}

        if containexpr is not None:
            (sub_newvalue, more_bindings) = match_containexpr(substrate, containexpr)
            if more_bindings is None:
                return NO_MATCH

            bindings.update(more_bindings)

            if newvalue is None:
                # current behavior: if there is a new value supplied both by this match expression and a match expression hiding in a subexpression,
                # use the outermost one. this may need to be changed; match expressions hiding in subexpressions are not actually supported yet.
                newvalue = sub_newvalue

        if newvalue is None:               
            newvalue = substrate

        return (newvalue, bindings)

    if not isinstance(substrate, dict):
        return NO_MATCH

    results = substrate.copy()
    bindings = {}
    for k in pattern:
        if k not in substrate:
            return NO_MATCH
        else:
            (new_value, more_bindings) = match_pattern(substrate[k], pattern[k])
            if more_bindings is None:
                return NO_MATCH
            bindings.update(more_bindings)
            results[k] = new_value

    return (results, bindings)

def match_containexpr(substrate, pattern : dict):
    # Match a "Containing:" subexpression

    if not isinstance(pattern, dict):
        return NO_MATCH

    def match_alternatives(substrate, patterns : list): 
        for pat in patterns:
            result = match_pattern(substrate, pat)
            if result != NO_MATCH:
                return result
        return NO_MATCH

    containtype = pattern.get("Type")
    alternatives = pattern.get("Alternatives")
    if not isinstance(alternatives, list):
        raise ValueError("Bad 'Containing:' match expression")
    # TODO a little more validation would be nice

    if containtype == "singleton" or containtype is None:
        return match_alternatives(substrate, alternatives)
    elif containtype == "set" or containtype == "list":
        if not isinstance(substrate, list):
            return NO_MATCH

        results = []
        bindings = {}
        for v_s in substrate:
            (new_value, more_bindings) = match_alternatives(v_s, alternatives)
            # TODO to support match subexpressions, a sub-naming scheme for repeated bindings will need to be added,
            # possibly by passing a namepath.
            if more_bindings is None:
                return NO_MATCH
            else:
                bindings.update(more_bindings)
                results.append(new_value)
        return (results, bindings)
    else:
        raise ValueError("Bad 'Containing:' expression")

def match_list(substrate, pattern : list):

    if not isinstance(substrate, list):
        return NO_MATCH
    if pattern == []:
        return (substrate, {})

    pat_idx = 0
    results = []
    bindings = {}
    for v_s in substrate:
        if pat_idx < len(pattern):
            (new_value, more_bindings) = match_pattern(v_s, pattern[pat_idx])
            if more_bindings is None:
                results.append(v_s)
            else:
                bindings.update(more_bindings)
                pat_idx += 1
                results.append(new_value)
        else:
            results.append(v_s)

        # Note that backtracking is not implemented here. However, the need should not arise,
        # because matching an item inappropriately early cannot (currently) affect the ability
        # of downstream elements to find their appropriate matches. This would change if sublist
        # fragments (not just lone items) or bound variable backreferences were supported.

    if pat_idx >= len(pattern):
        return (results, bindings)
    else:
        return NO_MATCH

def match_atom(substrate, pattern):
    if substrate == pattern:
        return (substrate, {})
    else:
        return NO_MATCH


# Additional tools and wrappers:

def backpatch_old_values(bindings : dict, overwrite : bool = False) -> None:
    # Collect the matched values and write them into the pattern fragments as Value fields
    # (which may then be used to patch a new substrate)
    for k in bindings:
        (matchexpr, origvalue) = bindings[k]
        if "Value" not in matchexpr or overwrite:
            matchexpr["Value"] = origvalue

def import_old_values(bindings : dict) -> dict:
    newbindings = copy.deepcopy(bindings) 
    backpatch_old_values(newbindings)
    return newbindings

def backpatch_names(bindings : dict) -> None:
    # Collect the binding instance names (which may have been auto-generated if left unspecified)
    # and write them into the patern fragements
    for k in bindings:
        (matchexpr, origvalue) = bindings[k]
        matchexpr["Name"] = k

def import_names(bindings : dict) -> dict:
    newbindings = copy.deepcopy(bindings) 
    backpatch_names(newbindings)
    return newbindings


def match_and_patch_pattern(substrate, pattern) -> tuple:
    # Like match_pattern but also yields an updated copy of pattern with match names and values.
    # Bindings may be modified to modify the pattern.
    updated_pattern = copy.deepcopy(pattern)
    (updated_substrate, bindings) = match_pattern(substrate, updated_pattern)
    if (updated_substrate, bindings) == NO_MATCH:
        return (None, None, None)
    backpatch_old_values(bindings)
    backpatch_names(bindings)
    return (updated_substrate, updated_pattern, bindings)


def update_pattern_values(pattern, oldbindings, newbindings):
    # Patch a pattern by way of its bindings, copying new values from a parallel bindings structure
    (newpattern, targetbindings) = copy.deepcopy((pattern, oldbindings))
    for k in newbindings:
        if k in targetbindings:
            targetbindings[k][0]["Value"] = newbindings[k][0]["Value"]
    return newpattern


def update_binding_values(oldbindings, newbindings):
    # Copy values (but not other parameters) from one bindings structure to another
    targetbindings = copy.deepcopy(oldbindings)
    for k in newbindings:
        if k in targetbindings:
            targetbindings[k][0]["Value"] = newbindings[k][0]["Value"]
        elif newbindings[k][0].get("Meta"):
            # Copy any missing metadata fields straight through; they're not actual binding matches so they're fine to add
            targetbindings[k] = copy.deepcopy(newbindings[k])
    return targetbindings


    
# todo warn for duplicate matches?
# (or better yet support them as wildcard instantiations??)


# Debugging tools:

def get_mismatched_keys(substrate, pattern):
    return {k for k in pattern if match_pattern(substrate, {k: pattern[k]}) == NO_MATCH}

def get_mismatched_keys_recursive(substrate, pattern):
    if isinstance(substrate, dict) and isinstance(pattern, dict):
        mismatches = get_mismatched_keys(substrate, pattern)
        return {k : get_mismatched_keys_recursive(substrate[k], pattern[k])
                if k in substrate else None for k in mismatches}
    elif isinstance(substrate, list) and isinstance(pattern, list):
        return [x for x in pattern if match_pattern(substrate, [x]) == NO_MATCH]
        # (minimally useful in this case, alas)
    else:
        return pattern



### Application strategy:
# Match copy of user-supplied pattern spec to base configuration.
# Use pattern spec's match bindings to define the variables to be specified for each individual
# (each binding either gets exactly one or gets zero or more instances, depending on type)
# (the zero or more will probably need their own distinct match tag with a template subtag,
#  that unescapes back into pattern yaml and defines the particular variable bindings within each item
#  -- which will probably have to be lifted (recursively) to the top under their own names, or something) 
# For each individual, overwrite pattern spec copy's match elements with individual-specific binding values 
# Write modified pattern spec to individual-specific configuration delta to readably identify the individual
# Apply individual-specific configuration delta to base, either from the written delta or from the in-memory original
# Write out modified base as config file for simulator run



#  cfg = betse.science.config.sim_config.read("testruns\\deftest0\\config.yaml")
#  (aaa, ppp, bbb) = gabee.fragmatcher.match_and_patch_pattern(cfg, yaml.load("variable settings:\n env boundary concentrations:\n  P:\n   $match:\n    Name: P\n    Max: 1000"))
#  gabee.fragmatcher.match_pattern(cfg, gabee.fragmatcher.update_pattern_values(ppp, bbb, gabee.variation.perturb_indiv(bbb, 1)))